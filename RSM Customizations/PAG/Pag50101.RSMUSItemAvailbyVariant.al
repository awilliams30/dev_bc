page 50101 "RSMUSItemAvailbyVariant"
{
    Caption = 'Item Availability by Variant Filter';
    DeleteAllowed = false;
    InsertAllowed = false;
    LinksAllowed = false;
    ModifyAllowed = false;
    PageType = Card;
    RefreshOnActivate = true;
    SourceTable = Item;
    Permissions = tabledata "RSMUSAllItemVariants" = rmid, tabledata "RSMUSItemVariantFilters" = rmid;

    layout
    {
        area(content)
        {
            group(Options)
            {
                Caption = 'Options';
                field(PeriodType; PeriodType)
                {
                    ApplicationArea = Planning;
                    Caption = 'View by';
                    OptionCaption = 'Day,Week,Month,Quarter,Year,Accounting Period';
                    ToolTip = 'Specifies by which period amounts are displayed.';

                    trigger OnValidate()
                    begin
                        //FindPeriod('');
                        UpdateSubForm;
                    end;
                }
                field(AmountType; AmountType)
                {
                    ApplicationArea = Planning;
                    Caption = 'View as';
                    OptionCaption = 'Net Change,Balance at Date';
                    ToolTip = 'Specifies how amounts are displayed. Net Change: The net change in the balance for the selected period. Balance at Date: The balance as of the last day in the selected period.';

                    trigger OnValidate()
                    begin
                        //FindPeriod('');
                        UpdateSubForm;
                    end;
                }
                field(DateFilter; DateFilter)
                {
                    ApplicationArea = Planning;
                    Caption = 'Date Filter';
                    Editable = false;
                    ToolTip = 'Specifies the dates that will be used to filter the amounts in the window.';
                }
                field(ItemNo; ItemAvailVariantFilter.RSMUSItemFilter)
                {
                    ApplicationArea = Planning;
                    Caption = 'Item';

                    trigger OnValidate()
                    begin
                        ItemAvailVariantFilter.Modify();
                        CurrPage.Update(false);
                        UpdateSubForm();
                    end;
                }
                field(VariantNo; ItemAvailVariantFilter.RSMUSVariantFilter)
                {
                    ApplicationArea = Planning;
                    Caption = 'Variant';

                    trigger OnValidate()
                    begin
                        ItemAvailVariantFilter.Modify();
                        CurrPage.Update(false);
                        UpdateSubForm();
                    end;

                }
                field(VariantDesc; ItemAvailVariantFilter.RSMUSVariantDescFilter)
                {
                    ApplicationArea = Planning;
                    Caption = 'Description';

                    trigger OnValidate()
                    begin
                        ItemAvailVariantFilter.Modify();
                        CurrPage.Update(false);
                        UpdateSubForm();
                    end;
                }
                field(VariantDesc2; ItemAvailVariantFilter.RSMUSVariantDesc2Filter)
                {
                    ApplicationArea = Planning;
                    Caption = 'Description 2';
                    trigger OnValidate()
                    begin
                        ItemAvailVariantFilter.Modify();
                        CurrPage.Update(false);
                        UpdateSubForm();
                    end;
                }

            }
            part(ItemAvailLocLines; RSMUSItemAvailByVariantLines)
            {
                ApplicationArea = Planning;
                Editable = false;
                //SubPageLink = "Item No." = FIELD("No.");
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group("&Item")
            {
                Caption = '&Item';
                Image = Item;
                group("&Item Availability by")
                {
                    Caption = '&Item Availability by';
                    Image = ItemAvailability;
                    action("<Action4>")
                    {
                        ApplicationArea = Planning;
                        Caption = 'Event';
                        Image = "Event";
                        ToolTip = 'View how the actual and the projected available balance of an item will develop over time according to supply and demand events.';

                        trigger OnAction()
                        begin
                            ItemAvailFormsMgt.ShowItemAvailFromItem(Rec, ItemAvailFormsMgt.ByEvent);
                        end;
                    }
                    action(Period)
                    {
                        ApplicationArea = Planning;
                        Caption = 'Period';
                        Image = Period;
                        RunObject = Page "Item Availability by Periods";
                        RunPageLink = "No." = FIELD("No."),
                                      "Global Dimension 1 Filter" = FIELD("Global Dimension 1 Filter"),
                                      "Global Dimension 2 Filter" = FIELD("Global Dimension 2 Filter"),
                                      "Location Filter" = FIELD("Location Filter"),
                                      "Drop Shipment Filter" = FIELD("Drop Shipment Filter"),
                                      "Variant Filter" = FIELD("Variant Filter");
                        ToolTip = 'Show the projected quantity of the item over time according to time periods, such as day, week, or month.';
                    }
                    action(Location)
                    {
                        ApplicationArea = Planning;
                        Caption = 'Location';
                        Image = Warehouse;
                        RunObject = Page "Item Availability by Location";
                        RunPageLink = "No." = FIELD("No."),
                                      "Global Dimension 1 Filter" = FIELD("Global Dimension 1 Filter"),
                                      "Global Dimension 2 Filter" = FIELD("Global Dimension 2 Filter"),
                                      "Location Filter" = FIELD("Location Filter"),
                                      "Drop Shipment Filter" = FIELD("Drop Shipment Filter"),
                                      "Variant Filter" = FIELD("Variant Filter");
                        ToolTip = 'View the actual and projected quantity of the item per location.';
                    }
                    action("BOM Level")
                    {
                        ApplicationArea = Planning;
                        Caption = 'BOM Level';
                        Image = BOMLevel;
                        ToolTip = 'View availability figures for items on bills of materials that show how many units of a parent item you can make based on the availability of child items.';

                        trigger OnAction()
                        begin
                            ItemAvailFormsMgt.ShowItemAvailFromItem(Rec, ItemAvailFormsMgt.ByBOM);
                        end;
                    }
                }
            }
        }
        area(processing)
        {
            action(PreviousPeriod)
            {
                ApplicationArea = Planning;
                Caption = 'Previous Period';
                Image = PreviousRecord;
                Promoted = true;
                PromotedCategory = Process;
                PromotedOnly = true;
                ToolTip = 'Show the information based on the previous period. If you set the View by field to Day, the date filter changes to the day before.';

                trigger OnAction()
                begin
                    IsPrevPeriod := true;
                    //FindPeriod('<=');
                    UpdateSubForm;
                    IsPrevPeriod := false;
                end;
            }
            action(NextPeriod)
            {
                ApplicationArea = Planning;
                Caption = 'Next Period';
                Image = NextRecord;
                Promoted = true;
                PromotedCategory = Process;
                PromotedOnly = true;
                ToolTip = 'Show the information based on the next period. If you set the View by field to Day, the date filter changes to the day before.';

                trigger OnAction()
                begin
                    IsNextPeriod := true;
                    //FindPeriod('>=');
                    UpdateSubForm;
                    IsNextPeriod := false;
                end;
            }
        }
    }

    trigger OnAfterGetCurrRecord()
    begin
        UpdateSubForm();
    end;

    trigger OnAfterGetRecord()
    begin
        SetRange("Drop Shipment Filter", false);
        UpdateSubForm;
    end;

    trigger OnClosePage()
    var
        ItemVariant: Record "Item Variant";
    begin
        CurrPage.ItemAvailLocLines.PAGE.GetRecord(ItemVariant);
        LastVariant := ItemVariant.Code;
    end;

    trigger OnOpenPage()
    var
        lAllItemVariants: Record RSMUSAllItemVariants;
        lItemVariants: Record "Item Variant";
    begin
        lAllItemVariants.Reset();
        lAllItemVariants.DeleteAll();


        lItemVariants.Reset();
        if lItemVariants.FindSet() then
            repeat
                if not lAllItemVariants.Get(lItemVariants."Item No.", lItemVariants.Code, lItemVariants.Description, lItemVariants."Description 2") then begin
                    lAllItemVariants.Init();
                    lAllItemVariants."Item No." := lItemVariants."Item No.";
                    lAllItemVariants.Code := lItemVariants.Code;
                    lAllItemVariants.Description := lItemVariants.Description;
                    lAllItemVariants."Description 2" := lItemVariants."Description 2";
                    lAllItemVariants.Insert();
                end;
            until lItemVariants.Next() = 0;


        ItemAvailVariantFilter.Validate(RSMUSItemFilter, "No.");
        if not ItemAvailVariantFilter.Insert() then
            ItemAvailVariantFilter.Modify();

        UpdateSubForm();
    end;

    var
        Calendar: Record Date;
        ItemAvailFormsMgt: Codeunit "Item Availability Forms Mgt";
        PeriodType: Option Day,Week,Month,Quarter,Year,"Accounting Period";
        AmountType: Option "Net Change","Balance at Date";
        LastVariant: Code[10];
        DateFilter: Text;
        ItemAvailVariantFilter: Record RSMUSItemVariantFilters temporary;
        Item: Record Item;
        IsPrevPeriod: Boolean;
        IsNextPeriod: Boolean;

    local procedure FindPeriod(SearchText: Text[3])
    var
        PeriodFormMgt: Codeunit PeriodFormManagement;
        lDateFilter: Date;
    begin

        if PeriodType = PeriodType::Day then begin
            SetRange("Date Filter", Today);
            DateFilter := GetFilter("Date Filter");
        end else begin
            if GetFilter("Date Filter") <> '' then begin
                Calendar.SetFilter("Period Start", GetFilter("Date Filter"));
                if not PeriodFormMgt.FindDate('+', Calendar, PeriodType) then
                    PeriodFormMgt.FindDate('+', Calendar, PeriodType::Day);
                Calendar.SetRange("Period Start");
            end;
            PeriodFormMgt.FindDate(SearchText, Calendar, PeriodType);
            if AmountType = AmountType::"Net Change" then begin
                SetRange("Date Filter", Calendar."Period Start", Calendar."Period End");
                if GetRangeMin("Date Filter") = GetRangeMax("Date Filter") then
                    SetRange("Date Filter", GetRangeMin("Date Filter"));
            end else
                SetRange("Date Filter", 0D, Calendar."Period End");
            DateFilter := GetFilter("Date Filter");
        end;


        //if Item.GetFilter("Date Filter") <> '' then begin
        //    Calendar.SetFilter("Period Start", Item.GetFilter("Date Filter"));
        //    if not PeriodFormMgt.FindDate('+', Calendar, PeriodType) then
        //        PeriodFormMgt.FindDate('+', Calendar, PeriodType::Day);
        //    Calendar.SetRange("Period Start");
        //end;
        //
        //PeriodFormMgt.FindDate(SearchText, Calendar, PeriodType);
        //if AmountType = AmountType::"Net Change" then begin
        //    Item.SetRange("Date Filter", Calendar."Period Start", Calendar."Period End");
        //    if Item.GetRangeMin("Date Filter") = Item.GetRangeMax("Date Filter") then
        //        Item.SetRange("Date Filter", Item.GetRangeMin("Date Filter"));
        //end else
        //    Item.SetRange("Date Filter", 0D, Calendar."Period End");

        // end else
        //         DateFilter := GetFilter("Date Filter");

        //if DateFilter <> '' then
        //    Item.ModifyAll("Date Filter", Item."Date Filter", false);

    end;

    local procedure UpdateSubForm()
    var
        lItemVariants: Record "Item Variant";
    begin
        //Item.Reset();
        //
        //with ItemAvailVariantFilter do begin
        //    if (('' in [RSMUSVariantFilter, RSMUSVariantDescFilter, RSMUSVariantDesc2Filter]) and (Not ('' in [RSMUSVariantFilter, RSMUSVariantDescFilter, RSMUSVariantDesc2Filter]))) or
        //    (Not ('' in [RSMUSVariantFilter, RSMUSVariantDescFilter, RSMUSVariantDesc2Filter])) then begin
        //        lItemVariants.Reset();
        //
        //        if RSMUSItemFilter <> '' then
        //            lItemVariants.SetFilter("Item No.", RSMUSItemFilter);
        //        if RSMUSVariantFilter <> '' then
        //            lItemVariants.SetFilter(Code, RSMUSVariantFilter);
        //        if RSMUSVariantDescFilter <> '' then
        //            lItemVariants.SetFilter(Description, RSMUSVariantDescFilter);
        //        if RSMUSVariantDesc2Filter <> '' then
        //            lItemVariants.SetFilter("Description 2", RSMUSVariantDesc2Filter);
        //        if lItemVariants.FindSet() then
        //            repeat
        //                Item.SetRange("No.", lItemVariants."Item No.");
        //                if Item.Find('-') then
        //                    Item.Mark(true);
        //                Item.Setrange("No.");
        //            until lItemVariants.Next() = 0;
        //
        //        Item.MarkedOnly(true);
        //
        //    end else
        //        if ItemAvailVariantFilter.RSMUSItemFilter <> '' then begin
        //            Item.SetFilter("No.", RSMUSItemFilter);
        //            Item.FindSet();
        //        end else
        //            Item.FindSet();
        //end;

        if Not (IsNextPeriod or IsPrevPeriod) then
            FindPeriod('')

        else begin
            if IsPrevPeriod then
                FindPeriod('<=');

            if IsNextPeriod then
                FindPeriod('>=');
        end;

        CurrPage.ItemAvailLocLines.Page.Set(Rec, AmountType, ItemAvailVariantFilter);
    end;

    [Scope('Personalization')]
    procedure GetLastVariant(): Code[10]
    begin
        exit(LastVariant);
    end;

}

