page 50106 "RSMUSRSMChange Log Entries"
{
    Caption = 'RSMUS - Change Log Entries';
    DeleteAllowed = false;
    InsertAllowed = false;
    ApplicationArea = All;
    UsageCategory = Administration;
    PageType = Worksheet;
    RefreshOnActivate = true;
    ShowFilter = true;
    SourceTable = "Change Log Entry";
    SourceTableView = SORTING("Table No.", "Primary Key Field 1 Value");
    Permissions = tabledata "Change Log Entry" = rimd;

    layout
    {
        area(content)
        {
            cuegroup("Select Table No.")
            {
                field(TableNo; TableNo)
                {
                    Caption = 'Table No.';
                    Editable = true;
                    ApplicationArea = All;
                    trigger OnDrillDown()
                    begin
                        FilterGroup(4);
                        Obj.SetRange("Object Type", Obj."Object Type"::Table);
                        if PAGE.RunModal(PAGE::Objects, Obj) = ACTION::LookupOK then begin
                            TableNo := Obj."Object ID";
                            Name := Obj."Object Name";
                            SetRange("Table No.", TableNo);
                        end;
                        FilterGroup(0);
                        CurrPage.Update(false);
                    end;

                    trigger OnValidate()
                    begin
                        SetRange("Table No.", TableNo);
                        CurrPage.Update(false);
                    end;
                }
            }
            group(Control1000000006)
            {

                ShowCaption = false;
                field(Name; Name)
                {
                    Caption = 'Table Name';
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;

                }
            }
            repeater(Control1)
            {
                ShowCaption = false;
                field("Entry No."; "Entry No.")
                {
                    ApplicationArea = All;
                    Editable = false;
                    Visible = false;


                }
                field("Date and Time"; "Date and Time")
                {
                    Editable = false;
                    ApplicationArea = All;

                }
                field("User ID"; "User ID")
                {
                    Editable = false;
                    ApplicationArea = All;

                }
                field("Table No."; "Table No.")
                {
                    Editable = false;
                    Lookup = false;
                    Visible = false;
                    ApplicationArea = All;

                }
                field("Table Caption"; "Table Caption")
                {
                    DrillDown = false;
                    Editable = false;
                    ApplicationArea = All;

                }
                field("Primary Key"; "Primary Key")
                {
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;

                }
                field("Primary Key Field 1 No."; "Primary Key Field 1 No.")
                {
                    Editable = false;
                    Lookup = false;
                    Visible = false;
                    ApplicationArea = All;

                }
                field("Primary Key Field 1 Caption"; "Primary Key Field 1 Caption")
                {
                    DrillDown = false;
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;

                }
                field("Primary Key Field 1 Value"; "Primary Key Field 1 Value")
                {
                    Editable = false;
                    ApplicationArea = All;

                }
                field("Primary Key Field 2 No."; "Primary Key Field 2 No.")
                {
                    Editable = false;
                    Lookup = false;
                    Visible = false;
                    ApplicationArea = All;

                }
                field("Primary Key Field 2 Caption"; "Primary Key Field 2 Caption")
                {
                    DrillDown = false;
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;

                }
                field("Primary Key Field 2 Value"; "Primary Key Field 2 Value")
                {
                    Editable = false;
                    ApplicationArea = All;

                }
                field("Primary Key Field 3 No."; "Primary Key Field 3 No.")
                {
                    Editable = false;
                    Lookup = false;
                    Visible = false;
                    ApplicationArea = All;

                }
                field("Primary Key Field 3 Caption"; "Primary Key Field 3 Caption")
                {
                    DrillDown = false;
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;

                }
                field("Primary Key Field 3 Value"; "Primary Key Field 3 Value")
                {
                    Editable = false;
                    ApplicationArea = All;

                }
                field("Field No."; "Field No.")
                {
                    Editable = false;
                    Lookup = false;
                    Visible = false;
                    ApplicationArea = All;

                }
                field("Field Caption"; "Field Caption")
                {
                    DrillDown = false;
                    Editable = false;
                    ApplicationArea = All;

                }
                field("Type of Change"; "Type of Change")
                {
                    Editable = false;
                    ApplicationArea = All;

                }
                field("Old Value"; "Old Value")
                {
                    Editable = false;
                    ApplicationArea = All;

                }
                field("Old Value Local"; GetLocalOldValue)
                {
                    Caption = 'Old Value (Local)';
                    Editable = false;
                    ApplicationArea = All;

                }
                field("New Value"; "New Value")
                {
                    Editable = false;
                    ApplicationArea = All;

                }
                field("New Value Local"; GetLocalNewValue)
                {
                    Caption = 'New Value (Local)';
                    Editable = false;
                    ApplicationArea = All;

                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action("&Print")
            {
                Caption = '&Print';
                Image = Print;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;

                trigger OnAction()
                var
                    ChangeLogEntriesRep: Report "Change Log Entries";
                begin
                    ChangeLogEntriesRep.SetTableView(Rec);
                    ChangeLogEntriesRep.Run;
                end;
            }
        }
    }

    trigger OnOpenPage()
    begin
        FilterGroup(4);
        TableNo := 0;
        SetRange("Table No.", TableNo);
        FilterGroup(0);
    end;

    var
        TableNo: Integer;
        Name: Text;
        Obj: Record AllObjWithCaption;
        PrimaryKey1Filter: Text;
        PrimaryKey2Filter: Text;
        PrimaryKey3Filter: Text;
}

