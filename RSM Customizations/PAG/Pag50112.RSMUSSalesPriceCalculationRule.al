page 50112 "RSMUSSalesPriceCalculationRule"
{
    PageType = List;
    ApplicationArea = All;
    Caption = 'Sales Price Calculation Rules';
    SourceTable = RSMUSSalesPriceCalculationRule;
    UsageCategory = Lists;
    layout
    {
        area(Content)
        {
            repeater(SalesPriceList)
            {
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                }
                field("Variant Code"; Rec."Variant Code")
                {
                    ApplicationArea = All;
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                }
                field(Amount; Rec.Amount)
                {
                    ApplicationArea = All;

                }
                field("Average Selling Price"; Rec."Average Selling Price")
                {
                    ApplicationArea = All;

                }

                field("Last Date/Time Modified"; Rec."Last Date/Time Modified")
                {
                    ApplicationArea = All;
                }
            }
        }
    }




}