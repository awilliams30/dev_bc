page 50104 "RSMUSAllItemQtyOnHand"
{
    Caption = 'Item Qty. On Hand';
    PageType = List;
    //ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = "Item Ledger Entry";
    SourceTableTemporary = true;
    Editable = false;

    layout
    {
        area(Content)
        {
            // group(Detail)
            // {
            //     Caption = 'Filters';
            //     group("Filter")
            //     {
            //         field(ItemFilter;
            //         ItemFilter)
            //         {
            //             Caption = 'Item Filter';
            //             ApplicationArea = all;
            //             trigger OnValidate()
            //             begin
            //                 SetFilter("Item No.", ItemFilter);
            //             end;

            //         }
            //         field(VariantFilter; VariantFilter)
            //         {
            //             Caption = 'Variant Filter';
            //             ApplicationArea = all;
            //             trigger OnValidate()
            //             begin
            //                 SetFilter("Variant Code", VariantFilter);
            //             end;
            //         }
            //         field(VariantDesc2; VariantDesc2Filter)
            //         {
            //             Caption = 'Variant Desc. 2 Filter';
            //             ApplicationArea = all;
            //             trigger OnValidate()
            //             var
            //                 lItemVariant: Record "Item Variant";
            //             begin
            //                 SetFilter("RSMUSVariant Description 2", VariantDesc2Filter);
            //             end;
            //         }
            //         field(Lotfilter; Lotfilter)
            //         {
            //             Caption = 'Lot No.';
            //             ApplicationArea = all;
            //             trigger OnValidate()
            //             begin
            //                 SetFilter("Lot No.", Lotfilter);
            //             end;
            //         }
            //     }
            // }
            repeater(Entries)
            {
                field("Item No."; "Item No.")
                {
                    Caption = 'Item No.';
                    ApplicationArea = all;
                    //Editable = false;
                    LookupPageId = "Item Card";
                    Lookup = true;

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        Item.Reset();
                        Clear(ItemCard);
                        if Item.Get("Item No.") then begin
                            ItemCard.SetRecord(Item);
                            ItemCard.SetTableView(Item);
                            ItemCard.LookupMode(true);
                            ItemCard.RunModal();
                        end;
                    end;

                    trigger OnDrillDown()
                    begin
                        begin
                            Item.Reset();
                            Clear(ItemCard);
                            if Item.Get("Item No.") then begin
                                ItemCard.SetRecord(Item);
                                ItemCard.SetTableView(Item);
                                ItemCard.LookupMode(true);
                                ItemCard.RunModal();
                            end;
                        end;
                    end;
                }
                field(Description; Description)
                {
                    ApplicationArea = all;
                }
                field("Variant Code"; "Variant Code")
                {
                    Caption = 'Variant';
                    ApplicationArea = all;
                    //Editable = false;
                }
                field("RSMUSVariant Description"; "RSMUSVariant Description")
                {
                    Caption = 'Variant Description';
                    ApplicationArea = all;
                    Visible = false;
                }
                field("RSMUSVariant Description 2"; "RSMUSVariant Description 2")
                {
                    Caption = 'Variant Description 2';
                    ApplicationArea = all;
                    //Editable = false;
                }
                field("Location Code"; "Location Code")
                {
                    Caption = 'Location Code';
                    ApplicationArea = all;
                }
                // field("Lot No."; "Lot No.")
                // {
                //     Caption = 'Lot No.';
                //     ApplicationArea = all;
                //     //Editable = false;
                // }
                field(RSMUSType; RSMUSType)
                {
                    Caption = 'Type';
                    ApplicationArea = all;
                }
                field(Quantity; Quantity)
                {
                    Caption = 'Qty. on Hand';
                    ApplicationArea = all;
                    //Editable = false;
                    // LookupPageId = "Item Ledger Entries";
                    // Lookup = true;
                    DrillDown = true;
                    DrillDownPageId = "Item Ledger Entries";

                    trigger OnDrillDown()
                    begin
                        ItemLedgEntry.Reset();
                        ItemLedgEntry.SetFilter("Item No.", "Item No.");
                        ItemLedgEntry.SetFilter("Variant Code", "Variant Code");
                        ItemLedgEntry.SetFilter("Lot No.", "Lot No.");
                        ItemLedgEntry.SetFilter("Location Code", "Location Code");
                        Clear(ItemLedgerList);
                        ItemLedgerList.SetRecord(ItemLedgEntry);
                        ItemLedgerList.SetTableView(ItemLedgEntry);
                        ItemLedgerList.LookupMode(true);
                        ItemLedgerList.RunModal();
                    end;
                }
                field(RSMUSItemGrossRequirement; RSMUSItemGrossRequirement)
                {
                    ApplicationArea = Planning;
                    DecimalPlaces = 0 : 5;
                    ToolTip = 'Specifies the sum of the all demand for the item.';
                    trigger OnDrillDown()
                    begin
                        ShowItemAvailLineList(0);
                    end;
                }

                field(RSMUSItemScheduledRcpt; RSMUSItemScheduledRcpt)
                {
                    ApplicationArea = Planning;
                    DecimalPlaces = 0 : 5;
                    ToolTip = 'Specifies the sum of items from replenishment orders.';
                    trigger OnDrillDown()
                    begin
                        ShowItemAvailLineList(2);
                    end;
                }
                field(RSMUSItemPlannedOrderRcpt; RSMUSItemPlannedOrderRcpt)
                {
                    ApplicationArea = Planning;
                    DecimalPlaces = 0 : 5;
                    ToolTip = 'Specifies the quantity on planned production orders plus planning worksheet lines plus requisition worksheet lines.';
                    trigger OnDrillDown()
                    begin
                        ShowItemAvailLineList(1);
                    end;
                }
                field(RSMUSItemProjectedAvailBal; RSMUSItemProjectedAvailBal)
                {
                    ApplicationArea = Planning;
                    DecimalPlaces = 0 : 5;
                    ToolTip = 'Specifies the item''s availability. This quantity includes all known supply and demand but does not include anticipated demand from production forecasts or blanket sales orders or suggested supplies from planning or requisition worksheets.';
                    trigger OnDrillDown()
                    begin
                        ShowItemAvailLineList(4);
                    end;
                }
                field(RSMUSItemPlannedOrderReleases; RSMUSItemPlannedOrderReleases)
                {
                    ApplicationArea = Planning;
                    DecimalPlaces = 0 : 5;
                    ToolTip = 'Specifies the sum of items from replenishment order proposals, which include planned production orders and planning or requisition worksheets lines, that are calculated according to the starting date in the planning worksheet and production order or the order date in the requisition worksheet. This sum is not included in the projected available inventory. However, it indicates which quantities should be converted from planned to scheduled receipts.';
                    trigger OnDrillDown()
                    begin
                        ShowItemAvailLineList(3);
                    end;
                }
                field("RSMUSAssembly BOM"; "RSMUSAssembly BOM")
                {
                    Caption = 'Assembly BOM';
                    ApplicationArea = all;
                    //AccessByPermission = tabledata "BOM Component" = r;
                    LookupPageId = "Assembly BOM";
                    Lookup = true;

                    trigger OnDrillDown()
                    begin
                        BOMComponent.Reset();
                        BOMComponent.SetRange("Parent Item No.", "Item No.");
                        Clear(AssemblyBOMPage);
                        AssemblyBOMPage.SetRecord(BOMComponent);
                        AssemblyBOMPage.SetTableView(BOMComponent);
                        AssemblyBOMPage.LookupMode(true);
                        AssemblyBOMPage.RunModal();
                    end;

                }
                field("RSMUSItem Tracking Code"; "RSMUSItem Tracking Code")
                {
                    Caption = 'Item Tracking Code';
                    ApplicationArea = all;
                }
                field("RSMUSUnit Price"; "RSMUSUnit Price")
                {
                    Caption = 'Unit Price';
                    ApplicationArea = all;
                }
                field("Item Category Code"; "Item Category Code")
                {
                    ApplicationArea = all;
                }
                field("RSMUSGen. Prod. Posting Group"; "RSMUSGen. Prod. Posting Group")
                {
                    Caption = 'Gen. Prod. Posting Group';
                    ApplicationArea = all;
                }

            }
        }
    }
    var
        OnHandQtyQuery: Query RSMUSOnHandQtyAllItem;
        Item: Record Item;
        ItemVariant: Record "Item Variant";
        ItemLedgEntry: Record "Item Ledger Entry";
        ItemCard: Page "Item Card";
        ItemLedgerList: Page "Item Ledger Entries";
        BOMComponent: Record "BOM Component";
        AssemblyBOMPage: Page "Assembly BOM";
        ItemAvailFormsMgt: Codeunit "Item Availability Forms Mgt";
        GeneralMgmt: Codeunit RSMUSGeneralManagement;


    trigger OnOpenPage()
    var
        lEntryNo: Integer;
        BOMComponent: Record "BOM Component";
    begin
        OnHandQtyQuery.Open();

        lEntryNo := 1;
        while OnHandQtyQuery.Read() do begin
            Clear(Item);
            Clear(BOMComponent);
            if Item.Get(OnHandQtyQuery.ItemNo_ItemLedgerEntry) then begin
                if Item.Type in [Item.Type::Inventory, Item.Type::"Non-Inventory"] then begin
                    Init();
                    "Entry No." := lEntryNo;
                    "Item No." := OnHandQtyQuery.ItemNo_ItemLedgerEntry;
                    Description := Item.Description;
                    "Variant Code" := OnHandQtyQuery.VariantCode_ItemLedgerEntry;

                    Clear(ItemVariant);
                    if ItemVariant.get("Item No.", "Variant Code") then begin
                        "RSMUSVariant Description" := ItemVariant.Description;
                        "RSMUSVariant Description 2" := ItemVariant."Description 2";
                    end;
                    "Location Code" := OnHandQtyQuery.LocationCode_ItemLedgerEntry;
                    //"Lot No." := OnHandQtyQuery.LotNo_ItemLedgerEntry;
                    RSMUSType := Item.Type;
                    Quantity := OnHandQtyQuery.Quantity_ItemLedgerEntry;

                    "RSMUSAssembly BOM" := false;
                    BOMComponent.SetRange("Parent Item No.", "Item No.");
                    if BOMComponent.FindFirst() then
                        "RSMUSAssembly BOM" := true;

                    "RSMUSItem Tracking Code" := Item."Item Tracking Code";

                    "RSMUSUnit Price" := Item."Unit Price";

                    "Item Category Code" := Item."Item Category Code";

                    "RSMUSGen. Prod. Posting Group" := Item."Gen. Prod. Posting Group";

                    CalculateAvailability();

                    Insert(false);
                    lEntryNo += 1;
                end;
            end;
        end;

        OnHandQtyQuery.Close();

        Item.Reset();
        Item.SetFilter(Type, '<>%1', Item.Type::Service);
        if Item.FindSet() then begin
            repeat
                SetRange("Item No.", Item."No.");
                if not find('+') then begin
                    Init();
                    "Entry No." := lEntryNo;
                    "Item No." := Item."No.";
                    Description := Item.Description;
                    //"Variant Code" := OnHandQtyQuery.VariantCode_ItemLedgerEntry;

                    //Clear(ItemVariant);
                    //if ItemVariant.get("Item No.", "Variant Code") then begin
                    //    "RSMUSVariant Description" := ItemVariant.Description;
                    //    "RSMUSVariant Description 2" := ItemVariant."Description 2";
                    //end;
                    //"Lot No." := OnHandQtyQuery.LotNo_ItemLedgerEntry;
                    RSMUSType := Item.Type;
                    Quantity := Item.Inventory; //OnHandQtyQuery.Quantity_ItemLedgerEntry;


                    "RSMUSAssembly BOM" := false;
                    BOMComponent.SetRange("Parent Item No.", "Item No.");
                    if BOMComponent.FindFirst() then
                        "RSMUSAssembly BOM" := true;

                    "RSMUSItem Tracking Code" := Item."Item Tracking Code";

                    "RSMUSUnit Price" := Item."Unit Price";

                    "Item Category Code" := Item."Item Category Code";

                    "RSMUSGen. Prod. Posting Group" := Item."Gen. Prod. Posting Group";

                    CalculateAvailability();

                    Insert(false);
                    lEntryNo += 1;
                end;
                SetRange("Item No.");
            until Item.Next() = 0;
        end;
    end;


    local procedure ShowItemAvailLineList(What: Integer)
    begin
        Item.Reset();
        Item.Get("Item No.");
        Item.SetRange("Variant Filter", "Variant Code");
        Item.SetRange("Location Filter", "Location Code");
        Item.SetRange("Date Filter", 0D, Today);
        ItemAvailFormsMgt.ShowItemAvailLineList(Item, What);
    end;

    local procedure CalculateAvailability()
    var
        ExpectedInventory: Decimal;
        QtyOnHand: Decimal;
    begin
        Item.SetRange("Variant Filter", "Variant Code");
        Item.SetRange("Location Filter", "Location Code");
        Item.SetRange("Date Filter", 0D, Today);
        //item.SetRange("Lot No. Filter", "Lot No.");
        //         ItemAvailFormsMgt.CalcAvailQuantities(
        //   Item, AmountType = AmountType::"Balance at Date",
        //   GrossRequirement, PlannedOrderRcpt, ScheduledRcpt,
        //   PlannedOrderReleases, ProjAvailableBalance, ExpectedInventory, QtyAvailable);
        QtyOnHand := Quantity;
        ItemAvailFormsMgt.CalcAvailQuantities(Item, true, RSMUSItemGrossRequirement, RSMUSItemPlannedOrderRcpt, RSMUSItemScheduledRcpt, RSMUSItemPlannedOrderReleases, RSMUSItemProjectedAvailBal, ExpectedInventory, QtyOnHand);
    end;
}