page 50105 "RSMUSChange Log Delete Setup"
{
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = "RSMUSChange Log Delete Setup";
    Permissions = tabledata "RSMUSChange Log Delete Setup" = rimd;

    layout
    {
        area(Content)
        {
            group(General)
            {
                field("Max Record Delete Count"; "Max Record Delete Count")
                {
                    ApplicationArea = All;

                }
                field("Delete Date Formula"; "Delete Date Formula")
                {
                    ApplicationArea = All;
                }
                field("Change Log Entry Count"; "Change Log Entry Count")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field("Last Run Delete Count"; "Last Run Delete Count")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
            }
        }
    }
    trigger OnOpenPage()
    begin
        Reset;
        IF not Get then begin
            init;
            insert;
        end;
    end;

}