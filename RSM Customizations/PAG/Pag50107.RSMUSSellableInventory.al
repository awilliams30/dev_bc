page 50107 "RSMUSSellableInventory" // 50107
{
    PageType = List;
    Caption = 'Sellable Inventory by Location and Variant';
    Editable = false;
    SourceTable = RSMUSLocationItemVariantBuffer;
    SourceTableTemporary = true;
    UsageCategory = Lists;
    //ApplicationArea = All;

    layout
    {
        area(Content)
        {
            repeater(BufferList)
            {

                field("Location Code"; "Location Code")
                {
                    ApplicationArea = All;
                }
                field("Item No."; "Item No.")
                {
                    ApplicationArea = All;
                }
                field("Item Description"; "Item Description")
                {
                    ApplicationArea = All;
                }
                field("Variant Code"; "Variant Code")
                {
                    ApplicationArea = All;
                }
                field("Variant Description"; "Variant Description 2")
                {
                    ApplicationArea = All;
                }
                field("Remaining Quantity"; "Remaining Quantity")
                {
                    ApplicationArea = All;
                    Caption = 'Qty. Available to Sell';
                }
                field(Value1; Value1)
                {
                    ApplicationArea = All;
                    Caption = 'Qty. in Item Ledger';
                }
                field(Value2; Value2)
                {
                    ApplicationArea = All;
                    Caption = 'Qty. on Sales Orders';
                }
                field(Value3; Value3)
                {
                    ApplicationArea = All;
                    Caption = 'Qty. on Prod. Components';
                }
                field(Value4; Value4)
                {
                    ApplicationArea = All;
                    Caption = 'Qty. in Excluded Bins';
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(InitBufferFromQuery)
            {
                Caption = 'Calculate Available Quantities';
                ApplicationArea = All;

                trigger OnAction()
                begin
                    PopulateBuffer(rec);
                end;
            }
        }
        area(Reporting)
        {
            action(PrintReport)
            {
                Caption = 'Sellable Inventory Report';
                ApplicationArea = All;

                trigger OnAction()
                begin
                    PrintTheReport(rec);
                end;
            }

        }
    }

    trigger OnOpenPage()
    begin
        populatebuffer(rec);
    end;

    Procedure PrintTheReport(var Buffer: Record RSMUSLocationItemVariantBuffer)
    var
        SellableInventoryReport: report RSMUSSellableInventory;
    begin
        with SellableInventoryReport do begin
            SetTableView(Buffer);
            RunModal();
        end;
    end;

    procedure PopulateBuffer(var Buffer: Record RSMUSLocationItemVariantBuffer)
    var
        InventoryReporting: codeunit RSMUSInventoryReporting;
    begin
        InventoryReporting.PopulateLocItemVarBuffer(Buffer);
        if not buffer.isempty then
            buffer.findfirst;
    end;
}