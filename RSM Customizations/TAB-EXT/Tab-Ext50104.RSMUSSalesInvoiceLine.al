tableextension 50104 "RSMUSSales Invoice Line" extends "Sales Invoice Line"
{
    fields
    {
        field(50001; "RSMUS Facebook Line No."; Integer)
        {
            Caption = 'Facebook Line No.';
            BlankZero = true;
            ObsoleteState = Removed;
        }
        field(50002; "RSMUS Customer Ref No."; Text[20])
        {
            Caption = 'Cust Ref No.';
        }
    }
}