/*
RSM0002 GHL 03/27/2020 General Enhancements - G004
  Added code to revise the tablerelation property in RSMUS Filtered No. field when Type is G/L Account.
*/
tableextension 50101 "RSMUSSalesLineExt" extends "Sales Line"
{

    fields
    {
        field(50000; "RSMUS Filtered No."; Code[20])
        {
            Caption = 'RSMUS Filtered No.';
            DataClassification = CustomerContent;
            TableRelation = IF (Type = CONST(" ")) "Standard Text"
            ELSE
            // RSM0002 >>
            // IF (Type = CONST("G/L Account"), "System-Created Entry" = CONST(FALSE)) "G/L Account" WHERE("Direct Posting" = CONST(true),
            //                                                                                                               "Account Type" = CONST(Posting),
            //                                                                                                               "Blocked" = CONST(false))
            // ELSE
            // IF (Type = CONST("G/L Account"), "System-Created Entry" = CONST(true)) "G/L Account"
            IF (Type = CONST("G/L Account"), "System-Created Entry" = CONST(FALSE)) "G/L Account" WHERE("Direct Posting" = CONST(true), "Account Type" = CONST(Posting), "Blocked" = CONST(false), RSMUSIncludeOnSO = CONST(true))
            ELSE
            IF (Type = CONST("G/L Account"), "System-Created Entry" = CONST(true)) "G/L Account" where(RSMUSIncludeOnSO = CONST(true))
            // RSM0002 <<
            ELSE
            IF (Type = CONST("Resource")) Resource
            ELSE
            IF (Type = CONST("Fixed Asset")) "Fixed Asset"
            ELSE
            IF (Type = CONST("Charge (Item)")) "Item Charge"
            // ELSE
            // IF (Type = CONST("Item"), "Document Type" = CONST(Order)) Item WHERE(Blocked = CONST(FALSE), "No." = FILTER(<> 'P-*'))
            // ELSE
            // IF (Type = CONST("Item"), "Document Type" = CONST(Invoice)) Item WHERE(Blocked = CONST(FALSE), "No." = FILTER(<> 'P-*'))
            ELSE
            IF (Type = CONST("Item")) Item WHERE(Blocked = CONST(FALSE));
            ValidateTableRelation = false;
        }
        field(50001; "RSMUS Facebook Line No."; Integer)
        {
            Caption = 'Facebook Line No.';
            BlankZero = true;
            ObsoleteState = Removed;
        }
        field(50002; "RSMUS Customer Ref No."; Text[20])
        {
            Caption = 'Cust Ref No.';
        }
        field(50003; RSMUSVariantCode; Code[10])
        {
            Caption = 'Variant Code';
            TableRelation = if (Type = filter(Item)) RSMUSItemVariants.Code where("Item No." = field("No."));
            trigger OnValidate()
            begin
                Validate("Variant Code", RSMUSVariantCode);
            end;
        }
        modify("No.")
        {
            trigger OnAfterValidate()
            begin
                "RSMUS Filtered No." := "No.";
            end;
        }

    }
}

