/*
    RSM0000 MP 05/29/2020 G008: New table extension to allow posting no series selection
*/

tableextension 50122 "RSMUSNoSeries" extends "No. Series" // 308
{
    fields
    {
        field(50100; "Posting Code"; Code[20])
        {
            Caption = 'Posting Code';
            TableRelation = "No. Series".Code;
        }
    }
}