/*
DCASE0022924 -JCC -2021/09/26 - Updates to remittance
*/
tableextension 50124 "RSMUSCompanyInformation" extends "Company Information"
{
    fields
    {
        field(50000; RSMUSRemittanceName; Text[100])
        {
            Caption = 'Remittance Name';
        }
        field(50001; "RSMUSRemittance Name 2"; Text[50])
        {
            Caption = 'Remittance Name 2';
        }
        field(50002; RSMUSRemittanceAddress; Text[100])
        {
            Caption = 'Remittance Address';
        }
        field(50003; "RSMUSRemittance Address 2"; Text[50])
        {
            Caption = 'Remittance Address 2';
        }
        field(50004; RSMUSRemittanceCity; Text[30])
        {
            Caption = 'Remittance City';
            TableRelation = IF ("Country/Region Code" = CONST('')) "Post Code".City
            ELSE
            IF ("Country/Region Code" = FILTER(<> '')) "Post Code".City WHERE("Country/Region Code" = FIELD("Country/Region Code"));
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;

            trigger OnLookup()
            begin
                PostCode2.LookupPostCode(City, "Post Code", County, "Country/Region Code");
            end;

            trigger OnValidate()
            begin
                PostCode2.ValidateCity(City, "Post Code", County, "Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
            end;
        }
        field(50005; "RSMUSRemittance Post Code"; Code[20])
        {
            Caption = 'Remittance Post Code';
            TableRelation = IF ("Country/Region Code" = CONST('')) "Post Code".Code
            ELSE
            IF ("Country/Region Code" = FILTER(<> '')) "Post Code".Code WHERE("Country/Region Code" = FIELD("Country/Region Code"));
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;

            trigger OnLookup()
            begin
                PostCode2.LookupPostCode(City, "Post Code", County, "Country/Region Code");
            end;

            trigger OnValidate()
            begin
                PostCode2.ValidatePostCode(City, "Post Code", County, "Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
            end;
        }
        field(50006; "RSMUSRemittance County"; Text[30])
        {
            Caption = 'Remittance State';
        }
        field(50007; "RSMUSRemit Country/Region Code"; Code[10])
        {
            Caption = 'Remittance Country/Region Code';
            TableRelation = "Country/Region";

            trigger OnValidate()
            begin
                PostCode2.CheckClearPostCodeCityCounty(RSMUSRemittanceCity, "RSMUSRemittance Post Code", "RSMUSRemittance County", "RSMUSRemit Country/Region Code", xRec."RSMUSRemit Country/Region Code");
            end;
        }
    }

    var
        PostCode2: Record "Post Code";
}