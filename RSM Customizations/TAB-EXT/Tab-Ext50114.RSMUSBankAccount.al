tableextension 50114 "RSMUSBankAccount" extends "Bank Account"
{
    //RSM0002 MP 03/23/2021: Wells Fargo Check file export Check No.
    fields
    {
        field(50100; RSMUSWellsFargoBankAcct; Boolean)
        {
            Caption = 'Wells Fargo Bank Acct.';
        }

        //RSM0002 >>
        field(50101; RSMUSWellsFargoCheckNo; Integer)
        {
            Caption = 'Last Wells Fargo Check No.';
            MinValue = 0;
        }
        //RSM0002 <<
    }
}