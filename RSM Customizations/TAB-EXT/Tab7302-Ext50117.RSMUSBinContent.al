/*
RSM0001 GHL 03/19/2020 Sellable Inventory Report BRD I003
    Added field RSMUSExclude from Sales Report.
RSM0002 GHL 03/19/2020 Generic enhancements - BRD G003
    Added field(s) RSMUSVariant Description 2, RSMUSItem Category Code.
*/

tableextension 50117 "RSMUSBinContent" extends "Bin Content" // 7302
{
    fields
    {
        field(50100; "RSMUSExclude from Sales Report"; Boolean)
        {
            Caption = 'Exclude from Sellable Inventory Reports';
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = lookup (bin."RSMUSExclude from Sales Report" where(Code = field("Bin Code")));
        }
        field(50110; "RSMUSVariant Description 2"; text[50])
        {
            Caption = 'Variant Description 2';
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = lookup ("Item Variant"."Description 2" where("Item No." = field("Item No."), Code = field("Variant Code")));
        }
        field(50120; "RSMUSItem Category Code"; code[20])
        {
            Caption = 'Item Category Code';
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = lookup (Item."Item Category Code" where("No." = field("Item No.")));
        }
    }
}