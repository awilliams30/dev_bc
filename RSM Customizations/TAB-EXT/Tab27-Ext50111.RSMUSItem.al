/*
RSM0001 GHL 06/16/2020 New Custom report Inventory Age Composition - BRD R006
*/
tableextension 50111 RSMUSItem extends Item //27
{
    fields
    {
        field(50120; RSMUSItemCategoryCodeFilter; Code[20])
        {
            Caption = 'Item Category Code Filter';
            FieldClass = FlowFilter;
            TableRelation = "Item Category";
        }
    }
}