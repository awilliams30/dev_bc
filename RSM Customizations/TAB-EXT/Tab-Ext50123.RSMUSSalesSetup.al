/*
RSM- G009- JCC-2020/11/08 - Added RSMUSSales Price Rules Enabled to enable rule. 
*/
tableextension 50123 "RSMUSSalesSetup" extends "Sales & Receivables Setup"
{
    fields
    {
        field(50000; "RSMUSSales Price Rules Enabled"; Boolean)
        {

        }
        field(50001; RSMUSSalesPriceRulePeriod; DateFormula)
        {
            Caption = 'RSMUSSales Price Rule Period';
        }
    }


}