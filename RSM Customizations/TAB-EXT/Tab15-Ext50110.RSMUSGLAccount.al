/*
RSM0001 MP 
RSM0002 GHL 03/26/2020 General Enhancements BRD G004
  Added field and key IncludeOnSO.
*/
tableextension 50110 "RSMUSG/L Account" extends "G/L Account" // 15
{
    fields
    {
        field(50001; RSMUSIncluded; Boolean)
        {
            Caption = 'Include in PO Lookup Field';
        }

        // RSM0002 >>
        field(50010; RSMUSIncludeOnSO; Boolean)
        {
            Caption = 'Include in SO Lookup List';
        }
        // RSM0002 <<
    }

    keys
    {
        // RSM0002 >>
        key(SOList; rsmusincludeonso)
        {

        }
        // RSM0002 <<
    }
}