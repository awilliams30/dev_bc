tableextension 50108 "RSMUSItemLedgerEntry" extends "Item Ledger Entry"
{
    fields
    {
        field(50100; "RSMUSVariant Description"; Text[100])
        {
            Caption = 'Variant Description';
        }
        field(50101; "RSMUSVariant Description 2"; Text[50])
        {
            Caption = 'Variant Description 2';
        }
        field(50102; RSMUSType; Option)
        {
            Caption = 'Type';
            OptionMembers = "Inventory","Service","Non-Inventory";
        }
        field(50103; "RSMUSAssembly BOM"; Boolean)
        {
            Caption = 'Assembly BOM';
        }
        field(50104; "RSMUSItem Tracking Code"; Code[10])
        {
            Caption = 'Item Tracking Code';
        }
        field(50105; "RSMUSUnit Price"; decimal)
        {
            Caption = 'Unit Price';
        }
        field(50106; "RSMUSGen. Prod. Posting Group"; Code[20])
        {
            Caption = 'Gen. Prod. Posting Group';
        }
        field(50107; RSMUSItemGrossRequirement; Decimal)
        {
            Caption = 'Gross Requirement';
        }
        field(50108; RSMUSItemScheduledRcpt; Decimal)
        {
            Caption = 'Scheduled Receipt';
        }
        field(50109; RSMUSItemPlannedOrderRcpt; Decimal)
        {
            Caption = 'Planned Receipt';
        }
        field(50110; RSMUSItemProjectedAvailBal; Decimal)
        {
            Caption = 'Projected Available Balance';
        }
        field(50111; RSMUSItemPlannedOrderReleases; Decimal)
        {
            Caption = 'Planned Order Releases';
        }
    }
}