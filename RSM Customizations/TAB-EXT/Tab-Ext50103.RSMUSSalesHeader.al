tableextension 50103 "RSMUSSalesHeader" extends "Sales Header"
{
    fields
    {
        field(50000; "RSMUSWITS SO No."; Code[20])
        {
            CaptionML = ENU = 'WITS SO No.';
        }
        field(50001; "RSMUSOrder Type"; Code[50])
        {
            Caption = 'Order Type';
            TableRelation = "Dimension Value".Code WHERE("Dimension Code" = CONST('ORDER TYPE'));
        }
    }
}