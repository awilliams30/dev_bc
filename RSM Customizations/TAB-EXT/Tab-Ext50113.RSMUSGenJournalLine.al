tableextension 50113 "RSMUSGenJournalLine" extends "Gen. Journal Line"
{
    fields
    {
        field(50100; RSMUSWFPaymentPrinted; Boolean)
        {
            Caption = 'Wells Fargo Payment Printed';
        }
    }
}