tableextension 50105 "RSMUSItemAttributeMapping" extends "Item Attribute Value Mapping"
{
    fields
    {
        modify("Item Attribute ID")
        {
            trigger OnAfterValidate()
            begin
                if not ItemAttr.Get("Item Attribute ID") then
                    Error('Item Attribute ID %1 cannot be found', Format("Item Attribute ID"));
            end;


        }
        modify("Item Attribute Value ID")
        {
            trigger OnAfterValidate()
            begin
                if "Item Attribute ID" = 0 then
                    Error('Item Attribute ID must be defined first');
            end;
        }

        field(50000; RSMUSAttributeName; Text[250])
        {
            Caption = 'Attribute Name';
            ObsoleteState = Removed;
            ObsoleteReason = 'Not Needed';

            trigger OnValidate()
            begin
                //GetItemAttributeName();
            end;
        }

        field(50001; RSMUSAttributeValName; Text[250])
        {
            Caption = 'Attribute Value Name';
            ObsoleteState = Removed;
            ObsoleteReason = 'Not Needed';
            trigger OnValidate()
            begin
                //GetItemAttrValueName();
            end;
        }
    }

    //procedure GetItemAttributeName(IsAttrNameModify: Boolean)
    /*
    procedure GetItemAttributeName()
    begin

        if ItemAttr.Get("Item Attribute ID") then begin
            RSMUSAttributeName := ItemAttr.Name;
        end else
            if RSMUSAttributeName <> '' then begin
                ItemAttr.Reset();
                ItemAttr.SetRange(Name, RSMUSAttributeName);
                if not ItemAttr.Find('-') then
                    Error('Item Attribute must be an existing attribute')
                else
                    Validate("Item Attribute ID", ItemAttr.ID);
            end;
    end;
    

    //procedure GetItemAttrValueName(IsAttrValNameModify: Boolean)
    procedure GetItemAttrValueName()
    var
        lItemAttrVal: Record "Item Attribute Value";
    begin
        if "Item Attribute ID" = 0 then
            Error('Item Attribute must be defined first');

        lItemAttrVal.Reset();
        lItemAttrVal.SetRange("Attribute ID", "Item Attribute ID");
        lItemAttrVal.SetRange(Value, format(RSMUSAttributeValName));
        if lItemAttrVal.Find('-') then
            validate("Item Attribute Value ID", lItemAttrVal.ID)
        else begin
            lItemAttrVal.Reset();
            lItemAttrVal.SetRange("Attribute ID", "Item Attribute ID");
            if lItemAttrVal.Find('+') then begin
                ItemAttrVal.Init();
                ItemAttrVal.Validate("Attribute ID", "Item Attribute ID");
                ItemAttrVal.Validate(ID, lItemAttrVal.ID + 1);
                ItemAttrVal.Validate(Value, RSMUSAttributeValName);
                if ItemAttrVal.Insert(true) then
                    validate("Item Attribute Value ID", ItemAttrVal.ID);
            end;
        end;
        //end;
    end;
*/
    var

        ItemAttr: record "Item Attribute";
        ItemAttrVal: record "Item Attribute Value";
        ItemAttrValSelection: record "Item Attribute Value Selection";
}