/*
RSM0001 GHL 03/27/2020 Warehouse Receipt Document BRD R005
  Added field "RSMUSVariant Description 2"
*/
tableextension 50121 "RSMUSPostedWhseRcptLine" extends "Posted Whse. Receipt Line" // 7319
{
    fields
    {
        field(50100; "RSMUSVariant Description 2"; text[50])
        {
            Caption = 'Variant Description 2';
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = lookup ("Item Variant"."Description 2" where("Item No." = field("Item No."), Code = field("Variant Code")));
        }

    }

}