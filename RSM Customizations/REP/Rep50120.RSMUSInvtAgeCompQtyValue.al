/*
RSM0001 GHL 07/28/2020 New Custom report Inventory Age Composition - BRD R006
RSM0002 MP  08/11/2020 BRD R006: Update report to include additional fields
*/
report 50120 "RSMUSInvtAgeCompQtyValue"
{
    DefaultLayout = RDLC;
    RDLCLayout = './Report Layouts/Rep50120.RSMUSInvtAgeCompQtyValue.rdl';
    ApplicationArea = Basic, Suite;
    Caption = 'Item Age Composition - Quantity and Value';
    UsageCategory = ReportsAndAnalysis;

    dataset
    {
        dataitem(Item; Item)
        {
            DataItemTableView = SORTING("No.") WHERE(Type = CONST(Inventory));
            RequestFilterFields = "No.", "RSMUSItemCategoryCodeFilter", "Location Filter", "Lot No. Filter", "Global Dimension 1 Filter", "Global Dimension 2 Filter", "Inventory Posting Group", "Statistics Group";

            dataitem(Location; Location)
            {
                DataItemLink = Code = field("Location Filter");
                DataItemTableView = sorting(Code);
                //dataitem("Item Ledger Entry"; "Item Ledger Entry")
                //{
                //    //DataItemLink = "Item No." = FIELD("No."), "Location Code" = FIELD("Location Filter"), "Lot No." = FIELD("Lot No. Filter"), "Item Category Code" = FIELD("RSMUSItemCategoryCodeFilter"), "Global Dimension 1 Code" = FIELD("Global Dimension 1 Filter"), "Global Dimension 2 Code" = FIELD("Global Dimension 2 Filter");
                //    DataItemLink = "Item No." = FIELD("No."), "Lot No." = FIELD("Lot No. Filter"), "Item Category Code" = FIELD("RSMUSItemCategoryCodeFilter"), "Global Dimension 1 Code" = FIELD("Global Dimension 1 Filter"), "Global Dimension 2 Code" = FIELD("Global Dimension 2 Filter");
                //    DataItemTableView = SORTING("Item No.", Open) WHERE(Open = CONST(true));
                //    DataItemLinkReference = Item;
                //
                //    // trigger OnAfterGetRecord()
                //    // begin
                //        // 
                //        // if ("Remaining Quantity" = 0) and Not(PrintLine) then
                //            // CurrReport.Skip();
                //        // 
                //        // CalcRemainingQty;
                //        // RemainingQty += TotalInvtQty;
                //        // PrintLine := true;
                //        // if Item."Costing Method" = Item."Costing Method"::Average then begin
                //            // InvtValue[i] += AverageCost[i] * InvtQty[i];
                //            // InvtValueRTC[i] += AverageCost[i] * InvtQty[i];
                //        // end else begin
                //            // CalcUnitCost;
                //            // TotalInvtValue_Item += UnitCost * Abs(TotalInvtQty);
                //            // InvtValue[i] += UnitCost * Abs(InvtQty[i]);
                // //
                //            // TotalInvtValueRTC += UnitCost * Abs(TotalInvtQty);
                //            // InvtValueRTC[i] += UnitCost * Abs(InvtQty[i]);
                //        // end;
                //    // end;
                // //
                // //
                //    // trigger OnPostDataItem()
                //    // var
                //        // AvgCostCurr: Decimal;
                //        // AvgCostCurrLCY: Decimal;
                //    // begin
                //        // if Item."Costing Method" = Item."Costing Method"::Average then begin
                //            // Item.SetRange("Date Filter");
                //            // ItemCostMgt.CalculateAverageCost(Item, AvgCostCurr, AvgCostCurrLCY);
                //            // TotalInvtValue_Item := AvgCostCurr * RemainingQty;
                //            // TotalInvtValueRTC += TotalInvtValue_Item;
                //        // end;
                //    // end;
                // //
                // //
                //    // trigger OnPreDataItem()
                //    // begin
                //        // SetRange("Location Code",Location.Code);
                //        ////RSM0002 >>
                //        // Clear(InvtValue);
                //        // Clear(TotalInvtValue_Item);
                //        ////TotalInvtValue_Item := 0;
                //        ////RSM0002 <<
                //        // for i := 1 to 5 do begin
                //            // InvtValue[i] := 0;
                //            // InvtQty[i] := 0;
                //        // end;
                //        // RemainingQty := 0;
                //    // end;
                //}
                dataitem("Integer"; "Integer")
                {
                    DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                    column(TotalInvtValue_ItemLedgEntry; TotalInvtValue_Item)
                    {
                        AutoFormatType = 1;
                    }
                    column(InvtValue5_ItemLedgEntry; InvtValue[5])
                    {
                        AutoFormatType = 1;
                    }
                    column(InvtValue4_ItemLedgEntry; InvtValue[4])
                    {
                        AutoFormatType = 1;
                    }
                    column(InvtValue3_ItemLedgEntry; InvtValue[3])
                    {
                        AutoFormatType = 1;
                    }
                    column(InvtValue2_ItemLedgEntry; InvtValue[2])
                    {
                        AutoFormatType = 1;
                    }
                    column(InvtValue1_ItemLedgEntry; InvtValue[1])
                    {
                        AutoFormatType = 1;
                    }
                    column(InvtQty1_ItemLedgEntry; InvtQty[1])
                    {
                        DecimalPlaces = 0 : 2;
                    }
                    column(InvtQty2_ItemLedgEntry; InvtQty[2])
                    {
                        DecimalPlaces = 0 : 2;
                    }
                    column(InvtQty3_ItemLedgEntry; InvtQty[3])
                    {
                        DecimalPlaces = 0 : 2;
                    }
                    column(InvtQty4_ItemLedgEntry; InvtQty[4])
                    {
                        DecimalPlaces = 0 : 2;
                    }
                    column(InvtQty5_ItemLedgEntry; InvtQty[5])
                    {
                        DecimalPlaces = 0 : 2;
                    }
                    column(TotalInvtQty; TotalInvtQty)
                    {
                        DecimalPlaces = 0 : 2;
                    }
                    column(Description_Item; Item.Description)
                    {
                    }
                    column(CategoryCode_Item; item."Item Category Code")
                    {
                    }
                    column(No_Item; Item."No.")
                    {
                    }
                    //
                    column(TodayFormatted; Format(Today, 0, 4))
                    {
                    }
                    column(CompanyName; COMPANYPROPERTY.DisplayName)
                    {
                    }
                    //RSM0002 >>
                    //column(ItemTableCaptItemFilter; TableCaption + ': ' + ItemFilter)
                    //{
                    //}
                    column(ItemTableCaptItemFilter; 'Item' + ': ' + ItemFilter)
                    {
                    }
                    //RSM0002 <<
                    column(ItemFilter; ItemFilter)
                    {
                    }
                    column(PeriodStartDate21; Format(PeriodStartDate[2] + 1))
                    {
                    }
                    column(PeriodStartDate3; Format(PeriodStartDate[3]))
                    {
                    }
                    column(PeriodStartDate31; Format(PeriodStartDate[3] + 1))
                    {
                    }
                    column(PeriodStartDate4; Format(PeriodStartDate[4]))
                    {
                    }
                    column(PeriodStartDate41; Format(PeriodStartDate[4] + 1))
                    {
                    }
                    column(PeriodStartDate5; Format(PeriodStartDate[5]))
                    {
                    }
                    column(PrintLine; PrintLine)
                    {
                    }
                    column(InvtValueRTC1; InvtValueRTC[1])
                    {
                        AutoFormatType = 1;
                    }
                    column(InvtValueRTC2; InvtValueRTC[2])
                    {
                        AutoFormatType = 1;
                    }
                    column(InvtValueRTC5; InvtValueRTC[5])
                    {
                        AutoFormatType = 1;
                    }
                    column(InvtValueRTC4; InvtValueRTC[4])
                    {
                        AutoFormatType = 1;
                    }
                    column(InvtValueRTC3; InvtValueRTC[3])
                    {
                        AutoFormatType = 1;
                    }
                    column(TotalInvtValueRTC; TotalInvtValueRTC)
                    {
                        AutoFormatType = 1;
                    }
                    column(InvtValue1_Item; InvtValue[1])
                    {
                        AutoFormatType = 1;
                    }
                    column(InvtValue2_Item; InvtValue[2])
                    {
                        AutoFormatType = 1;
                    }
                    column(InvtValue3_Item; InvtValue[3])
                    {
                        AutoFormatType = 1;
                    }
                    column(InvtValue4_Item; InvtValue[4])
                    {
                        AutoFormatType = 1;
                    }
                    column(InvtValue5_Item; InvtValue[5])
                    {
                        AutoFormatType = 1;
                    }
                    column(TotalInvtValue_Item; TotalInvtValue_Item)
                    {
                        AutoFormatType = 1;
                    }
                    column(ItemInvtQty1_Item; ItemInvtQty[1])
                    {
                        AutoFormatType = 1;
                    }
                    column(ItemInvtQty2_Item; ItemInvtQty[2])
                    {
                        AutoFormatType = 1;
                    }
                    column(ItemInvtQty3_Item; ItemInvtQty[3])
                    {
                        AutoFormatType = 1;
                    }
                    column(ItemInvtQty4_Item; ItemInvtQty[4])
                    {
                        AutoFormatType = 1;
                    }
                    column(ItemInvtQty5_Item; ItemInvtQty[5])
                    {
                        AutoFormatType = 1;
                    }
                    column(ItemTotalInvtQty_Item; ItemTotalInvtQty)
                    {
                        AutoFormatType = 1;
                    }
                    column(ItemAgeCompositionValueCaption; ItemAgeCompositionValueCaptionLbl)
                    {
                    }
                    column(CurrReportPageNoCaption; CurrReportPageNoCaptionLbl)
                    {
                    }
                    column(AfterCaption; AfterCaptionLbl)
                    {
                    }
                    column(BeforeCaption; BeforeCaptionLbl)
                    {
                    }
                    column(InventoryValueCaption; InventoryValueCaptionLbl)
                    {
                    }
                    column(InventoryQtyCaption; InventoryQtyCaptionLbl)
                    {
                    }
                    column(ItemDescriptionCaption; ItemDescriptionCaptionLbl)
                    {
                    }
                    column(ItemNoCaption; ItemNoCaptionLbl)
                    {
                    }
                    column(ItemCatetoryCaption; ItemCategoryCaptionLbl)
                    {
                    }
                    column(QuantityCaption; quantitycaptionlbl)
                    {
                    }
                    column(ValueCaption; valueCaptionLbl)
                    {
                    }

                    column(TotalCaption; TotalCaptionLbl)
                    {
                    }
                    //RSM0002 >>
                    column(LocationCaption; LocationCaptionLbl) { } //MP
                    column(LocationCode_Location; Location.Code) { }//MP    
                    //RSM0002 <<  

                    trigger OnAfterGetRecord()
                    var
                        ItemLedgEntry: Record "Item Ledger Entry";
                    begin
                        clear(ItemValueLedgerEntry);
                        ItemValueLedgerEntry.Reset();
                        ItemValueLedgerEntry.SetRange("Item No.", Item."No.");
                        ItemValueLedgerEntry.SetRange("Location Code", Location.Code);

                        if Item.GetFilter("Lot No. Filter") <> '' then
                            ItemValueLedgerEntry.SetRange("Lot No.", Item.GetFilter("Lot No. Filter"));
                        if Item.GetFilter(RSMUSItemCategoryCodeFilter) <> '' then
                            ItemValueLedgerEntry.SetRange("Item Category Code", Item.GetFilter(RSMUSItemCategoryCodeFilter));
                        if Item.GetFilter("Global Dimension 1 Filter") <> '' then
                            ItemValueLedgerEntry.SetRange("Global Dimension 1 Code", Item.GetFilter("Global Dimension 1 Filter"));
                        if Item.GetFilter("Global Dimension 2 Filter") <> '' then
                            ItemValueLedgerEntry.SetRange("Global Dimension 2 Code", Item.GetFilter("Global Dimension 2 Filter"));
                        ItemValueLedgerEntry.SetRange(Open, true);
                        ItemValueLedgerEntry.SetFilter("Remaining Quantity", '>%1', 0);

                        //>>
                        ItemValueLedgerEntry.SetFilter("Posting Date", '<=%1', PeriodStartDate[5]);

                        if ItemValueLedgerEntry.FindSet() then
                            repeat
                                //if (ItemValueLedgerEntry."Remaining Quantity" <> 0) then begin
                                CalcRemainingQty;
                                RemainingQty += TotalInvtQty;
                                PrintLine := true;
                                if Item."Costing Method" = Item."Costing Method"::Average then begin
                                    InvtValue[i] += AverageCost[i] * InvtQty[i];
                                    InvtValueRTC[i] += AverageCost[i] * InvtQty[i];
                                end else begin
                                    CalcUnitCost;
                                    TotalInvtValue_Item += UnitCost * Abs(TotalInvtQty);
                                    InvtValue[i] += UnitCost * Abs(InvtQty[i]);
                                    TotalInvtValueRTC += UnitCost * Abs(TotalInvtQty);
                                    InvtValueRTC[i] += UnitCost * Abs(InvtQty[i]);
                                end;
                            //end;
                            until ItemValueLedgerEntry.Next() = 0;

                        if not (PrintLine) then
                            CurrReport.Skip();

                    end;

                    trigger OnPreDataItem()
                    begin
                        //SetRange("Location Code",Location.Code);
                        //RSM0002 >>
                        Clear(InvtValue);
                        Clear(TotalInvtValue_Item);
                        //TotalInvtValue_Item := 0;
                        //RSM0002 <<
                        for i := 1 to 5 do begin
                            InvtValue[i] := 0;
                            //InvtQty[i] := 0;
                        end;
                        //RemainingQty := 0;
                    end;


                    trigger OnPostDataItem()
                    var
                        AvgCostCurr: Decimal;
                        AvgCostCurrLCY: Decimal;
                    begin
                        if Item."Costing Method" = Item."Costing Method"::Average then begin
                            Item.SetRange("Date Filter");
                            ItemCostMgt.CalculateAverageCost(Item, AvgCostCurr, AvgCostCurrLCY);
                            TotalInvtValue_Item := AvgCostCurr * RemainingQty;
                            TotalInvtValueRTC += TotalInvtValue_Item;
                        end;
                    end;

                }

                trigger OnAfterGetRecord()
                var
                    ItemLedgEntry: Record "Item Ledger Entry";
                begin
                    PrintLine := false;

                    ItemTotalInvtQty := 0;
                    for i := 1 to 5 do
                        ItemInvtQty[i] := 0;


                    //Item.SetFilter("Location Filter",Location.Code);
                    Clear(TempItem);
                    TempItem := Item;
                    TempItem.CopyFilters(Item);
                    TempItem.SetFilter("Location Filter", Location.Code);
                    ItemLedgEntry.FilterLinesWithItemToPlan(TempItem, false);

                    //>>
                    ItemLedgEntry.SetFilter("Posting Date", '<=%1', PeriodStartDate[5]);

                    if ItemLedgEntry.FindSet then
                        repeat
                            PrintLine := true;
                            ItemTotalInvtQty := ItemTotalInvtQty + ItemLedgEntry."Remaining Quantity";
                            for i := 1 to 5 do
                                if (ItemLedgEntry."Posting Date" > PeriodStartDate[i]) and (ItemLedgEntry."Posting Date" <= PeriodStartDate[i + 1]) then
                                    ItemInvtQty[i] := ItemInvtQty[i] + ItemLedgEntry."Remaining Quantity";
                        until ItemLedgEntry.Next = 0;
                end;


                trigger OnPreDataItem()
                begin
                    //SetRange("Location Code",Location.Code);
                    //RSM0002 >>
                    // Clear(InvtValue);
                    // Clear(TotalInvtValue_Item);
                    //TotalInvtValue_Item := 0;
                    //RSM0002 <<
                    for i := 1 to 5 do begin
                        //InvtValue[i] := 0;
                        InvtQty[i] := 0;
                    end;
                    RemainingQty := 0;
                end;
            }
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(EndingDate; PeriodStartDate[5])
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Ending Date';
                        ToolTip = 'Specifies the end date of the report. The report calculates backwards from this date and sets up three periods of the length specified in the Period Length field.';

                        trigger OnValidate()
                        begin
                            if PeriodStartDate[5] = 0D then
                                Error(Text002);
                        end;
                    }
                    field(PeriodLength; PeriodLength)
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Period Length';
                        ToolTip = 'Specifies the length of the three periods in the report.';

                        trigger OnValidate()
                        begin
                            if Format(PeriodLength) = '' then
                                Evaluate(PeriodLength, '<0D>');
                        end;
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnOpenPage()
        begin
            if PeriodStartDate[5] = 0D then
                PeriodStartDate[5] := CalcDate('<CM>', WorkDate);


            if Format(PeriodLength) = '' then
                Evaluate(PeriodLength, '<1M>');
        end;
    }

    labels
    {
    }

    trigger OnPreReport()
    var
        NegPeriodLength: DateFormula;
    begin
        ItemFilter := Item.GetFilters;

        PeriodStartDate[6] := DMY2Date(31, 12, 9999);
        Evaluate(NegPeriodLength, StrSubstNo('-%1', Format(PeriodLength)));
        for i := 1 to 3 do
            PeriodStartDate[5 - i] := CalcDate(NegPeriodLength, PeriodStartDate[6 - i]);

    end;

    var
        Text002: Label 'Enter the ending date';
        ItemCostMgt: Codeunit ItemCostManagement;
        ItemFilter: Text;
        InvtValue: array[6] of Decimal;
        InvtValueRTC: array[6] of Decimal;
        InvtQty: array[6] of Decimal;
        ItemInvtQty: array[6] of Decimal;
        UnitCost: Decimal;
        PeriodStartDate: array[6] of Date;
        PeriodLength: DateFormula;
        i: Integer;
        TotalInvtValue_Item: Decimal;
        TotalInvtValueRTC: Decimal;
        TotalInvtQty: Decimal;
        ItemTotalInvtQty: Decimal;
        PrintLine: Boolean;
        AverageCost: array[5] of Decimal;
        AverageCostACY: array[5] of Decimal;
        ItemAgeCompositionValueCaptionLbl: Label 'Item Age Composition - Quantity and Value';
        CurrReportPageNoCaptionLbl: Label 'Page';
        AfterCaptionLbl: Label 'After...';
        BeforeCaptionLbl: Label '...Before';
        InventoryValueCaptionLbl: Label 'Inventory Value';
        InventoryQtyCaptionLbl: Label 'Inventory';
        ItemDescriptionCaptionLbl: Label 'Description';
        ItemCategoryCaptionLbl: Label 'Item Category';
        QuantityCaptionLbl: Label 'Quantity';
        ValueCaptionLbl: Label 'Value';
        ItemNoCaptionLbl: Label 'Item No.';
        TotalCaptionLbl: Label 'Total';
        RemainingQty: Decimal;
        //RSM0002 >>
        LocationCaptionLbl: Label 'Location';
        TempItem: Record Item temporary;
        ItemValueLedgerEntry: Record "Item Ledger Entry";
        Continue: boolean;
        EndingDate: Date;
    //RSM0002 <<

    local procedure CalcRemainingQty()
    begin
        with ItemValueLedgerEntry do begin
            //with "Item Ledger Entry" do begin
            for i := 1 to 5 do
                InvtQty[i] := 0;

            TotalInvtQty := "Remaining Quantity";
            for i := 1 to 5 do
                if ("Posting Date" > PeriodStartDate[i]) and
                   ("Posting Date" <= PeriodStartDate[i + 1])
                then
                    if "Remaining Quantity" <> 0 then begin
                        InvtQty[i] := "Remaining Quantity";
                        exit;
                    end;
        end;
    end;

    local procedure CalcUnitCost()
    var
        ValueEntry: Record "Value Entry";
    begin
        with ValueEntry do begin
            //SetRange("Item Ledger Entry No.", "Item Ledger Entry"."Entry No.");
            //Message(format(ItemValueLedgerEntry."Entry No."));
            SetRange("Item Ledger Entry No.", ItemValueLedgerEntry."Entry No.");
            UnitCost := 0;

            if Find('-') then
                repeat
                    if "Partial Revaluation" then
                        SumUnitCost(UnitCost, "Cost Amount (Actual)" + "Cost Amount (Expected)", "Valued Quantity")
                    else begin
                        //SumUnitCost(UnitCost, "Cost Amount (Actual)" + "Cost Amount (Expected)", "Item Ledger Entry".Quantity);
                        SumUnitCost(UnitCost, "Cost Amount (Actual)" + "Cost Amount (Expected)", ItemValueLedgerEntry.Quantity);
                    end;
                until Next = 0;
        end;
    end;

    local procedure SumUnitCost(var UnitCost: Decimal; CostAmount: Decimal; Quantity: Decimal)
    begin
        UnitCost := UnitCost + CostAmount / Abs(Quantity);
    end;

    procedure InitializeRequest(NewEndingDate: Date; NewPeriodLength: DateFormula)
    begin
        PeriodStartDate[5] := NewEndingDate;
        PeriodLength := NewPeriodLength;
    end;
}

