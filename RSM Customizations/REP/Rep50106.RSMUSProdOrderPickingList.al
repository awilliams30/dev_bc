report 50106 "RSMUSProdOrderPickingList"
{
    DefaultLayout = RDLC;
    RDLCLayout = './Rep50106.RSMUSProdOrderPickingList.rdl';
    ApplicationArea = Manufacturing;
    Caption = 'Prod. Order - Picking List';
    UsageCategory = ReportsAndAnalysis;

    dataset
    {
        dataitem(Item; Item)
        {
            PrintOnlyIfDetail = true;
            RequestFilterFields = "No.", "Search Description", "Assembly BOM", "Inventory Posting Group", "Location Filter", "Bin Filter", "Shelf No.";
            column(TodayFormatted; Format(Today, 0, 4))
            {
            }
            column(CompanyName; COMPANYPROPERTY.DisplayName)
            {
            }
            column(ItemTableCaptionFilter; TableCaption + ': ' + ItemFilter)
            {
            }
            column(ItemFilter; ItemFilter)
            {
            }
            column(CompneedCompFilter; StrSubstNo(Text000, ComponentFilter))
            {
            }

            column(ComponentFilter; ComponentFilter)
            {
            }
            column(No_Item; "No.")
            {
            }
            column(BaseUOM_Item; "Base Unit of Measure")
            {
                IncludeCaption = true;
            }
            column(ProdOrderPickingListCapt; ProdOrderPickingListCaptLbl)
            {
            }
            column(CurrReportPageNoCapt; CurrReportPageNoCaptLbl)
            {
            }
            column(ProdOrderDescCaption; ProdOrderDescCaptionLbl)
            {
            }
            column(ProdOrderCompDueDateCapt; ProdOrderCompDueDateCaptLbl)
            {
            }

            dataitem("Prod. Order Component"; "Prod. Order Component")
            {
                DataItemLink = "Item No." = FIELD("No."), "Variant Code" = FIELD("Variant Filter"), "Location Code" = FIELD("Location Filter"), "Bin Code" = FIELD("Bin Filter");
                DataItemTableView = SORTING("Item No.", "Variant Code", "Location Code", Status, "Due Date");
                RequestFilterFields = Status, "Due Date";
                column(ProdOrdNo_ProdOrderComp; "Prod. Order No.")
                {
                    IncludeCaption = true;
                }
                column(Desc_ProdOrder; ProdOrder.Description)
                {
                }
                column(DueDate_ProdOrderComp; "Due Date")
                {
                }
                column(RmngQty_ProdOrderComp; "Remaining Quantity")
                {
                    IncludeCaption = true;
                }
                column(Scrap_ProdOrderComp; "Scrap %")
                {
                    IncludeCaption = true;
                }
                column(LoctionCode_ProdOrderComp; "Location Code")
                {
                    IncludeCaption = true;
                }
                column(BinCode_ProdOrderComp; "Bin Code")
                {
                    IncludeCaption = true;
                }
                column(Description_Item; Item.Description)
                {
                }
                column(Variant_Code; "Variant Code")
                {
                }
                column(VariantDesc2; ItemVariant."Description 2")
                { }
                Column(SourceNo_ProdOrder; ProdOrder."Source No.")
                { }
                Column(Quantity_ProdOrder; ProdOrder.Quantity)
                { }
                column(ItemTrackingCode; Item."Item Tracking Code") { }

                trigger OnAfterGetRecord()
                begin
                    if not ItemVariant.Get("Item No.", "Variant Code") then
                        clear(ItemVariant);

                    if Status = Status::Finished then
                        CurrReport.Skip;
                    ProdOrder.Get(Status, "Prod. Order No.");

                    if ProdOrder2.Get("Prod. Order Component".Status, "Prod. Order Component"."Prod. Order No.") then;
                end;

                trigger OnPreDataItem()
                begin
                    SetFilter("Remaining Quantity", '<>0');
                end;
            }
        }
    }

    requestpage
    {

        layout
        {
        }

        actions
        {
        }
    }

    labels
    {
    }

    trigger OnPreReport()
    begin
        ItemFilter := Item.GetFilters;
        ComponentFilter := "Prod. Order Component".GetFilters;
    end;

    var
        Text000: Label 'Component Need : %1';
        ProdOrder: Record "Production Order";
        ItemFilter: Text;
        ComponentFilter: Text;
        ProdOrderPickingListCaptLbl: Label 'Prod. Order - Picking List';
        CurrReportPageNoCaptLbl: Label 'Page';
        ProdOrderDescCaptionLbl: Label 'Name';
        ProdOrderCompDueDateCaptLbl: Label 'Due Date';
        ProdOrder2: Record "Production Order";
        ItemVariant: Record "Item Variant";
}

