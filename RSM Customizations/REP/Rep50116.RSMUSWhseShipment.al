report 50116 "RSMUSWhseShipment"
{
    DefaultLayout = RDLC;
    RDLCLayout = './Report Layouts/Rep50116.RSMUSWhseShipment.rdl';
    ApplicationArea = Warehouse;
    Caption = 'Warehouse Shipment';
    UsageCategory = Documents;

    dataset
    {
        dataitem("Warehouse Shipment Header"; "Warehouse Shipment Header")
        {
            DataItemTableView = SORTING("No.");
            RequestFilterFields = "No.";
            column(HeaderNo_WhseShptHeader; "No.")
            {
            }
            column(ShippingName;ShippingAddressArray[1]){}
            column(ShippingAddress;ShippingAddressArray[2]){}
            column(ShippingAddress2;ShippingAddressArray[3]){}
            column(ShippingCity;ShippingAddressArray[4]){}
            column(ShippingState;ShippingAddressArray[5]){}
            column(ShippingZip;ShippingAddressArray[6]){}
            column(ShippingCountry;ShippingAddressArray[7]){}
            
            dataitem("Integer"; "Integer")
            {
                DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                column(CompanyName; COMPANYPROPERTY.DisplayName)
                {
                }
                column(TodayFormatted; Format(Today, 0, 4))
                {
                }
                column(AssUid__WhseShptHeader; "Warehouse Shipment Header"."Assigned User ID")
                {
                    IncludeCaption = true;
                }
                column(HrdLocCode_WhseShptHeader; "Warehouse Shipment Header"."Location Code")
                {
                    IncludeCaption = true;
                }
                column(HeaderNo1_WhseShptHeader; "Warehouse Shipment Header"."No.")
                {
                    IncludeCaption = true;
                }
                column(Show1; not Location."Bin Mandatory")
                {
                }
                column(Show2; Location."Bin Mandatory")
                {
                }
                column(CurrReportPageNoCaption; CurrReportPageNoCaptionLbl)
                {
                }
                column(WarehouseShipmentCaption; WarehouseShipmentCaptionLbl)
                {
                }
                dataitem("Warehouse Shipment Line"; "Warehouse Shipment Line")
                {
                    DataItemLink = "No." = FIELD("No.");
                    DataItemLinkReference = "Warehouse Shipment Header";
                    DataItemTableView = SORTING("No.", "Line No.");
                    column(ShelfNo_WhseShptLine; "Shelf No.")
                    {
                        IncludeCaption = true;
                    }
                    column(ItemNo_WhseShptLine; "Item No.")
                    {
                        IncludeCaption = true;
                    }
                    column(Desc_WhseShptLine; Description)
                    {
                        IncludeCaption = true;
                    }
                    column(UomCode_WhseShptLine; "Unit of Measure Code")
                    {
                        IncludeCaption = true;
                    }
                    column(LocCode_WhseShptLine; "Location Code")
                    {
                        IncludeCaption = true;
                    }
                    column(Qty_WhseShptLine; Quantity)
                    {
                        IncludeCaption = true;
                    }
                    column(SourceNo_WhseShptLine; "Source No.")
                    {
                        IncludeCaption = true;
                    }
                    column(SourceDoc_WhseShptLine; "Source Document")
                    {
                        IncludeCaption = true;
                    }
                    column(ZoneCode_WhseShptLine; "Zone Code")
                    {
                        IncludeCaption = true;
                    }
                    column(BinCode_WhseShptLine; "Bin Code")
                    {
                        IncludeCaption = true;
                    }

                    trigger OnAfterGetRecord()
                    begin
                        GetLocation("Location Code");
                    end;
                }
            }

            trigger OnAfterGetRecord()
            begin
                GetLocation("Location Code");

                Clear(ShippingAddressArray);

                WhseShipmentLine.Reset();
                WhseShipmentLine.SetRange("No.","Warehouse Shipment Header"."No.");
                if WhseShipmentLine.Find('-') then begin

                    case WhseShipmentLine."Source Type" of
                    DATABASE::"Sales Line":
                        begin
                            if SalesHeader.GET(WhseShipmentLine."Source Subtype",WhseShipmentLine."Source No.") then begin
                                ShippingAddressArray[1] := SalesHeader."Ship-to Name";
                                ShippingAddressArray[2] := SalesHeader."Ship-to Address";
                                ShippingAddressArray[3] := SalesHeader."Ship-to Address 2";
                                ShippingAddressArray[4] := SalesHeader."Ship-to City";
                                ShippingAddressArray[5] := SalesHeader."Ship-to County";
                                ShippingAddressArray[6] := SalesHeader."Ship-to Post Code";
                                ShippingAddressArray[7] := SalesHeader."Ship-to Country/Region Code";
                            end;
                        end;
                    DATABASE::"Purchase Line":
                        begin
                            if PurchaseHeader.GET(WhseShipmentLine."Source Subtype",WhseShipmentLine."Source No.") then begin                                 
                                ShippingAddressArray[1] := PurchaseHeader."Ship-to Name";
                                ShippingAddressArray[2] := PurchaseHeader."Ship-to Address";
                                ShippingAddressArray[3] := PurchaseHeader."Ship-to Address 2";
                                ShippingAddressArray[4] := PurchaseHeader."Ship-to City";
                                ShippingAddressArray[5] := PurchaseHeader."Ship-to County";
                                ShippingAddressArray[6] := PurchaseHeader."Ship-to Post Code";
                                ShippingAddressArray[7] := PurchaseHeader."Ship-to Country/Region Code";
                            end;
                        end;
                    DATABASE::"Transfer Line":
                        begin
                            if TransferHeader.GET(WhseShipmentLine."Source No.") then begin                                 
                                ShippingAddressArray[1] := TransferHeader."Transfer-to Name";
                                ShippingAddressArray[2] := TransferHeader."Transfer-to Address";
                                ShippingAddressArray[3] := TransferHeader."Transfer-to Address 2";
                                ShippingAddressArray[4] := TransferHeader."Transfer-to City";
                                ShippingAddressArray[5] := TransferHeader."Transfer-to County";
                                ShippingAddressArray[6] := TransferHeader."Transfer-to Post Code";
                                ShippingAddressArray[7] := TransferHeader."Trsf.-to Country/Region Code";
                            end;
                        end;
                    DATABASE::"Service Line":
                        begin
                            if ServiceHeader.GET(WhseShipmentLine."Source Subtype",WhseShipmentLine."Source No.") then begin                                 
                                ShippingAddressArray[1] := ServiceHeader."Ship-to Name";
                                ShippingAddressArray[2] := ServiceHeader."Ship-to Address";
                                ShippingAddressArray[3] := ServiceHeader."Ship-to Address 2";
                                ShippingAddressArray[4] := ServiceHeader."Ship-to City";
                                ShippingAddressArray[5] := ServiceHeader."Ship-to County";
                                ShippingAddressArray[6] := ServiceHeader."Ship-to Post Code";
                                ShippingAddressArray[7] := ServiceHeader."Ship-to Country/Region Code";
                            end;
                        end;
                    end;
                    
                end;
            end;
        }
    }

    requestpage
    {
        Caption = 'Warehouse Posted Shipment';

        layout
        {
        }

        actions
        {
        }
    }

    labels
    {
    }

    var
        Location: Record Location;
        CurrReportPageNoCaptionLbl: Label 'Page';
        WarehouseShipmentCaptionLbl: Label 'Warehouse Shipment';
        WhseShipmentLine: Record "Warehouse Shipment Line";
        SalesHeader: Record "Sales Header";
        PurchaseHeader: Record "Purchase Header";
        TransferHeader: Record "Transfer Header";
        ServiceHeader: Record "Service Header";
        ShippingAddressArray: Array [10] of Text;


    local procedure GetLocation(LocationCode: Code[10])
    begin
        if LocationCode = '' then
            Location.Init
        else
            if Location.Code <> LocationCode then
                Location.Get(LocationCode);
    end;
}

