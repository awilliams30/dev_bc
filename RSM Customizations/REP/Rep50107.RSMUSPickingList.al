report 50107 "RSMUSPickingList"
{
    DefaultLayout = RDLC;
    RDLCLayout = './Rep50107.RSMUSPickingList.rdl';
    Caption = 'Picking List';

    dataset
    {
        dataitem("Warehouse Activity Header"; "Warehouse Activity Header")
        {
            DataItemTableView = SORTING(Type, "No.") WHERE(Type = FILTER(Pick | "Invt. Pick"));
            RequestFilterFields = "No.", "No. Printed";
            column(No_WhseActivHeader; "No.")
            {
            }
            column(CommentsExist; CommentsExist)
            { }
            column(ShippingAgent; ShippingAgentCode) { }
            column(ShippingAgentServiceCode; ShippingAgentServiceCode) { }
            column(CustomerName; SalesHeader."Sell-to Customer Name")
            { }
            column(ShippingMethodCode; ShippingMethodCode)
            { }
            column(WhseDocumentNo; WhseDocumentNo)
            { }
            column(SOSalesPerson;SalespersonPurchaser.Name)
            { }
            dataitem("Integer"; "Integer")
            {
                DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                column(CompanyName; COMPANYPROPERTY.DisplayName)
                {
                }
                column(TodayFormatted; Format(Today, 0, 4))
                {
                }
                column(Time; Time)
                {
                }
                column(PickFilter; PickFilter)
                {
                }
                column(DirectedPutAwayAndPick; Location."Directed Put-away and Pick")
                {
                }
                column(BinMandatory; Location."Bin Mandatory")
                {
                }
                column(InvtPick; InvtPick)
                {
                }
                column(ShowLotSN; ShowLotSN)
                {
                }
                column(SumUpLines; SumUpLines)
                {
                }
                column(No_WhseActivHeaderCaption; "Warehouse Activity Header".FieldCaption("No."))
                {
                }
                column(WhseActivHeaderCaption; "Warehouse Activity Header".TableCaption + ': ' + PickFilter)
                {
                }
                column(LoctnCode_WhseActivHeader; "Warehouse Activity Header"."Location Code")
                {
                }
                column(SortingMtd_WhseActivHeader; "Warehouse Activity Header"."Sorting Method")
                {
                }
                column(AssgUserID_WhseActivHeader; "Warehouse Activity Header"."Assigned User ID")
                {
                }
                column(SourcDocument_WhseActLine; "Warehouse Activity Line"."Source Document")
                {
                }
                column(LoctnCode_WhseActivHeaderCaption; "Warehouse Activity Header".FieldCaption("Location Code"))
                {
                }
                column(SortingMtd_WhseActivHeaderCaption; "Warehouse Activity Header".FieldCaption("Sorting Method"))
                {
                }
                column(AssgUserID_WhseActivHeaderCaption; "Warehouse Activity Header".FieldCaption("Assigned User ID"))
                {
                }
                column(SourcDocument_WhseActLineCaption; "Warehouse Activity Line".FieldCaption("Source Document"))
                {
                }
                column(SourceNo_WhseActLineCaption; WhseActLine.FieldCaption("Source No."))
                {
                }
                column(ShelfNo_WhseActLineCaption; WhseActLine.FieldCaption("Shelf No."))
                {
                }
                column(VariantCode_WhseActLineCaption; WhseActLine.FieldCaption("Variant Code"))
                {
                }
                column(Description_WhseActLineCaption; WhseActLine.FieldCaption(Description))
                {
                }
                column(ItemNo_WhseActLineCaption; WhseActLine.FieldCaption("Item No."))
                {
                }
                column(UOMCode_WhseActLineCaption; WhseActLine.FieldCaption("Unit of Measure Code"))
                {
                }
                column(QtytoHandle_WhseActLineCaption; WhseActLine.FieldCaption("Qty. to Handle"))
                {
                }
                column(QtyBase_WhseActLineCaption; WhseActLine.FieldCaption("Qty. (Base)"))
                {
                }
                column(DestinatnType_WhseActLineCaption; WhseActLine.FieldCaption("Destination Type"))
                {
                }
                column(DestinationNo_WhseActLineCaption; WhseActLine.FieldCaption("Destination No."))
                {
                }
                column(ZoneCode_WhseActLineCaption; WhseActLine.FieldCaption("Zone Code"))
                {
                }
                column(BinCode_WhseActLineCaption; WhseActLine.FieldCaption("Bin Code"))
                {
                }
                column(ActionType_WhseActLineCaption; WhseActLine.FieldCaption("Action Type"))
                {
                }
                column(CurrReportPageNoCaption; CurrReportPageNoCaptionLbl)
                {
                }
                column(PickingListCaption; PickingListCaptionLbl)
                {
                }
                column(WhseActLineDueDateCaption; WhseActLineDueDateCaptionLbl)
                {
                }
                column(QtyHandledCaption; QtyHandledCaptionLbl)
                {
                }


                dataitem("Warehouse Activity Line"; "Warehouse Activity Line")
                {
                    DataItemLink = "Activity Type" = FIELD(Type), "No." = FIELD("No.");
                    DataItemLinkReference = "Warehouse Activity Header";
                    DataItemTableView = SORTING("Activity Type", "No.", "Sorting Sequence No.") WHERE("Action Type" = filter('Take'));

                    trigger OnAfterGetRecord()
                    begin
                        if SumUpLines and
                           ("Warehouse Activity Header"."Sorting Method" <>
                            "Warehouse Activity Header"."Sorting Method"::Document)
                        then begin
                            if TempWhseActivLine."No." = '' then begin
                                TempWhseActivLine := "Warehouse Activity Line";
                                TempWhseActivLine.Insert;
                                Mark(true);
                            end else begin
                                //Original
                                //TempWhseActivLine.SetSumLinesFilter("Warehouse Activity Line");
                                TempWhseActivLine.SETCURRENTKEY("Activity Type", "No.", "Bin Code", "Breakbulk No.", "Action Type");
                                TempWhseActivLine.SETRANGE("Activity Type", "Activity Type");
                                TempWhseActivLine.SETRANGE("No.", "No.");
                                TempWhseActivLine.SETRANGE("Bin Code", "Bin Code");
                                TempWhseActivLine.SETRANGE("Item No.", "Item No.");
                                TempWhseActivLine.SETRANGE("Action Type", "Action Type");
                                TempWhseActivLine.SETRANGE("Variant Code", "Variant Code");
                                TempWhseActivLine.SETRANGE("Unit of Measure Code", "Unit of Measure Code");
                                TempWhseActivLine.SETRANGE("Due Date", "Due Date");
                                if "Warehouse Activity Header"."Sorting Method" =
                                   "Warehouse Activity Header"."Sorting Method"::"Ship-To"
                                then begin
                                    TempWhseActivLine.SetRange("Destination Type", "Destination Type");
                                    TempWhseActivLine.SetRange("Destination No.", "Destination No.")
                                end;
                                if TempWhseActivLine.FindFirst then begin
                                    TempWhseActivLine."Qty. (Base)" := TempWhseActivLine."Qty. (Base)" + "Qty. (Base)";
                                    TempWhseActivLine."Qty. to Handle" := TempWhseActivLine."Qty. to Handle" + "Qty. to Handle";
                                    TempWhseActivLine."Source No." := '';
                                    if "Warehouse Activity Header"."Sorting Method" <>
                                       "Warehouse Activity Header"."Sorting Method"::"Ship-To"
                                    then begin
                                        TempWhseActivLine."Destination Type" := TempWhseActivLine."Destination Type"::" ";
                                        TempWhseActivLine."Destination No." := '';
                                    end;
                                    TempWhseActivLine.Modify;
                                end else begin
                                    TempWhseActivLine := "Warehouse Activity Line";
                                    TempWhseActivLine.Insert;
                                    Mark(true);
                                end;
                            end;
                        end else
                            Mark(true);
                    end;

                    trigger OnPostDataItem()
                    begin
                        MarkedOnly(true);
                    end;

                    trigger OnPreDataItem()
                    begin
                        TempWhseActivLine.SetRange("Activity Type", "Warehouse Activity Header".Type);
                        TempWhseActivLine.SetRange("No.", "Warehouse Activity Header"."No.");
                        TempWhseActivLine.DeleteAll;
                        if BreakbulkFilter then
                            TempWhseActivLine.SetRange("Original Breakbulk", false);
                        Clear(TempWhseActivLine);
                    end;
                }
                dataitem(WarehouseCommentLine; "Warehouse Comment Line")
                {
                    DataItemLinkReference = "Warehouse Activity Header";
                    DataItemLink = "No." = field("No.");
                    DataItemTableView = sorting("Table Name", "Type", "No.", "Line No.") where("Table Name" = filter('Whse. Activity Header'), Type = filter('Pick'));

                    column(LineNo_CommentLin; "Line No.") { }
                    column(Comment; Comment)
                    { }
                }

                dataitem(WhseActLine; "Warehouse Activity Line")
                {
                    DataItemLink = "Activity Type" = FIELD(Type), "No." = FIELD("No.");
                    DataItemLinkReference = "Warehouse Activity Header";
                    DataItemTableView = SORTING("Activity Type", "No.", "Sorting Sequence No.") WHERE("Action Type" = filter('Take'));


                    column(SourceNo_WhseActLine; "Source No.")
                    {
                    }
                    column(FormatSourcDocument_WhseActLine; Format("Source Document"))
                    {
                    }
                    column(ShelfNo_WhseActLine; "Shelf No.")
                    {
                    }
                    column(ItemNo_WhseActLine; "Item No.")
                    {
                    }
                    column(Description_WhseActLine; Description)
                    {
                    }
                    column(VariantCode_WhseActLine; "Variant Code")
                    {

                    }
                    column(UOMCode_WhseActLine; "Unit of Measure Code")
                    {
                    }
                    column(DueDate_WhseActLine; Format("Due Date"))
                    {
                    }
                    column(QtytoHandle_WhseActLine; "Qty. to Handle")
                    {
                    }
                    column(QtyBase_WhseActLine; "Qty. (Base)")
                    {
                    }
                    column(DestinatnType_WhseActLine; "Destination Type")
                    {
                    }
                    column(DestinationNo_WhseActLine; "Destination No.")
                    {
                    }
                    column(ZoneCode_WhseActLine; "Zone Code")
                    {
                    }
                    column(BinCode_WhseActLine; "Bin Code")
                    {
                    }
                    column(ActionType_WhseActLine; "Action Type")
                    {
                    }
                    column(LotNo_WhseActLine; "Lot No.")
                    {
                    }
                    column(SerialNo_WhseActLine; "Serial No.")
                    {
                    }
                    column(LotNo_WhseActLineCaption; FieldCaption("Lot No."))
                    {
                    }
                    column(SerialNo_WhseActLineCaption; FieldCaption("Serial No."))
                    {
                    }
                    column(LineNo_WhseActLine; "Line No.")
                    {
                    }
                    column(BinRanking_WhseActLine; "Bin Ranking")
                    {
                    }
                    column(EmptyStringCaption; EmptyStringCaptionLbl)
                    {
                    }
                    column(ShippingAgentCode_WhseActLine; "Shipping Agent Code")
                    {
                    }
                    column(ShippingAgentServiceCode_WhseActLine; "Shipping Agent Service Code")
                    { }
                    column(ShipmentMethodCode_WhseActLine; "Shipment Method Code")
                    { }
                    column(VariantDesc2; ItemVariant."Description 2")
                    { }
                    column(SummedQty_WhseActLine; SummedQty)
                    { }

                    dataitem(WhseActLine2; "Warehouse Activity Line")
                    {
                        DataItemLink = "Activity Type" = FIELD("Activity Type"), "No." = FIELD("No."), "Bin Code" = FIELD("Bin Code"), "Item No." = FIELD("Item No."), "Action Type" = FIELD("Action Type"), "Variant Code" = FIELD("Variant Code"), "Unit of Measure Code" = FIELD("Unit of Measure Code"), "Due Date" = FIELD("Due Date");
                        DataItemLinkReference = WhseActLine;
                        DataItemTableView = SORTING("Activity Type", "No.", "Bin Code", "Breakbulk No.", "Action Type") WHERE("Action Type" = filter('Take'));
                        column(LotNo_WhseActLine2; "Lot No.")
                        {
                        }
                        column(SerialNo_WhseActLine2; "Serial No.")
                        {
                        }
                        column(QtyBase_WhseActLine2; "Qty. (Base)")
                        {
                        }
                        column(QtytoHandle_WhseActLine2; "Qty. to Handle")
                        {
                        }
                        column(LineNo_WhseActLine2; "Line No.")
                        {
                        }
                    }

                    dataitem(SalesLine; "Sales Line")
                    {
                        DataItemTableView = sorting("Document Type", "Document No.", "Line No.");
                        UseTemporary = true;
                        column(LineNo_SalesLine; "Line No.") { }
                        column(Description_SalesLine; Description) { }
                    }

                    trigger OnAfterGetRecord()
                    var
                        lWhseActivLine: Record "Warehouse Activity Line";
                        lWhseActivLine2: Record "Warehouse Activity Line";
                        lSalesLine: Record "Sales Line";
                        lSalesLine2: Record "Sales Line";
                        lSalesLine3: Record "Sales Line";
                        FoundLineComments: Boolean;
                        StartLineNo: Integer;
                        EndLineNo: Integer;
                    begin
                        SummedQty := 0;
                        lWhseActivLine.Reset();
                        lWhseActivLine.SetRange("Action Type", WhseActLine."Action Type");
                        lWhseActivLine.SetRange("No.", WhseActLine."No.");
                        //lWhseActivLine.SetRange("Due Date", WhseActLine."Due Date");
                        lWhseActivLine.SetRange("Item No.", WhseActLine."Item No.");
                        lWhseActivLine.SetRange(Description, WhseActLine.Description);
                        lWhseActivLine.SetRange("Variant Code", WhseActLine."Variant Code");
                        //lWhseActivLine.SetRange("Zone Code", WhseActLine."Zone Code");
                        lWhseActivLine.SetRange("Shelf No.", WhseActLine."Shelf No.");
                        lWhseActivLine.SetRange("Bin Code", WhseActLine."Bin Code");
                        //lWhseActivLine.SetRange("Unit of Measure Code", WhseActLine."Unit of Measure Code");
                        lWhseActivLine.CalcSums("Qty. (Base)");
                        SummedQty := lWhseActivLine."Qty. (Base)";

                        Clear(ItemVariant);
                        if ItemVariant.Get(WhseActLine."Item No.", WhseActLine."Variant Code") then;

                        if SumUpLines then begin
                            TempWhseActivLine.Get("Activity Type", "No.", "Line No.");
                            "Qty. (Base)" := TempWhseActivLine."Qty. (Base)";
                            "Qty. to Handle" := TempWhseActivLine."Qty. to Handle";
                        end;

                        SalesLine.DeleteAll(false);
                        if WhseActLine."Source Document" = WhseActLine."Source Document"::"Sales Order" then begin
                            lSalesLine.Reset();
                            lSalesLine.SetRange("Document Type", lSalesLine."Document Type"::Order);
                            lSalesLine.SetRange("Document No.", WhseActLine."Source No.");
                            if lSalesLine.find('-') then begin
                                StartLineNo := 0;
                                repeat
                                    if StartLineNo = 0 then begin
                                        if (lSalesLine."No." = WhseActLine."Item No.") and (lSalesLine.Description = WhseActLine.Description) and (lSalesLine."Variant Code" = WhseActLine."Variant Code") then
                                            StartLineNo := lSalesLine."Line No.";
                                    end;

                                    if (StartLineNo <> 0) and (lSalesLine."Line No." > StartLineNo) and (lSalesLine.Type = lSalesLine.Type::" ") then begin
                                        SalesLine.Init();
                                        SalesLine := lSalesLine;
                                        if SalesLine.Insert(false) then;
                                    end else
                                        if lSalesLine."Line No." <> StartLineNo then
                                            StartLineNo := 0;

                                until lSalesLine.Next() = 0;
                            end;
                        end;
                    end;

                    trigger OnPreDataItem()
                    begin
                        Copy("Warehouse Activity Line");
                        Counter := Count;
                        if Counter = 0 then
                            CurrReport.Break;

                        if BreakbulkFilter then
                            SetRange("Original Breakbulk", false);
                    end;
                }


            }


            trigger OnAfterGetRecord()
            var
                lWhseComments: Record "Warehouse Comment Line";
            begin
                GetLocation("Location Code");
                InvtPick := Type = Type::"Invt. Pick";
                if InvtPick then
                    BreakbulkFilter := false
                else
                    BreakbulkFilter := "Breakbulk Filter";

                if not IsReportInPreviewMode then
                    CODEUNIT.Run(CODEUNIT::"Whse.-Printed", "Warehouse Activity Header");

                CommentsExist := false;
                lWhseComments.Reset();
                lWhseComments.SetRange("No.", "Warehouse Activity Header"."No.");
                lWhseComments.SetRange("Table Name", lWhseComments."Table Name"::"Whse. Activity Header");
                lWhseComments.SetRange(Type, lWhseComments.Type::Pick);
                if lWhseComments.FindFirst() then
                    CommentsExist := true;


                SalesHeader.Reset();
                WhseActivityLine.Reset();
                WhseActivityLine.SetRange("Source Document", WhseActivityLine."Source Document"::"Sales Order");
                WhseActivityLine.SetRange("No.", "Warehouse Activity Header"."No.");
                if not WhseActivityLine.FindFirst() then begin
                    Clear(WhseActivityLine);
                    Clear(SalesHeader);
                    Clear(SalespersonPurchaser);
                end else begin
                    SalesHeader.SetRange("Document Type", SalesHeader."Document Type"::Order);
                    SalesHeader.SetRange("No.", WhseActivityLine."Source No.");
                    if not SalesHeader.FindFirst() then begin
                        Clear(SalesHeader);
                        Clear(SalespersonPurchaser);
                    end else begin
                        if SalesHeader."Salesperson Code" <> '' then begin
                            if SalespersonPurchaser.get(SalesHeader."Salesperson Code") then;
                        end else
                            clear(SalespersonPurchaser);
                    end;
                end;


                ShippingAgentCode := '';
                ShippingAgentServiceCode := '';
                ShippingMethodCode := '';
                WhseDocumentNo := '';


                WhseActivityLine.Reset();
                WhseActivityLine.SetRange("No.", "Warehouse Activity Header"."No.");
                WhseActivityLine.SetFilter("Shipping Agent Code", '<>%1', '');
                if WhseActivityLine.FindFirst() then
                    ShippingAgentCode := WhseActivityLine."Shipping Agent Code";

                WhseActivityLine.Reset();
                WhseActivityLine.SetRange("No.", "Warehouse Activity Header"."No.");
                WhseActivityLine.SetFilter("Shipping Agent Service Code", '<>%1', '');
                if WhseActivityLine.FindFirst() then
                    ShippingAgentServiceCode := WhseActivityLine."Shipping Agent Service Code";

                WhseActivityLine.Reset();
                WhseActivityLine.SetRange("No.", "Warehouse Activity Header"."No.");
                WhseActivityLine.SetFilter("Shipment Method Code", '<>%1', '');
                if WhseActivityLine.FindFirst() then
                    ShippingMethodCode := WhseActivityLine."Shipment Method Code";

                WhseActivityLine.Reset();
                WhseActivityLine.SetRange("No.", "Warehouse Activity Header"."No.");
                WhseActivityLine.SetFilter("Whse. Document No.", '<>%1', '');
                if WhseActivityLine.FindFirst() then
                    WhseDocumentNo := WhseActivityLine."Whse. Document No.";


            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(Breakbulk; BreakbulkFilter)
                    {
                        ApplicationArea = Warehouse;
                        Caption = 'Set Breakbulk Filter';
                        Editable = BreakbulkEditable;
                        ToolTip = 'Specifies if you do not want to view the intermediate lines that are created when the unit of measure is changed in pick instructions.';
                    }
                    field(SumUpLines; SumUpLines)
                    {
                        ApplicationArea = Warehouse;
                        Caption = 'Sum up Lines';
                        Editable = SumUpLinesEditable;
                        ToolTip = 'Specifies if you want the lines to be summed up for each item, such as several pick lines that originate from different source documents that concern the same item and bins.';
                    }
                    field(LotSerialNo; ShowLotSN)
                    {
                        ApplicationArea = Warehouse;
                        Caption = 'Show Serial/Lot Number';
                        ToolTip = 'Specifies if you want to show lot and serial number information for items that use item tracking.';
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnInit()
        begin
            SumUpLinesEditable := true;
            BreakbulkEditable := true;
        end;

        trigger OnOpenPage()
        begin
            if HideOptions then begin
                BreakbulkEditable := false;
                SumUpLinesEditable := false;
            end;
        end;
    }

    labels
    {
    }

    trigger OnPreReport()
    begin
        PickFilter := "Warehouse Activity Header".GetFilters;
    end;

    var
        Location: Record Location;
        TempWhseActivLine: Record "Warehouse Activity Line" temporary;
        ItemVariant: Record "Item Variant";
        PickFilter: Text;
        BreakbulkFilter: Boolean;
        SumUpLines: Boolean;
        HideOptions: Boolean;
        InvtPick: Boolean;
        ShowLotSN: Boolean;
        Counter: Integer;
        [InDataSet]
        BreakbulkEditable: Boolean;
        [InDataSet]
        SumUpLinesEditable: Boolean;
        CurrReportPageNoCaptionLbl: Label 'Page';
        PickingListCaptionLbl: Label 'Picking List';
        WhseActLineDueDateCaptionLbl: Label 'Due Date';
        QtyHandledCaptionLbl: Label 'Qty. Handled';
        EmptyStringCaptionLbl: Label '____________';
        SummedQty: decimal;
        CommentsExist: Boolean;
        WhseActivityLine: Record "Warehouse Activity Line";
        ShippingMethodCode: Text;
        ShippingAgentCode: Text;
        ShippingAgentServiceCode: Text;
        WhseDocumentNo: text;
        //WhseActivityLine2: Record "Warehouse Activity Line";
        SalesHeader: Record "Sales Header";
        SalespersonPurchaser: Record "Salesperson/Purchaser";



    local procedure GetLocation(LocationCode: Code[10])
    begin
        if LocationCode = '' then
            Location.Init
        else
            if Location.Code <> LocationCode then
                Location.Get(LocationCode);
    end;

    local procedure IsReportInPreviewMode(): Boolean
    var
        MailManagement: Codeunit "Mail Management";
    begin
        exit(CurrReport.Preview or MailManagement.IsHandlingGetEmailBody);
    end;

    procedure SetBreakbulkFilter(BreakbulkFilter2: Boolean)
    begin
        BreakbulkFilter := BreakbulkFilter2;
    end;

    procedure SetInventory(SetHideOptions: Boolean)
    begin
        HideOptions := SetHideOptions;
    end;
}

