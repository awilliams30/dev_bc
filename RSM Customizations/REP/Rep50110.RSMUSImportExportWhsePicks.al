/*
RSM0001 GHL 06/05/2020 Import Export Warehouse Pick transactions BRD I001
  Added support for processing Qty. to Handle
*/
report 50110 "RSMUSImportExportWhsePicks"
{
    Caption = 'Warehouse Pick Lines';
    ProcessingOnly = true;

    dataset
    {

        dataitem("Warehouse Activity Line"; "Warehouse Activity Line")
        {
            RequestFilterFields = "No.", "Location Code", "Item No.", "Variant Code", "Unit of Measure Code", "Bin Code", "Zone Code";
            DataItemTableView = sorting("Activity Type", "No.", "Line No.") where("Activity Type" = filter('Pick'));
            UseTemporary = true;
        }

    }

    requestpage
    {
        //trigger OnQueryClosePage(CloseAction: Action): Boolean
        //begin
        //    if CloseAction = CloseAction::OK then
        //        ExportWhsePicksExcel();
        //end;
    }

    procedure SetFilters(var WhseActivityHeader: Record "Warehouse Activity Header")
    begin
        "Warehouse Activity Line".SetFilter("No.", WhseActivityHeader."No.");
    end;

    var
        ExcelBuffer: Record "Excel Buffer";
        Rows: Integer;
        Columns: Integer;
        Filename: Text;
        FileMgmt: Codeunit "File Management";
        ExcelFile: File;
        Instr: InStream;
        Sheetname: Text;
        FileUploaded: Boolean;
        RowNo: Integer;
        ColNo: Integer;
        WhseActivityLine: Record "Warehouse Activity Line";
        ItemVariant: Record "Item Variant";

    procedure ImportWhsePicksExcel()
    begin
        clear(Filename);
        Clear(Sheetname);

        ExcelBuffer.DeleteAll();
        Rows := 0;
        Columns := 0;
        FileUploaded := UploadIntoStream('Select File to Upload', '', '', Filename, Instr);

        if Filename <> '' then
            Sheetname := ExcelBuffer.SelectSheetsNameStream(Instr)
        else
            exit;


        ExcelBuffer.Reset;
        ExcelBuffer.OpenBookStream(Instr, Sheetname);
        ExcelBuffer.ReadSheet();

        Commit();
        ExcelBuffer.Reset();
        ExcelBuffer.SetRange("Column No.", 1);
        if ExcelBuffer.FindFirst() then
            repeat
                Rows := Rows + 1;
            until ExcelBuffer.Next() = 0;

        for RowNo := 2 to Rows do begin
            if GetValueAtIndex(RowNo, 1) <> Format(WhseActivityLine."Activity Type"::Pick) then
                Error('Import must only contain Activity Types of Pick');

            if WhseActivityLine.Get(WhseActivityLine."Activity Type"::Pick, GetValueAtIndex(RowNo, 2), GetValueAtIndex(RowNo, 3)) then begin
                Evaluate(WhseActivityLine.Quantity, GetValueAtIndex(RowNo, 10));
                WhseActivityLine.Validate(Quantity);
                // RSM0001 >>
                // Evaluate(WhseActivityLine."Serial No.", GetValueAtIndex(RowNo, 11));
                // WhseActivityLine.Validate("Serial No.");
                // Evaluate(WhseActivityLine."Lot No.", GetValueAtIndex(RowNo, 12));
                // WhseActivityLine.Validate("Lot No.");
                // Evaluate(WhseActivityLine."Bin Code", GetValueAtIndex(RowNo, 13));
                // WhseActivityLine.Validate("Bin Code");
                // Evaluate(WhseActivityLine."Zone Code", GetValueAtIndex(RowNo, 14));
                // WhseActivityLine.Validate("Zone Code");

                Evaluate(WhseActivityLine."Qty. to Handle", GetValueAtIndex(RowNo, 11));
                WhseActivityLine.Validate("Qty. to Handle");

                Evaluate(WhseActivityLine."Serial No.", GetValueAtIndex(RowNo, 12));
                WhseActivityLine.Validate("Serial No.");
                Evaluate(WhseActivityLine."Lot No.", GetValueAtIndex(RowNo, 13));
                WhseActivityLine.Validate("Lot No.");
                Evaluate(WhseActivityLine."Bin Code", GetValueAtIndex(RowNo, 14));
                WhseActivityLine.Validate("Bin Code");
                Evaluate(WhseActivityLine."Zone Code", GetValueAtIndex(RowNo, 15));
                WhseActivityLine.Validate("Zone Code");
                // RSM0001 <<

                WhseActivityLine.Modify(true);
            end else
                Error('Document No.: %1 Line No.: %2 is not an existing Warehouse Pick', GetValueAtIndex(RowNo, 2), GetValueAtIndex(RowNo, 3));
        end;
        Message('%1 Rows Imported Successfully!!', Rows - 1);
    end;

    local procedure GetValueAtIndex(RowNo: Integer; ColNo: Integer): Text
    var
    begin
        ExcelBuffer.Reset();
        IF ExcelBuffer.Get(RowNo, ColNo) then
            exit(ExcelBuffer."Cell Value as Text");
    end;

    procedure ExportWhsePicksExcel(var WhseActivityHeader: Record "Warehouse Activity Header")
    begin
        ExportHeaderWhsePick();
        WhseActivityLine.Reset();

        WhseActivityLine.Reset();

        //WhseActivityLine.CopyFilters("Warehouse Activity Line");
        WhseActivityLine.SetRange("No.", WhseActivityHeader."No.");

        WhseActivityLine.SetRange("Activity Type", WhseActivityLine."Activity Type"::Pick);
        if WhseActivityLine.Find('-') then begin
            repeat
                if not ItemVariant.Get(WhseActivityLine."Item No.", WhseActivityLine."Variant Code") then
                    Clear(ItemVariant);

                ExcelBuffer.NewRow();
                ExcelBuffer.AddColumn(Format(WhseActivityLine."Activity Type"), false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(WhseActivityLine."No.", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(WhseActivityLine."Line No.", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Number);
                ExcelBuffer.AddColumn(WhseActivityLine."Location Code", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(WhseActivityLine."Item No.", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(WhseActivityLine."Variant Code", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(WhseActivityLine."Unit of Measure Code", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(WhseActivityLine.Description, false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(ItemVariant."Description 2", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(WhseActivityLine.Quantity, false, '', false, false, false, '', ExcelBuffer."Cell Type"::Number);
                // RSM0001 <<
                ExcelBuffer.AddColumn(WhseActivityLine."Qty. to Handle", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Number);
                // RSM0001 >>
                ExcelBuffer.AddColumn(WhseActivityLine."Serial No.", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(WhseActivityLine."Lot No.", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(WhseActivityLine."Bin Code", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(WhseActivityLine."Zone Code", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(WhseActivityLine."Action Type", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
            // ExcelBuffer.AddColumn('', false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
            until WhseActivityLine.next = 0;
            ExcelBuffer.CreateNewBook('Warehouse Pick');
            ExcelBuffer.WriteSheet('Warehouse Pick', CompanyName(), UserId());
            ExcelBuffer.CloseBook();
            ExcelBuffer.OpenExcel();
        end else
            Message('Nothing to export');
    end;

    local procedure ExportHeaderWhsePick()
    begin
        ExcelBuffer.Reset();
        ExcelBuffer.DeleteAll();
        ExcelBuffer.Init();
        ExcelBuffer.AddColumn(WhseActivityLine.FieldCaption("Activity Type"), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(WhseActivityLine.FieldCaption("No."), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(WhseActivityLine.FieldCaption("Line No."), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(WhseActivityLine.FieldCaption("Location Code"), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(WhseActivityLine.FieldCaption("Item No."), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(WhseActivityLine.FieldCaption("Variant Code"), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(WhseActivityLine.FieldCaption("Unit of Measure Code"), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(WhseActivityLine.FieldCaption(Description), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(ItemVariant.FieldCaption("Description 2"), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(WhseActivityLine.FieldCaption(Quantity), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        // RSM0001 >>
        ExcelBuffer.AddColumn(WhseActivityLine.FieldCaption("Qty. to Handle"), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        // RSM0001 <<
        ExcelBuffer.AddColumn(WhseActivityLine.FieldCaption("Serial No."), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(WhseActivityLine.FieldCaption("Lot No."), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(WhseActivityLine.FieldCaption("Bin Code"), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(WhseActivityLine.FieldCaption("Zone Code"), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(WhseActivityLine.FieldCaption("Action Type"), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
    end;

}