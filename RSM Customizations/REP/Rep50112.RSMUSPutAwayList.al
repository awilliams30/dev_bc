report 50112 "RSMUSPutAwayList"
{
    DefaultLayout = RDLC;
    RDLCLayout = './Report Layouts/Rep50112.RSMUSPutAwayList.rdl';
    Caption = 'Put-away List';

    dataset
    {
        dataitem("Warehouse Activity Header"; "Warehouse Activity Header")
        {
            DataItemTableView = SORTING(Type, "No.") WHERE(Type = FILTER("Put-away" | "Invt. Put-away"));
            RequestFilterFields = "No.", "No. Printed";
            column(No_WhseActivHeader; "No.")
            {
            }
            column(CommentsExist; CommentsExist)
            { }
            column(ShippingAgent; ShippingAgentCode) { }
            column(ShippingAgentServiceCode; ShippingAgentServiceCode) { }
            column(CustomerName; SalesHeader."Sell-to Customer Name")
            { }
            column(ShippingMethodCode; ShippingMethodCode)
            { }
            column(WhseDocumentNo; WhseDocumentNo)
            { }
            dataitem("Integer"; "Integer")
            {
                DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                column(CompanyName; COMPANYPROPERTY.DisplayName)
                {
                }
                column(TodayFormatted; Format(Today, 0, 4))
                {
                }
                column(Time; Time)
                {
                }
                column(SumUpLines; SumUpLines)
                {
                }
                column(ShowLotSN; ShowLotSN)
                {
                }
                column(InvtPutAway; InvtPutAway)
                {
                }
                column(BinMandatory; Location."Bin Mandatory")
                {
                }
                column(DirPutAwayAndPick; Location."Directed Put-away and Pick")
                {
                }
                column(PutAwayFilter; PutAwayFilter)
                {
                }
                column(TblCptnPutAwayFilter; "Warehouse Activity Header".TableCaption + ': ' + PutAwayFilter)
                {
                }
                column(No1_WhseActivHeader; "Warehouse Activity Header"."No.")
                {
                    IncludeCaption = true;
                }
                column(LocCode_WhseActivHeader; "Warehouse Activity Header"."Location Code")
                {
                    IncludeCaption = true;
                }
                column(AssgndUID_WhseActivHeader; "Warehouse Activity Header"."Assigned User ID")
                {
                    IncludeCaption = true;
                }
                column(SortingMthd_WhseActivHeader; "Warehouse Activity Header"."Sorting Method")
                {
                    IncludeCaption = true;
                }
                column(SrcDoc_WhseActivHeader; "Warehouse Activity Line"."Source Document")
                {
                    IncludeCaption = true;
                }
                column(CurrReportPAGENOCaption; CurrReportPAGENOCaptionLbl)
                {
                }
                column(PutawayListCaption; PutawayListCaptionLbl)
                {
                }
                column(WhseActLineDueDateCaption; WhseActLineDueDateCaptionLbl)
                {
                }
                column(QtyHandledCaption; QtyHandledCaptionLbl)
                {
                }
                dataitem("Warehouse Activity Line"; "Warehouse Activity Line")
                {
                    DataItemLink = "Activity Type" = FIELD(Type), "No." = FIELD("No.");
                    DataItemLinkReference = "Warehouse Activity Header";
                    DataItemTableView = SORTING("Activity Type", "No.", "Sorting Sequence No.") where("Action Type" = filter('Place'));

                    trigger OnAfterGetRecord()
                    begin
                        if SumUpLines and
                           ("Warehouse Activity Header"."Sorting Method" <>
                            "Warehouse Activity Header"."Sorting Method"::Document)
                        then begin
                            if TempWhseActivLine."No." = '' then begin
                                TempWhseActivLine := "Warehouse Activity Line";
                                TempWhseActivLine.Insert;
                                Mark(true);
                            end else begin
                                //Original
                                //TempWhseActivLine.SetSumLinesFilter("Warehouse Activity Line");
                                // if TempWhseActivLine.FindFirst then begin
                                //     TempWhseActivLine."Qty. (Base)" := TempWhseActivLine."Qty. (Base)" + "Qty. (Base)";
                                //     TempWhseActivLine."Qty. to Handle" := TempWhseActivLine."Qty. to Handle" + "Qty. to Handle";
                                //     TempWhseActivLine."Source No." := '';
                                //     TempWhseActivLine.Modify;

                                TempWhseActivLine.SETCURRENTKEY("Activity Type", "No.", "Bin Code", "Breakbulk No.", "Action Type");
                                TempWhseActivLine.SETRANGE("Activity Type", "Activity Type");
                                TempWhseActivLine.SETRANGE("No.", "No.");
                                TempWhseActivLine.SETRANGE("Bin Code", "Bin Code");
                                TempWhseActivLine.SETRANGE("Item No.", "Item No.");
                                TempWhseActivLine.SETRANGE("Action Type", "Action Type");
                                TempWhseActivLine.SETRANGE("Variant Code", "Variant Code");
                                TempWhseActivLine.SETRANGE("Unit of Measure Code", "Unit of Measure Code");
                                TempWhseActivLine.SETRANGE("Due Date", "Due Date");
                                if "Warehouse Activity Header"."Sorting Method" =
                                   "Warehouse Activity Header"."Sorting Method"::"Ship-To"
                                then begin
                                    TempWhseActivLine.SetRange("Destination Type", "Destination Type");
                                    TempWhseActivLine.SetRange("Destination No.", "Destination No.")
                                end;
                                if TempWhseActivLine.FindFirst then begin
                                    TempWhseActivLine."Qty. (Base)" := TempWhseActivLine."Qty. (Base)" + "Qty. (Base)";
                                    TempWhseActivLine."Qty. to Handle" := TempWhseActivLine."Qty. to Handle" + "Qty. to Handle";
                                    TempWhseActivLine."Source No." := '';
                                    if "Warehouse Activity Header"."Sorting Method" <>
                                       "Warehouse Activity Header"."Sorting Method"::"Ship-To"
                                    then begin
                                        TempWhseActivLine."Destination Type" := TempWhseActivLine."Destination Type"::" ";
                                        TempWhseActivLine."Destination No." := '';
                                    end;
                                    TempWhseActivLine.Modify;
                                end else begin
                                    TempWhseActivLine := "Warehouse Activity Line";
                                    TempWhseActivLine.Insert;
                                    Mark(true);
                                end;
                            end;
                        end else
                            Mark(true);
                        SetCrossDockMark("Cross-Dock Information");
                    end;

                    trigger OnPostDataItem()
                    begin
                        MarkedOnly(true);
                    end;

                    trigger OnPreDataItem()
                    begin
                        TempWhseActivLine.SetRange("Activity Type", "Warehouse Activity Header".Type);
                        TempWhseActivLine.SetRange("No.", "Warehouse Activity Header"."No.");
                        TempWhseActivLine.DeleteAll;
                        if BreakbulkFilter then
                            TempWhseActivLine.SetRange("Original Breakbulk", false);
                        Clear(TempWhseActivLine);
                    end;
                }
                dataitem(WarehouseCommentLine; "Warehouse Comment Line")
                {
                    DataItemLinkReference = "Warehouse Activity Header";
                    DataItemLink = "No." = field("No.");
                    DataItemTableView = sorting("Table Name", "Type", "No.", "Line No.") where("Table Name" = filter('Whse. Activity Header'), Type = filter('Put-Away'));

                    column(LineNo_CommentLin; "Line No.") { }
                    column(Comment; Comment)
                    { }
                }


                dataitem(WhseActLine; "Warehouse Activity Line")
                {
                    DataItemLink = "Activity Type" = FIELD(Type), "No." = FIELD("No.");
                    DataItemLinkReference = "Warehouse Activity Header";
                    DataItemTableView = SORTING("Activity Type", "No.", "Sorting Sequence No.") where("Action Type" = filter('Place'));
                    column(SrcNo_WhseActivLine; "Source No.")
                    {
                        IncludeCaption = false;
                    }
                    column(SrcDoc_WhseActivLine; Format("Source Document"))
                    {
                    }
                    column(ShelfNo_WhseActivLine; "Shelf No.")
                    {
                        IncludeCaption = false;
                    }
                    column(ItemNo1_WhseActivLine; "Item No.")
                    {
                        IncludeCaption = false;
                    }
                    column(Desc_WhseActivLine; Description)
                    {
                        IncludeCaption = false;
                    }
                    column(CrsDocInfo_WhseActivLine; "Cross-Dock Information")
                    {
                        IncludeCaption = false;
                    }
                    column(UOMCode_WhseActivLine; "Unit of Measure Code")
                    {
                        IncludeCaption = false;
                    }
                    column(DueDate_WhseActivLine; Format("Due Date"))
                    {
                    }
                    column(QtyToHndl_WhseActivLine; "Qty. to Handle")
                    {
                        IncludeCaption = false;
                    }
                    column(QtyBase_WhseActivLine; "Qty. (Base)")
                    {
                        IncludeCaption = false;
                    }
                    column(CrossDockMark; CrossDockMark)
                    {
                    }
                    column(VariantCode_WhseActivLine; "Variant Code")
                    {
                        IncludeCaption = false;
                    }
                    column(ZoneCode_WhseActivLine; "Zone Code")
                    {
                        IncludeCaption = true;
                    }
                    column(BinCode_WhseActivLine; "Bin Code")
                    {
                        IncludeCaption = true;
                    }
                    column(ActionType_WhseActivLine; "Action Type")
                    {
                        IncludeCaption = true;
                    }
                    column(LotNo1_WhseActivLine; "Lot No.")
                    {
                        IncludeCaption = true;
                    }
                    column(SerialNo_WhseActivLine; "Serial No.")
                    {
                        IncludeCaption = true;
                    }
                    column(LineNo1_WhseActivLine; "Line No.")
                    {
                    }
                    column(BinRanking_WhseActivLine; "Bin Ranking")
                    {
                    }
                    column(EmptyStringCaption; EmptyStringCaptionLbl)
                    {
                    }
                    column(ShippingAgentCode_WhseActLine; "Shipping Agent Code")
                    {
                    }
                    column(ShippingAgentServiceCode_WhseActLine; "Shipping Agent Service Code")
                    { }
                    column(ShipmentMethodCode_WhseActLine; "Shipment Method Code")
                    { }
                    column(VariantDesc2; ItemVariant."Description 2")
                    { }
                    column(SummedQty_WhseActLine; SummedQty)
                    { }

                    dataitem(WhseActLine2; "Warehouse Activity Line")
                    {
                        DataItemLink = "Activity Type" = FIELD("Activity Type"), "No." = FIELD("No."), "Bin Code" = FIELD("Bin Code"), "Item No." = FIELD("Item No."), "Action Type" = FIELD("Action Type"), "Variant Code" = FIELD("Variant Code"), "Unit of Measure Code" = FIELD("Unit of Measure Code"), "Due Date" = FIELD("Due Date");
                        DataItemTableView = SORTING("Activity Type", "No.", "Bin Code", "Breakbulk No.", "Action Type") where("Action Type" = filter('Place'));
                        column(LotNo_WhseActivLine; "Lot No.")
                        {
                        }
                        column(SerialNo2_WhseActivLine; "Serial No.")
                        {
                        }
                        column(QtyBase2_WhseActivLine; "Qty. (Base)")
                        {
                        }
                        column(QtyToHndl2_WhseActivLine; "Qty. to Handle")
                        {
                        }
                        column(LineNo_WhseActivLine; "Line No.")
                        {
                        }
                    }
                    dataitem(PurchaseOrderCommentLines; "Purchase Line")
                    {
                        DataItemTableView = sorting("Document Type", "Document No.", "Line No.");
                        DataItemLink = "Document No." = field("Source No.");
                        column(POCommentLines; Description) { }
                        trigger OnAfterGetRecord()
                        begin
                            if PurchaseOrderCommentLines.Description <> '' then
                                CommentsExist := true;
                        end;

                        trigger OnPreDataItem()
                        begin
                            SetRange(PurchaseOrderCommentLines."Document Type", PurchaseOrderCommentLines."Document Type"::Order);
                            SetRange(PurchaseOrderCommentLines.Type, PurchaseOrderCommentLines.Type::" ");
                        end;
                    }

                    dataitem(SalesLine; "Sales Line")
                    {
                        DataItemTableView = sorting("Document Type", "Document No.", "Line No.");
                        UseTemporary = true;
                        column(LineNo_SalesLine; "Line No.") { }
                        column(Description_SalesLine; Description) { }
                    }

                    trigger OnAfterGetRecord()
                    var
                        lWhseActivLine: Record "Warehouse Activity Line";
                        lWhseActivLine2: Record "Warehouse Activity Line";
                        lSalesLine: Record "Sales Line";
                        lSalesLine2: Record "Sales Line";
                        lSalesLine3: Record "Sales Line";
                        FoundLineComments: Boolean;
                        StartLineNo: Integer;
                        EndLineNo: Integer;
                    begin
                        SummedQty := 0;
                        lWhseActivLine.Reset();
                        lWhseActivLine.SetRange("Action Type", WhseActLine."Action Type");
                        lWhseActivLine.SetRange("No.", WhseActLine."No.");
                        //lWhseActivLine.SetRange("Due Date", WhseActLine."Due Date");
                        lWhseActivLine.SetRange("Item No.", WhseActLine."Item No.");
                        lWhseActivLine.SetRange(Description, WhseActLine.Description);
                        lWhseActivLine.SetRange("Variant Code", WhseActLine."Variant Code");
                        //lWhseActivLine.SetRange("Zone Code", WhseActLine."Zone Code");
                        lWhseActivLine.SetRange("Shelf No.", WhseActLine."Shelf No.");
                        lWhseActivLine.SetRange("Bin Code", WhseActLine."Bin Code");
                        //lWhseActivLine.SetRange("Unit of Measure Code", WhseActLine."Unit of Measure Code");
                        lWhseActivLine.CalcSums("Qty. (Base)");
                        SummedQty := lWhseActivLine."Qty. (Base)";

                        Clear(ItemVariant);
                        if ItemVariant.Get(WhseActLine."Item No.", WhseActLine."Variant Code") then;

                        if SumUpLines then begin
                            TempWhseActivLine.Get("Activity Type", "No.", "Line No.");
                            "Qty. (Base)" := TempWhseActivLine."Qty. (Base)";
                            "Qty. to Handle" := TempWhseActivLine."Qty. to Handle";
                        end;
                        SetCrossDockMark("Cross-Dock Information");
                        SalesLine.DeleteAll(false);
                        if WhseActLine."Source Document" = WhseActLine."Source Document"::"Sales Order" then begin
                            lSalesLine.Reset();
                            lSalesLine.SetRange("Document Type", lSalesLine."Document Type"::Order);
                            lSalesLine.SetRange("Document No.", WhseActLine."Source No.");
                            if lSalesLine.find('-') then begin
                                StartLineNo := 0;
                                repeat
                                    if StartLineNo = 0 then begin
                                        if (lSalesLine."No." = WhseActLine."Item No.") and (lSalesLine.Description = WhseActLine.Description) and (lSalesLine."Variant Code" = WhseActLine."Variant Code") then
                                            StartLineNo := lSalesLine."Line No.";
                                    end;

                                    if (StartLineNo <> 0) and (lSalesLine."Line No." > StartLineNo) and (lSalesLine.Type = lSalesLine.Type::" ") then begin
                                        SalesLine.Init();
                                        SalesLine := lSalesLine;
                                        if SalesLine.Insert(false) then;
                                    end else
                                        if lSalesLine."Line No." <> StartLineNo then
                                            StartLineNo := 0;

                                until lSalesLine.Next() = 0;
                            end;
                        end;
                    end;

                    trigger OnPreDataItem()
                    begin
                        Copy("Warehouse Activity Line");
                        Counter := Count;
                        if Counter = 0 then
                            CurrReport.Break;

                        if BreakbulkFilter then
                            SetRange("Original Breakbulk", false);
                    end;
                }
            }

            trigger OnAfterGetRecord()
            var
                lWhseComments: Record "Warehouse Comment Line";
            begin
                GetLocation("Location Code");
                InvtPutAway := Type = Type::"Invt. Put-away";
                if InvtPutAway then
                    BreakbulkFilter := false
                else
                    BreakbulkFilter := "Breakbulk Filter";

                if not IsReportInPreviewMode then
                    CODEUNIT.Run(CODEUNIT::"Whse.-Printed", "Warehouse Activity Header");


                lWhseComments.Reset();
                lWhseComments.SetRange("No.", "Warehouse Activity Header"."No.");
                lWhseComments.SetRange("Table Name", lWhseComments."Table Name"::"Whse. Activity Header");
                lWhseComments.SetRange(Type, lWhseComments.Type::"Put-away");
                if lWhseComments.FindFirst() then
                    CommentsExist := true;


                SalesHeader.Reset();
                WhseActivityLine.Reset();
                WhseActivityLine.SetRange("Source Document", WhseActivityLine."Source Document"::"Sales Order");
                WhseActivityLine.SetRange("No.", "Warehouse Activity Header"."No.");
                if not WhseActivityLine.FindFirst() then begin
                    Clear(WhseActivityLine);
                    Clear(SalesHeader);
                end else begin
                    SalesHeader.SetRange("Document Type", SalesHeader."Document Type"::Order);
                    SalesHeader.SetRange("No.", WhseActivityLine."Source No.");
                    if not SalesHeader.FindFirst() then
                        Clear(SalesHeader);
                end;


                ShippingAgentCode := '';
                ShippingAgentServiceCode := '';
                ShippingMethodCode := '';
                WhseDocumentNo := '';


                WhseActivityLine.Reset();
                WhseActivityLine.SetRange("No.", "Warehouse Activity Header"."No.");
                WhseActivityLine.SetFilter("Shipping Agent Code", '<>%1', '');
                if WhseActivityLine.FindFirst() then
                    ShippingAgentCode := WhseActivityLine."Shipping Agent Code";

                WhseActivityLine.Reset();
                WhseActivityLine.SetRange("No.", "Warehouse Activity Header"."No.");
                WhseActivityLine.SetFilter("Shipping Agent Service Code", '<>%1', '');
                if WhseActivityLine.FindFirst() then
                    ShippingAgentServiceCode := WhseActivityLine."Shipping Agent Service Code";

                WhseActivityLine.Reset();
                WhseActivityLine.SetRange("No.", "Warehouse Activity Header"."No.");
                WhseActivityLine.SetFilter("Shipment Method Code", '<>%1', '');
                if WhseActivityLine.FindFirst() then
                    ShippingMethodCode := WhseActivityLine."Shipment Method Code";

                WhseActivityLine.Reset();
                WhseActivityLine.SetRange("No.", "Warehouse Activity Header"."No.");
                WhseActivityLine.SetFilter("Whse. Document No.", '<>%1', '');
                if WhseActivityLine.FindFirst() then
                    WhseDocumentNo := WhseActivityLine."Whse. Document No.";


            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(Breakbulk; BreakbulkFilter)
                    {
                        ApplicationArea = Warehouse;
                        Caption = 'Set Breakbulk Filter';
                        Editable = BreakbulkEditable;
                        ToolTip = 'Specifies if you do not want to view the intermediate lines that are created when the unit of measure is changed in put-away instructions.';
                    }
                    field(SumUpLines; SumUpLines)
                    {
                        ApplicationArea = Warehouse;
                        Caption = 'Sum up Lines';
                        Editable = SumUpLinesEditable;
                        ToolTip = 'Specifies if you want the lines to be summed up for each item, such as several put-away lines that originate from different source documents that concern the same item and bins.';
                    }
                    field(LotSerialNo; ShowLotSN)
                    {
                        ApplicationArea = Warehouse;
                        Caption = 'Show Serial/Lot Number';
                        ToolTip = 'Specifies if you want to show lot and serial number information for items that use item tracking.';
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnInit()
        begin
            SumUpLinesEditable := true;
            BreakbulkEditable := true;
            CommentsExist := false;
        end;

        trigger OnOpenPage()
        begin
            if HideOptions then begin
                BreakbulkEditable := false;
                SumUpLinesEditable := false;
            end;
        end;
    }

    labels
    {
        WhseActLineItemNoCaption = 'Item No.';
        WhseActLineDescriptionCaption = 'Description';
        WhseActLineVariantCodeCaption = 'Variant Code';
        WhseActLineCrossDockInformationCaption = 'Cross-Dock Information';
        WhseActLineShelfNoCaption = 'Shelf No.';
        WhseActLineQtyBaseCaption = 'Quantity(Base)';
        WhseActLineQtytoHandleCaption = 'Quantity to Handle';
        WhseActLineUnitofMeasureCodeCaption = 'Unit of Measure Code';
        WhseActLineSourceNoCaption = 'Source No.';
        WhseActLineVariantDesc2Caption = 'Variant Description 2';
    }

    trigger OnPreReport()
    begin
        PutAwayFilter := "Warehouse Activity Header".GetFilters;
    end;

    var
        Location: Record Location;
        TempWhseActivLine: Record "Warehouse Activity Line" temporary;
        PutAwayFilter: Text;
        ItemVariant: Record "Item Variant";
        BreakbulkFilter: Boolean;
        SumUpLines: Boolean;
        HideOptions: Boolean;
        InvtPutAway: Boolean;
        ShowLotSN: Boolean;
        Counter: Integer;
        CrossDockMark: Text[1];
        [InDataSet]
        BreakbulkEditable: Boolean;
        [InDataSet]
        SumUpLinesEditable: Boolean;
        CurrReportPAGENOCaptionLbl: Label 'Page';
        PutawayListCaptionLbl: Label 'Put-away List';
        WhseActLineDueDateCaptionLbl: Label 'Due Date';
        QtyHandledCaptionLbl: Label 'Quantity Handled';
        EmptyStringCaptionLbl: Label '____________';
        SummedQty: decimal;
        CommentsExist: Boolean;
        WhseActivityLine: Record "Warehouse Activity Line";
        ShippingMethodCode: Text;
        ShippingAgentCode: Text;
        ShippingAgentServiceCode: Text;
        WhseDocumentNo: text;
        //WhseActivityLine2: Record "Warehouse Activity Line";
        SalesHeader: Record "Sales Header";



    local procedure GetLocation(LocationCode: Code[10])
    begin
        if LocationCode = '' then
            Location.Init
        else
            if Location.Code <> LocationCode then
                Location.Get(LocationCode);
    end;

    local procedure IsReportInPreviewMode(): Boolean
    var
        MailManagement: Codeunit "Mail Management";
    begin
        exit(CurrReport.Preview or MailManagement.IsHandlingGetEmailBody);
    end;

    procedure SetBreakbulkFilter(BreakbulkFilter2: Boolean)
    begin
        BreakbulkFilter := BreakbulkFilter2;
    end;

    procedure SetCrossDockMark(CrossDockInfo: Option)
    begin
        if CrossDockInfo <> 0 then
            CrossDockMark := '!'
        else
            CrossDockMark := '';
    end;

    procedure SetInventory(SetHideOptions: Boolean)
    begin
        HideOptions := SetHideOptions;
    end;
}

