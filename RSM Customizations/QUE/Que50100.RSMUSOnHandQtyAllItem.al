query 50100 "RSMUSOnHandQtyAllItem"
{
    Caption = 'On Hand Qty. All Item';
    QueryType = Normal;
    Permissions = tabledata "Item Ledger Entry" = r;

    elements
    {
        dataitem(ItemLedgerEntry; "Item Ledger Entry")
        {

            column(ItemNo_ItemLedgerEntry; "Item No.")
            {
            }
            column(VariantCode_ItemLedgerEntry; "Variant Code")
            {
            }
            column(LocationCode_ItemLedgerEntry; "Location Code")
            { }
            column(Quantity_ItemLedgerEntry; Quantity)
            {
                Method = Sum;
            }
            column("Count_")
            {
                Method = Count;
            }
        }
    }
}