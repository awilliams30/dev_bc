query 50104 "RSMUSBinContent" // 50104
{
    Caption = 'Bin Content';
    QueryType = Normal;
    Permissions = tabledata "Bin Content" = r, tabledata Bin = r;

    elements
    {
        dataitem(Bin; Bin)
        {
            DataItemTableFilter = "RSMUSExclude from Sales Report" = filter(TRUE);
            dataitem(BinContent; "Bin Content")
            {
                DataItemLink = "Bin Code" = Bin.Code;
                column(LocationCode; "Location Code")
                { }
                column(ItemNo; "Item No.")
                { }
                column(VariantCode; "Variant Code")
                { }
                column(UnitofMeasureCode; "Unit of Measure Code")
                { }
                column(QtyperUnitofMeasure; "Qty. per Unit of Measure")
                { }
                column(Quantity; Quantity)
                { }
                column(QuantityBase; "Quantity (Base)")
                { }
                dataitem(Item; Item)
                {
                    DataItemLink = "No." = BinContent."Item No.";
                    column(ItemType; Type)
                    {

                    }

                }

            }
        }
    }
}