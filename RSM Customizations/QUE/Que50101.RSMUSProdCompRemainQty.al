query 50101 "RSMUSProdCompRemainQty" // 50101
{
    QueryType = Normal;
    Permissions = tabledata "Prod. Order Component" = r;

    elements
    {
        dataitem(ProdOrderComp; "Prod. Order Component")
        {
            DataItemTableFilter = Status = filter(Planned .. Released), "Remaining Qty. (Base)" = filter('>0');
            column(ItemNo; "Item No.")
            {
            }
            column(VariantCode; "Variant Code")
            {
            }
            column(LocationCode; "Location Code")
            {
            }
            column(UOM; "Unit of Measure Code")
            {

            }
            column(RemainingQuantity; "Remaining Quantity")
            {
            }
            column(RemainingQuantityBase; "Remaining Qty. (Base)")
            {
            }
            dataitem(Item; Item)
            {
                DataItemLink = "No." = ProdOrderComp."Item No.";
                column(ItemType; Type)
                {

                }

            }

        }
    }

    trigger OnBeforeOpen()
    begin

    end;
}