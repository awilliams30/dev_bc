codeunit 50151 "RSMUSGeneralManagement"
{

    procedure InsertOrModifyItemVariantsCustomTab(Rec: Record "Item Variant")
    var
        lItemVariants: Record "Item Variant";
    begin
        lItemVariants.Reset();
        lItemVariants.SetRange("Item No.", Rec."Item No.");
        if lItemVariants.FindSet() then begin
            ItemVariants.Reset();
            ItemVariants.SetRange("Item No.", Rec."Item No.");
            if ItemVariants.FindSet() then
                ItemVariants.DeleteAll(true);
            repeat
                ItemVariants.Init();
                ItemVariants.Validate("Item No.", lItemVariants."Item No.");
                ItemVariants.Validate(Code, lItemVariants.Code);
                ItemVariants.Validate(Description, lItemVariants.Description);
                ItemVariants.Validate("Description 2", lItemVariants."Description 2");
                if ItemVariants.Insert(true) then;
            until lItemVariants.Next() = 0
        end else begin
            ItemVariants.Init();
            ItemVariants.Validate("Item No.", Rec."Item No.");
            ItemVariants.Validate(Code, Rec.Code);
            ItemVariants.Validate(Description, Rec.Description);
            ItemVariants.Validate("Description 2", Rec."Description 2");
            if ItemVariants.Insert(true) then;
        end;
    end;


    procedure DeleteFromItemVariantsCustomTab(ItemVariant: Record "Item Variant")
    begin
        if ItemVariants.Get(ItemVariant."Item No.", ItemVariant.Code) then
            ItemVariants.Delete(true);
    end;

    procedure DeleteFromAllItemVariantsCustomTab(ItemVariant: Record "Item Variant")
    begin
        AllItemVariant.Reset();
        AllItemVariant.SetRange("Item No.", ItemVariant."Item No.");
        AllItemVariant.SetRange(Code, ItemVariant.Code);
        if ItemVariants.FindSet() then
            ItemVariants.DeleteAll(true);
    end;

    procedure ResetChaseCheckPrinted(var Rec: Record "Gen. Journal Line")
    begin
        Rec.Validate(RSMUSWFPaymentPrinted, false);
        Rec.Modify(false);
    end;



    var
        AllItemVariant: Record RSMUSAllItemVariants;
        ItemVariants: Record RSMUSItemVariants;
}