codeunit 50121 "RSMUSInventoryReporting" // 50121
{
    procedure PopulateLocItemVarBuffer(var Buffer: record RSMUSLocationItemVariantBuffer temporary)
    begin
        Buffer.reset;
        Buffer.deleteall;
        GetLedgerEntries(Buffer);
        GetSalesLines(Buffer);
        GetProdComps(Buffer);
        GetExcludedBinContents(Buffer);
        UpdateTotals(buffer);
    end;

    local procedure UpdateTotals(var Buffer: record RSMUSLocationItemVariantBuffer temporary)
    begin
        with buffer do begin
            if not isempty then begin
                findset;
                repeat
                    "Remaining Quantity" := Value1 - Value2 - Value3 - Value4;
                    if "Remaining Quantity" < 0 then
                        "Remaining Quantity" := 0;
                    modify(false);
                until next = 0;
            end;
        end;
    end;

    local procedure GetLedgerEntries(var Buffer: record RSMUSLocationItemVariantBuffer temporary)
    var
        SourceQry: Query 50103;
    begin
        SourceQry.Open();
        while SourceQry.Read() do begin
            if SourceQry.ItemType = SourceQry.ItemType::Inventory then begin
                with buffer do begin
                    init;
                    "Item No." := SourceQry.ItemNo;
                    "Variant Code" := SourceQry.VariantCode;
                    "Location Code" := SourceQry.LocationCode;
                    Value1 := SourceQry.RemainingQuantity;
                    if not insert then begin
                        find;
                        Value1 += SourceQry.RemainingQuantity;
                        modify;
                    end;
                end;
            end;
        end;
        SourceQry.Close();
    end;

    local procedure GetSalesLines(var Buffer: record RSMUSLocationItemVariantBuffer temporary)
    var
        SourceQry: query 50102;
    begin
        SourceQry.Open();
        while SourceQry.Read() do begin
            if SourceQry.ItemType = SourceQry.ItemType::Inventory then begin
                with buffer do begin
                    init;
                    "Item No." := SourceQry.ItemNo;
                    "Variant Code" := SourceQry.VariantCode;
                    "Location Code" := SourceQry.LocationCode;
                    Value2 := SourceQry.RemainingQuantityBase;
                    if not insert then begin
                        find;
                        Value2 += SourceQry.RemainingQuantityBase;
                        modify;
                    end;
                end;
            end;
        end;
        SourceQry.Close();
    end;

    local procedure GetProdComps(var Buffer: record RSMUSLocationItemVariantBuffer temporary)
    var
        SourceQry: query 50101;
    begin
        SourceQry.Open();
        while SourceQry.Read() do begin
            if SourceQry.ItemType = SourceQry.ItemType::Inventory then begin
                with buffer do begin
                    init;
                    "Item No." := SourceQry.ItemNo;
                    "Variant Code" := SourceQry.VariantCode;
                    "Location Code" := SourceQry.LocationCode;
                    Value3 := SourceQry.RemainingQuantityBase;
                    if not insert then begin
                        find;
                        Value3 += SourceQry.RemainingQuantityBase;
                        modify;
                    end;
                end;
            end;
        end;
        SourceQry.Close();
    end;

    local procedure GetExcludedBinContents(var Buffer: record RSMUSLocationItemVariantBuffer temporary)
    var
        SourceQry: query 50104;
    begin
        SourceQry.Open();
        while SourceQry.Read() do begin
            if SourceQry.ItemType = SourceQry.ItemType::Inventory then begin
                with buffer do begin
                    init;
                    "Item No." := SourceQry.ItemNo;
                    "Variant Code" := SourceQry.VariantCode;
                    "Location Code" := SourceQry.LocationCode;
                    Value4 := SourceQry.QuantityBase;
                    if not insert then begin
                        find;
                        Value4 += SourceQry.QuantityBase;
                        modify;
                    end;
                end;
            end;
        end;
        SourceQry.Close();
    end;

}