/*
RSM0001 BH 11/18/2019 Created report selection settings
*/

codeunit 50102 "RSMUSExtensionInstall"
{
    Subtype = Install;

    trigger OnInstallAppPerCompany()
    var
        ReportSelections: Record "Report Selections";
        WhseReportSelections:Record "Report Selection Warehouse";
    begin
        //Set report selections for our custom reports
        SetReportSelection(ReportSelections.Usage::"S.Invoice", 50100);
        SetReportSelection(ReportSelections.Usage::"S.Order", 50101);
        SetReportSelection(ReportSelections.Usage::"P.Order", 50102);
        SetReportSelection(ReportSelections.Usage::"S.Cr.Memo", 50108);
        SetReportSelection(ReportSelections.Usage::"S.Shipment", 50109);
        SetReportSelection(ReportSelections.Usage::"S.Return", 50111);
        SetReportSelection(ReportSelections.Usage::Inv1,50119);
        SetReportSelection(ReportSelections.Usage::"P.Arch.Order",50103);
        SetWhseReportSelection(WhseReportSelections.Usage::Shipment,50116);
    end;

    local procedure SetReportSelection(UsageOption: Option; ReportId: Integer)
    var
        ReportSelections: Record "Report Selections";
    begin
        ReportSelections.Reset();
        ReportSelections.SetRange(Usage, UsageOption);
        if ReportSelections.FindFirst() then begin
            ReportSelections.Validate("Report ID", ReportId);
            ReportSelections.Modify(true);
        end;
    end;

    local procedure SetWhseReportSelection(UsageOption: Option; ReportID: Integer)
    var
        WhseReportSelections:Record "Report Selection Warehouse";
    begin
        WhseReportSelections.Reset();
        WhseReportSelections.SetRange(Usage, UsageOption);
        if WhseReportSelections.FindFirst() then begin
            WhseReportSelections.Validate("Report ID", ReportId);
            WhseReportSelections.Modify(true);
        end;
    end;
}