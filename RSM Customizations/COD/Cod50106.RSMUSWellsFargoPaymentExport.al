codeunit 50106 "RSMUSWellsFargoPaymentExport"
{
    procedure HaveAllLinesBeenPrinted(GenJournal: Record "Gen. Journal Line")
    begin
        PaymentJournal.Reset();
        PaymentJournal.SetFilter("Journal Template Name", GenJournal."Journal Template Name");
        PaymentJournal.SetFilter("Journal Batch Name", GenJournal."Journal Batch Name");
        PaymentJournal.SetFilter("Document Type", '%1', PaymentJournal."Document Type"::Payment);
        PaymentJournal.SetFilter(RSMUSWFPaymentPrinted, '%1', false);
        if PaymentJournal.FindFirst() then
            repeat
                clear(BankAccount);
                if BankAccount.Get(PaymentJournal."Bal. Account No.") then
                    if BankAccount.RSMUSWellsFargoBankAcct then
                        Error('All Wells Fargo payment entries must be exported before posting.');
            until PaymentJournal.Next() = 0;
    end;

    procedure WellsFargoPaymentCSV(GenJournal: Record "Gen. Journal Line")
    var
    //tempfilename: text;
    begin
        CompanyInfo.Get();
        Delimiter := '|';
        tempfilename := 'WFPaymentExport.txt';

        PaymentJournal.SetFilter("Journal Template Name", GenJournal."Journal Template Name");
        PaymentJournal.SetFilter("Journal Batch Name", GenJournal."Journal Batch Name");
        PaymentJournal.SetFilter("Document Type", '%1', PaymentJournal."Document Type"::Payment);
        RecordCount := PaymentJournal.Count();
        PaymentJournal.CalcSums(Amount);
        RecordSum := PaymentJournal.Amount;

        if PaymentJournal.FindFirst() then begin

            WellsFargoInsertHeader();

            Repeat
                clear(BankAccount);
                BankAccount.get(PaymentJournal."Bal. Account No.");

                clear(Vendor);
                Clear(VendorBankAccount);
                Vendor.Get(PaymentJournal."Bill-to/Pay-to No.");
                if VendorBankAccount.Get(Vendor."No.", PaymentJournal."Recipient Bank Account") then;
                Clear(PaymentMethod);
                PaymentMethod.Get(PaymentJournal."Payment Method Code");

                SetCheckNo();
                WellsFargoInsertPaymentRecord();
                WellsFargoInsertOriginatingPartyInfo();
                WellsFargoInsertReceivingPartyInfo();
                WellsFargoInsertReceivingBank();
                WellsFargoInsertSupplementalCheck();
                WellsFargoInsertSupplementalACH();
                WellsFargoInsertSupplementalWire();
                WellsFargoInsertSupplementalCCR();
                WellsFargoInsertInvoiceDetail();


                PaymentJournal.RSMUSWFPaymentPrinted := true;
                PaymentJournal.Modify(false);
            until PaymentJournal.Next() = 0;

            WellsFargoInsertFooter();
            Exportfile();
        end else
            Message('There is nothing to export');

    end;

    var
        AccountTypeEnum: enum "Gen. Journal Account Type";
        Vendor: Record Vendor;
        VendorBankAccount: Record "Vendor Bank Account";
        CountryRegion: Record "Country/Region";
        CompanyInfo: Record "Company Information";
        VendorLedgerEntries: Record "Vendor Ledger Entry";
        PurchInvHeader: Record "Purch. Inv. Header";
        PaymentMethod: Record "Payment Method";
        BankAccount: Record "Bank Account";
        RecordCount: Integer;
        RecordSum: Decimal;
        PaymentJournal: Record "Gen. Journal Line";
        FileLineText: Text;
        Delimiter: Text;
        NewLine: Label '\';
        TempBlob: codeunit "Temp Blob";
        CSVInStream: InStream;
        CSVOutStream: OutStream;
        CR: Char;
        LF: Char;
        tempfilename: text;
        OriginatingBankID: Label '121042882';
        OutstreamCreated: Boolean;

    local procedure Exportfile()
    begin
        TempBlob.CreateInStream(CSVInStream);
        DOWNLOADFROMSTREAM(CSVInStream, 'Export', '', 'All Files (*.*)|*.*', tempfilename);
    end;

    // local procedure InitBlob()
    // begin
    //     TempBlob.CreateOutStream(CSVOutStream);
    // end;

    local procedure AddToBlob()
    begin
        if not OutstreamCreated then begin
            TempBlob.CreateOutStream(CSVOutStream);
            OutstreamCreated := true;
        end;

        CR := 13;
        LF := 10;
        CSVOutStream.WriteText(FileLineText + CR + LF);
    end;

    local procedure WellsFargoInsertHeader()
    begin
        //Header Record
        Clear(FileLineText);
        FileLineText += 'HD' + Delimiter;
        FileLineText += format(CurrentDateTime, 0, '<Closing><Year4><Month,2><Day,2><Hours24,2><Filler Character,0><Minutes,2><Seconds,2>') + Delimiter; //+HHMMSS
        FileLineText += format(Today, 0, '<Closing><Year4>-<Month,2>-<Day,2>');

        // InitBlob();
        AddToBlob();
    end;

    local procedure WellsFargoInsertPaymentRecord()
    begin
        //Payment Record
        Clear(FileLineText);
        FileLineText += 'PY' + Delimiter;
        FileLineText += format(PaymentMethod.RSMUSWellsFargoPaymentMethod) + Delimiter;
        FileLineText += 'C' + Delimiter;
        FileLineText += PaymentJournal."Document No." + Delimiter;
        FileLineText += format(PaymentJournal."Posting Date", 0, '<Closing><Year4>-<Month,2>-<Day,2>') + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += format(Round(PaymentJournal.Amount, 0.01, '='), 0, '<Sign><Integer><Decimals><Comma,.>') + Delimiter;
        if PaymentJournal."Currency Code" <> '' then
            FileLineText += PaymentJournal."Currency Code" + Delimiter
        else
            FileLineText += 'USD' + Delimiter;
        FileLineText += 'D' + Delimiter;
        FileLineText += BankAccount."Bank Account No." + Delimiter;
        if BankAccount."Currency Code" <> '' then
            FileLineText += BankAccount."Currency Code" + Delimiter
        else
            FileLineText += 'USD' + Delimiter;
        FileLineText += 'ABA' + Delimiter;
        //FileLineText += BankAccount."Transit No." + Delimiter;
        FileLineText += OriginatingBankID + Delimiter;
        FileLineText += 'D' + Delimiter;
        FileLineText += VendorBankAccount."Bank Account No." + Delimiter;
        if VendorBankAccount."Currency Code" <> '' then
            FileLineText += VendorBankAccount."Currency Code" + Delimiter
        else
            FileLineText += 'USD' + Delimiter;


        if PaymentMethod.RSMUSWellsFargoPaymentMethod = PaymentMethod.RSMUSWellsFargoPaymentMethod::CHK then
            FileLineText += '' + Delimiter
        else
            FileLineText += 'ABA' + Delimiter;
        FileLineText += VendorBankAccount."Transit No." + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += PaymentJournal.Description + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '';
        AddToBlob();
    end;


    local procedure WellsFargoInsertOriginatingPartyInfo()
    begin
        //Payment Name & Address - Originating Party
        clear(FileLineText);
        FileLineText += 'PA' + Delimiter;
        FileLineText += 'PR' + Delimiter;
        FileLineText += CompanyInfo.Name + Delimiter;
        FileLineText += CompanyInfo."Name 2" + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += CompanyInfo.Address + Delimiter;
        FileLineText += companyinfo."Address 2" + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += CompanyInfo.City + Delimiter;
        FileLineText += CompanyInfo.County + Delimiter;
        FileLineText += CompanyInfo."Post Code" + Delimiter;
        FileLineText += CompanyInfo."Country/Region Code" + Delimiter;
        if CompanyInfo."Country/Region Code" <> '' then begin
            Clear(CountryRegion);
            CountryRegion.Get(CompanyInfo."Country/Region Code");
            FileLineText += CountryRegion.Name + Delimiter
        end else
            FileLineText += '' + Delimiter;
        FileLineText += CompanyInfo."E-Mail" + Delimiter;
        FileLineText += CompanyInfo."Phone No." + Delimiter;
        FileLineText += '';
        AddToBlob();
    end;


    local procedure WellsFargoInsertReceivingPartyInfo()
    begin
        //Payment Name & Address - Receiving Party
        Clear(FileLineText);
        FileLineText += 'PA' + Delimiter;
        FileLineText += 'PE' + Delimiter;
        FileLineText += Vendor.Name + Delimiter;
        FileLineText += Vendor."Name 2" + Delimiter;
        FileLineText += Vendor."No." + Delimiter;
        FileLineText += Vendor.Address + Delimiter;
        FileLineText += Vendor."Address 2" + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += Vendor.City + Delimiter;
        FileLineText += Vendor.County + Delimiter;
        FileLineText += Vendor."Post Code" + Delimiter;
        FileLineText += Vendor."Country/Region Code" + Delimiter;
        if Vendor."Country/Region Code" <> '' then begin
            Clear(CountryRegion);
            CountryRegion.Get(Vendor."Country/Region Code");
            FileLineText += CountryRegion.Name + Delimiter
        end else
            FileLineText += '' + Delimiter;
        FileLineText += Vendor."E-Mail" + Delimiter;
        FileLineText += Vendor."Phone No." + Delimiter;
        FileLineText += '';
        AddToBlob();
    end;

    local procedure WellsFargoInsertReceivingBank()
    begin
        //Payment Name & Address - Receiving Bank
        if PaymentJournal."Recipient Bank Account" <> '' then begin
            Clear(FileLineText);
            FileLineText += 'PA' + Delimiter;
            FileLineText += 'RB' + Delimiter;
            FileLineText += VendorBankAccount.Name + Delimiter;
            FileLineText += VendorBankAccount."Name 2" + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += VendorBankAccount.Address + Delimiter;
            FileLineText += VendorBankAccount."Address 2" + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += VendorBankAccount.City + Delimiter;
            FileLineText += VendorBankAccount.County + Delimiter;
            FileLineText += VendorBankAccount."Post Code" + Delimiter;
            FileLineText += VendorBankAccount."Country/Region Code" + Delimiter;
            if VendorBankAccount."Country/Region Code" <> '' then begin
                Clear(CountryRegion);
                CountryRegion.Get(VendorBankAccount."Country/Region Code");
                FileLineText += CountryRegion.Name + Delimiter
            end else
                FileLineText += '' + Delimiter;
            FileLineText += VendorBankAccount."E-Mail" + Delimiter;
            FileLineText += VendorBankAccount."Phone No." + Delimiter;
            FileLineText += '';
            AddToBlob();
        end;
    end;

    local procedure WellsFargoInsertSupplementalCheck()
    begin
        //Supplemental Check //Check Account
        if PaymentMethod.RSMUSWellsFargoPaymentMethod = PaymentMethod.RSMUSWellsFargoPaymentMethod::CHK then begin
            Clear(FileLineText);
            FileLineText += 'CK' + Delimiter;
            FileLineText += PaymentJournal."Document No." + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            AddToBlob();
        end;
    end;

    local procedure WellsFargoInsertSupplementalACH()
    begin
        //Supplemental ACH
        if PaymentMethod.RSMUSWellsFargoPaymentMethod = PaymentMethod.RSMUSWellsFargoPaymentMethod::DAC then begin
            Clear(FileLineText);
            FileLineText += 'AC' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += 'PPD' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '';
            AddToBlob();
        end;
    end;

    local procedure WellsFargoInsertSupplementalWire()
    begin

        //Supplemental Wire
        if PaymentMethod.RSMUSWellsFargoPaymentMethod in [PaymentMethod.RSMUSWellsFargoPaymentMethod::IWI, PaymentMethod.RSMUSWellsFargoPaymentMethod::MTS] then begin
            Clear(FileLineText);
            FileLineText += 'WR' + Delimiter;
            FileLineText += '' + Delimiter; //Blank
            FileLineText += '' + Delimiter; //Blank
            FileLineText += 'OUR' + Delimiter; //Blank
            FileLineText += '' + Delimiter; //Blank
            FileLineText += '' + Delimiter; //Blank
            FileLineText += '' + Delimiter; //Blank
            FileLineText += 'ABA' + Delimiter;
            FileLineText += VendorBankAccount."Transit No." + Delimiter; //Vendor Routing Number
            FileLineText += '' + Delimiter; //blank
            FileLineText += '' + Delimiter; //blank
            FileLineText += '' + Delimiter; //blank
            FileLineText += '' + Delimiter; //blank
            FileLineText += '' + Delimiter; //blank
            FileLineText += '' + Delimiter; //blank
            FileLineText += '' + Delimiter; //blank
            FileLineText += '';
            AddToBlob();
        end;
    end;

    local procedure WellsFargoInsertSupplementalCCR()
    begin

        //Supplemental CCR
        if PaymentMethod.RSMUSWellsFargoPaymentMethod = PaymentMethod.RSMUSWellsFargoPaymentMethod::CCR then begin
            Clear(FileLineText);
            FileLineText += 'CC' + Delimiter;
            FileLineText += format(CurrentDateTime, 0, '<Closing><Year4><Month,2><Day,2><Hours24,2><Filler Character,0><Minutes,2><Seconds,2><Second dec.>') + Delimiter; //CurrentDateTime
            FileLineText += '' + Delimiter; //blank
            FileLineText += '' + Delimiter; //blank
            FileLineText += Vendor."No." + Delimiter; //Vendor ID
            FileLineText += '' + Delimiter; //blank
            FileLineText += '' + Delimiter; //vendor email
            FileLineText += '' + Delimiter;
            FileLineText += '' + Delimiter;
            FileLineText += '';
            AddToBlob();
        end;
    end;

    local procedure WellsFargoInsertDocumentDelivery()
    begin
        //Document Delivery
        Clear(FileLineText);
        FileLineText += 'DD' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '' + Delimiter;
        FileLineText += '';
        AddToBlob();
    end;

    local procedure WellsFargoInsertInvoiceDetail()
    begin
        //InvoiceDetail
        Clear(VendorLedgerEntries);
        VendorLedgerEntries.Setrange("Vendor No.", PaymentJournal."Account No.");
        VendorLedgerEntries.SetRange("Applies-to ID", PaymentJournal."Document No.");
        VendorLedgerEntries.SetRange("Document Type", VendorLedgerEntries."Document Type"::Invoice);
        if VendorLedgerEntries.FindFirst() then begin
            repeat
                Clear(FileLineText);
                VendorLedgerEntries.CalcFields("Original Amount");

                FileLineText += 'IN' + Delimiter;
                // clear(PurchInvHeader);
                // if not PurchInvHeader.Get(VendorLedgerEntries."Document No.") then begin
                FileLineText += VendorLedgerEntries."Document No." + Delimiter;
                FileLineText += Format(VendorLedgerEntries."Posting Date", 0, '<Closing><Year4>-<Month,2>-<Day,2>') + Delimiter;
                FileLineText += VendorLedgerEntries."External Document No." + Delimiter;
                FileLineText += Format(Round(VendorLedgerEntries."Amount to Apply", 0.01, '='), 0, '<Sign><Integer><Decimals><Comma,.>') + Delimiter;
                FileLineText += Format(Round(VendorLedgerEntries."Original Amount", 0.01, '='), 0, '<Sign><Integer><Decimals><Comma,.>') + Delimiter;
                FileLineText += Format(Round(VendorLedgerEntries."Pmt. Disc. Rcd.(LCY)", 0.01, '='), 0, '<Sign><Integer><Decimals><Comma,.>') + Delimiter;
                FileLineText += VendorLedgerEntries."External Document No." + Delimiter;
                case VendorLedgerEntries."Document Type" of
                    "Gen. Journal Document Type"::Invoice:
                        FileLineText += 'IV' + Delimiter;
                    "Gen. Journal Document Type"::"Credit Memo":
                        FileLineText += 'CM' + Delimiter;
                end;
                FileLineText += '' + Delimiter;
                FileLineText += '' + Delimiter;
                FileLineText += '' + Delimiter;
                FileLineText += '' + Delimiter;
                FileLineText += '';
                AddToBlob();

            until VendorLedgerEntries.Next() = 0;
        end else begin
            Clear(VendorLedgerEntries);
            VendorLedgerEntries.Setrange("Vendor No.", PaymentJournal."Bill-to/Pay-to No.");
            VendorLedgerEntries.SetRange("Document No.", PaymentJournal."Applies-to Doc. No.");
            VendorLedgerEntries.SetRange("Document Type", PaymentJournal."Applies-to Doc. Type");
            if VendorLedgerEntries.FindFirst() then begin
                Clear(FileLineText);
                VendorLedgerEntries.CalcFields("Original Amount");


                FileLineText += 'IN' + Delimiter;
                // clear(PurchInvHeader);
                // if not PurchInvHeader.Get(VendorLedgerEntries."Document No.") then begin
                FileLineText += VendorLedgerEntries."Document No." + Delimiter;
                FileLineText += Format(VendorLedgerEntries."Posting Date", 0, '<Closing><Year4>-<Month,2>-<Day,2>') + Delimiter;
                FileLineText += VendorLedgerEntries."External Document No." + Delimiter;
                FileLineText += Format(Round(VendorLedgerEntries."Amount to Apply", 0.01, '='), 0, '<Sign><Integer><Decimals><Comma,.>') + Delimiter;
                FileLineText += Format(Round(VendorLedgerEntries."Original Amount", 0.01, '='), 0, '<Sign><Integer><Decimals><Comma,.>') + Delimiter;
                FileLineText += Format(Round(VendorLedgerEntries."Pmt. Disc. Rcd.(LCY)", 0.01, '='), 0, '<Sign><Integer><Decimals><Comma,.>') + Delimiter;
                FileLineText += VendorLedgerEntries."External Document No." + Delimiter;
                case VendorLedgerEntries."Document Type" of
                    "Gen. Journal Document Type"::Invoice:
                        FileLineText += 'IV' + Delimiter;
                    "Gen. Journal Document Type"::"Credit Memo":
                        FileLineText += 'CM' + Delimiter;
                end;
                FileLineText += '' + Delimiter;
                FileLineText += '' + Delimiter;
                FileLineText += '' + Delimiter;
                FileLineText += '' + Delimiter;
                FileLineText += '';
                AddToBlob();
            end;
        end;
    end;

    local procedure WellsFargoInsertFooter()
    begin
        Clear(FileLineText);
        FileLineText += 'TR' + Delimiter;
        FileLineText += format(RecordCount, 0, '<Sign><Integer><Comma,.>') + Delimiter;
        FileLineText += format(Round(RecordSum, 0.01, '='), 0, '<Sign><Integer><Decimals><Comma,.>');
        AddToBlob();
    end;

    local procedure SetCheckNo()
    var
        lBankAccount: Record "Bank Account";
        lPaymentMethods: Record "Payment Method";
        lCheckNo: Integer;
    begin
        if Not (Evaluate(lCheckNo, PaymentJournal."Document No.")) and not (PaymentJournal.RSMUSWFPaymentPrinted) then begin
            Clear(lPaymentMethods);
            lPaymentMethods.Get(PaymentJournal."Payment Method Code");
            if lPaymentMethods.RSMUSWellsFargoPaymentMethod = lPaymentMethods.RSMUSWellsFargoPaymentMethod::CHK then begin
                Clear(lBankAccount);
                lBankAccount.Get(PaymentJournal."Bal. Account No.");
                lBankAccount.RSMUSWellsFargoCheckNo += 1;
                PaymentJournal.Validate("Document No.", Format(lBankAccount.RSMUSWellsFargoCheckNo, 0, '<Sign><Integer><Comma,.>'));
                lBankAccount.Modify(true)
            end;
        end;
    end;

}