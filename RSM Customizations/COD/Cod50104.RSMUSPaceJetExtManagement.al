/*
RSM0001 GHL 04/23/2020 PaceJet Tracking No update - BRD S001
  New object, event subscription and related processing function.
*/
codeunit 50104 "RSMUSPaceJetExtManagement" // 50104
{
    Permissions = tabledata "Sales Header" = rm, tabledata "Sales Shipment Header" = rm, tabledata "PJ Trx. Header Hist. PSE" = r;

    [EventSubscriber(ObjectType::Table, 70174914, 'OnAfterInsertEvent', '', true, true)]
    procedure UpdateTrackingNoFromPaceJetInsert(var Rec: Record "PJ Trx. Header Hist. PSE")
    begin
        if (Rec."Transaction Status" = rec."Transaction Status"::SHIPPED)
            AND (rec."Posted Source Doc. Type" = Database::"Sales Shipment Header")
            AND (rec."Master Tracking No." <> '')
        then begin
            UpdateSalesShptTrackingNoFromPaceJet(rec);
        end;
    end;

    [EventSubscriber(ObjectType::Table, database::"sales shipment header", 'OnAfterInsertEvent', '', true, true)]
    procedure UpdateTrackingNoFromShptHdrInsert(var rec: Record "Sales Shipment Header")
    begin
        if rec."Package Tracking No." = '' then
            UpdateSalesShptTrackingNoFromShipmentHeader(rec);
    end;

    [EventSubscriber(ObjectType::Codeunit, codeunit::"Sales-Post", 'OnAfterPostSalesDoc', '', true, true)]
    procedure UpdateSalesHeaderTrackingNo(var SalesHeader: Record "Sales Header"; SalesShptHdrNo: Code[20])
    var
        PJTrxHeader: Record "PJ Trx. Header Hist. PSE";
        SalesShptHeader: record "Sales Shipment Header";
    begin
        if (SalesHeader."Document Type" = SalesHeader."Document Type"::Order)
            and (SalesHeader."Package Tracking No." = '')
            and (SalesShptHdrNo <> '')
        then begin
            SalesShptHeader.get(SalesShptHdrNo);
            if SalesShptHeader."Package Tracking No." <> '' then begin
                SalesHeader."Package Tracking No." := SalesShptHeader."Package Tracking No.";
                if SalesHeader.modify(false) then;
            end else begin
                with PJTrxHeader do begin
                    SetFilter("Source Document Type", '%1', Database::"Sales Header");
                    SetFilter("Source Document Subtype", '%1', 1); // Document Type::Order
                    SetFilter("Transaction Status", '%1', "Transaction Status"::SHIPPED);
                    setfilter("Source Document No.", '%1', SalesHeader."No.");
                    if not isempty then begin
                        findlast;
                        if strlen("Master Tracking No.") > MaxStrLen(salesheader."Package Tracking No.") then
                            "Master Tracking No." := copystr("Master Tracking No.", 1, MaxStrLen(salesheader."Package Tracking No."));
                        SalesHeader."Package Tracking No." := "Master Tracking No.";
                        if SalesHeader.modify(false) then;
                    end;
                end;
            end;
        end;
    end;

    procedure UpdateSalesShptTrackingNoFromPaceJet(PJTrxHeader: Record "PJ Trx. Header Hist. PSE")
    var
        SalesShptHeader: record "Sales Shipment Header";

    begin
        if PJTrxHeader."Posted Source Doc. Type" = Database::"Sales Shipment Header" then begin
            with SalesShptHeader do begin
                if get(PJTrxHeader."Posted Source Doc. No.") then begin
                    if strlen(PJTrxHeader."Master Tracking No.") > MaxStrLen("Package Tracking No.") then
                        PJTrxHeader."Master Tracking No." := copystr(PJTrxHeader."Master Tracking No.", 1, MaxStrLen("Package Tracking No."));
                    if ("Package Tracking No." <> PJTrxHeader."Master Tracking No.") then begin
                        "Package Tracking No." := PJTrxHeader."Master Tracking No.";
                        modify(false);
                    end;
                end;
            end;
        end;

    end;

    procedure UpdateSalesShptTrackingNoFromShipmentHeader(var SalesShptHeader: record "Sales Shipment Header")
    var
        PJTrxHeader: record "PJ Trx. Header Hist. PSE";
    begin
        if SalesShptHeader."Package Tracking No." = '' then begin
            with PJTrxHeader do begin
                SetFilter("Posted Source Doc. Type", '%1', database::"Sales Shipment Header");
                setfilter("Posted Source Doc. No.", '%1', SalesShptHeader."No.");
                SetFilter("Transaction Status", '%1', "Transaction Status"::SHIPPED);
            end;
            if not pjtrxheader.IsEmpty then begin
                pjtrxheader.FindLast();
                if strlen(PJTrxHeader."Master Tracking No.") > MaxStrLen(SalesShptHeader."Package Tracking No.") then
                    PJTrxHeader."Master Tracking No." := copystr(PJTrxHeader."Master Tracking No.", 1, MaxStrLen(SalesShptHeader."Package Tracking No."));
                if (SalesShptHeader."Package Tracking No." <> PJTrxHeader."Master Tracking No.") then begin
                    SalesShptHeader."Package Tracking No." := PJTrxHeader."Master Tracking No.";
                    SalesShptHeader.modify(false);
                end;
            end;
        end;
    end;
}