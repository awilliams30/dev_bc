/*
RSM0002 MP 05/07/2020 G007: Added logic to save Sales Invoice Report in new Sales Inv Header Blob and updated permissions
RSM0003 MP 05/29/2020 G008: Add new subscriber to set posting no series for Sales Invoices
RSM-G009 JCC 11/09/2020 - Added subscribers to validate against Sales Price Rules
*/

codeunit 50101 "RSMUS Sales Event Subscribers"
{
    // RSM0002 >>
    Permissions = tabledata "Sales Invoice Header" = rm;
    // RSM0002 <<

    //RSM0001 >>
    [EventSubscriber(ObjectType::Table, 36, 'OnAfterValidateEvent', 'RSMUSOrder Type', true, true)]
    procedure OnAfterValidateType(var Rec: Record "Sales Header")
    var
        OrderType: Code[30];
        Dimen: code[30];
        DimManagement: Codeunit DimensionManagement;
        TempDimSetEntry: Record "Dimension Set Entry" temporary;
        TempDimSetEntry2: Record "Dimension Set Entry" temporary;

        DimSetEntry: Record "Dimension Set Entry";
        DimSetEntryOriginal: Record "Dimension Set Entry";
        DimSetEntry2: Record "Dimension Set Entry" Temporary;
        DimSetEntry3: Record "Dimension Set Entry" Temporary;
        SalesLines: Record "Sales Line";
        ValueDim: Integer;
        ValueDim2: Integer;

        meSalesDocumentTypes: Option Quote,Order,Invoice,"Credit Memo","Blanket Order","Return Order";
        mcuDimensionSetTools: Codeunit "RSMUSDimension Set Tools";
    begin

        //Dimensions--------------------------------

        OrderType := rec."RSMUSOrder Type";
        DimSetEntry2.DeleteAll();
        DimSetEntry.SetFilter(DimSetEntry."Dimension Set ID", '=%1', Rec."Dimension Set ID");
        IF DimSetEntry.Find('-') THEN
            repeat
                DimSetEntry2."Dimension Set ID" := -1;
                DimSetEntry2.Validate("Dimension Code", DimSetEntry."Dimension Code");
                DimSetEntry2.Validate("Dimension Value Code", DimSetEntry."Dimension Value Code");
                DimSetEntry2.Validate("Dimension Value ID", DimSetEntry."Dimension Value ID");
                //DimSetEntry2.Validate("Dimension Name", DimSetEntry."Dimension Name");
                //DimSetEntry2.Validate("Dimension Value Name", DimSetEntry."Dimension Value Name");
                if DimSetEntry."Dimension Code" <> 'ORDER TYPE' then begin
                    DimSetEntry2.Insert();
                end;

            until DimSetEntry.Next() = 0;

        DimSetEntry2."Dimension Set ID" := -1;
        DimSetEntry2.Validate(DimSetEntry2."Dimension Code", 'ORDER TYPE');
        DimSetEntry2.Validate(DimSetEntry2."Dimension Value Code", OrderType);
        DimSetEntry2.Insert();

        ValueDim := DimManagement.GetDimensionSetID(DimSetEntry2);
        DimSetEntry.SetFilter(DimSetEntry."Dimension Set ID", '=%1', ValueDim);
        if not DimSetEntry.Find('-') then begin
            DimSetEntryOriginal.SetFilter(DimSetEntryOriginal."Dimension Set ID", '=%1', Rec."Dimension Set ID");
            IF DimSetEntryOriginal.Find('-') THEN
                repeat
                    DimSetEntry."Dimension Set ID" := ValueDim;
                    DimSetEntry.Validate("Dimension Code", DimSetEntryOriginal."Dimension Code");
                    DimSetEntry.Validate("Dimension Value Code", DimSetEntryOriginal."Dimension Value Code");
                    DimSetEntry.Validate("Dimension Value ID", DimSetEntryOriginal."Dimension Value ID");
                    DimSetEntry.Insert();
                until DimSetEntry3.Next() = 0;

            DimSetEntry."Dimension Set ID" := ValueDim;
            DimSetEntry.Validate(DimSetEntry."Dimension Code", 'ORDER TYPE');
            DimSetEntry.Validate(DimSetEntry."Dimension Value Code", OrderType);
            DimSetEntry.Insert();

            Rec."Dimension Set ID" := DimManagement.GetDimensionSetID(DimSetEntry);
        end
        ELSE
            Rec."Dimension Set ID" := ValueDim;

        Rec.Modify();

        ///////----------------------------------------
        DimSetEntry.Reset();

        SalesLines.SetFilter(SalesLines."Document Type", '=%1', SalesLines."Document Type"::Order);
        SalesLines.SetFilter(SalesLines."Document No.", '=%1', Rec."No.");
        if SalesLines.Find('-') then
            repeat
                /*S DimSetEntry.Reset();
                 TempDimSetEntry.Reset();

                 DimSetEntry.SetFilter(DimSetEntry."Dimension Set ID", '=%1', SalesLines."Dimension Set ID");
                 if DimSetEntry.Find('-') then
                     TempDimSetEntry.Copy(DimSetEntry);

                 TempDimSetEntry.Validate("Dimension Code", 'ORDER TYPE');
                 TempDimSetEntry.Validate("Dimension Value Code", OrderType);
                 ValueDim := DimManagement.GetDimensionSetID(TempDimSetEntry);
                 if ValueDim2 <> ValueDim then begin
                     TempDimSetEntry.Insert();
                     SalesLines."Dimension Set ID" := DimManagement.GetDimensionSetID(TempDimSetEntry);
                 end
                 else
                     SalesLines."Dimension Set ID" := Rec."Dimension Set ID";

                 SalesLines.Modify();
             until SalesLines.Next = 0;*/

                DimSetEntry2.DeleteAll();
                DimSetEntry.SetFilter(DimSetEntry."Dimension Set ID", '=%1', SalesLines."Dimension Set ID");
                IF DimSetEntry.Find('-') THEN
                    repeat
                        DimSetEntry2."Dimension Set ID" := -1;
                        DimSetEntry2.Validate("Dimension Code", DimSetEntry."Dimension Code");
                        DimSetEntry2.Validate("Dimension Value Code", DimSetEntry."Dimension Value Code");
                        DimSetEntry2.Validate("Dimension Value ID", DimSetEntry."Dimension Value ID");
                        if DimSetEntry."Dimension Code" <> 'ORDER TYPE' then
                            DimSetEntry2.Insert();
                    until DimSetEntry.Next() = 0;

                DimSetEntry2."Dimension Set ID" := -1;
                DimSetEntry2.Validate(DimSetEntry2."Dimension Code", 'ORDER TYPE');
                DimSetEntry2.Validate(DimSetEntry2."Dimension Value Code", OrderType);
                DimSetEntry2.Insert();

                ValueDim := DimManagement.GetDimensionSetID(DimSetEntry2);
                DimSetEntry.SetFilter(DimSetEntry."Dimension Set ID", '=%1', ValueDim);
                if not DimSetEntry.Find('-') then begin
                    DimSetEntryOriginal.SetFilter(DimSetEntryOriginal."Dimension Set ID", '=%1', SalesLines."Dimension Set ID");
                    IF DimSetEntryOriginal.Find('-') THEN
                        repeat
                            DimSetEntry."Dimension Set ID" := ValueDim;
                            DimSetEntry.Validate("Dimension Code", DimSetEntryOriginal."Dimension Code");
                            DimSetEntry.Validate("Dimension Value Code", DimSetEntryOriginal."Dimension Value Code");
                            DimSetEntry.Validate("Dimension Value ID", DimSetEntryOriginal."Dimension Value ID");
                            DimSetEntry.Insert();
                        until DimSetEntry3.Next() = 0;

                    DimSetEntry."Dimension Set ID" := ValueDim;
                    DimSetEntry.Validate(DimSetEntry."Dimension Code", 'ORDER TYPE');
                    DimSetEntry.Validate(DimSetEntry."Dimension Value Code", OrderType);
                    DimSetEntry.Insert();

                    SalesLines."Dimension Set ID" := DimManagement.GetDimensionSetID(DimSetEntry);
                end
                else
                    SalesLines."Dimension Set ID" := ValueDim;

                SalesLines.Modify();
            until SalesLines.Next = 0;


    end;


    //RSM0002 >>
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterSalesInvLineInsert', '', true, true)]
    procedure OnAfterSalesInvLineInsert(var SalesInvLine: Record "Sales Invoice Line"; SalesInvHeader: Record "Sales Invoice Header"; var SalesHeader: Record "Sales Header"; SalesLine: Record "Sales Line"; var TempItemChargeAssgntSales: Record "Item Charge Assignment (Sales)"; ItemLedgShptEntryNo: Integer; CommitIsSuppressed: Boolean; WhseReceive: Boolean; WhseShip: Boolean)
    var
        ReportOutStream: OutStream;
        SalesInvReport: Report RSMUSStandardSalesInvoice;
    begin
        SalesInvHeader.Reset();
        Clear(SalesInvHeader);

        SalesInvHeader.SetRange("No.", SalesInvLine."Document No.");
        if SalesInvHeader.FindFirst() then begin
            SalesInvHeader.SetRecFilter();
            SalesInvReport.SetTableView(SalesInvHeader);
            SalesInvHeader.RSMUSReportAttachment.CreateOutStream(ReportOutStream, TextEncoding::UTF8);
            SalesInvReport.SaveAs('', ReportFormat::Pdf, ReportOutStream);
            SalesInvHeader.Modify(false);
        end;
    end;
    //RSM0002 <<

    //RSM0003 >>
    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnAfterModifyEvent', '', true, true)]
    procedure OnAfterModifySalesHeader(var Rec: Record "Sales Header"; var xRec: Record "Sales Header"; RunTrigger: Boolean)
    var
        lNoSeries: Record "No. Series";
    begin
        if Rec.IsTemporary then exit;
        if not RunTrigger then exit;
        if Rec."Document Type" <> Rec."Document Type"::Invoice then exit;
        if lNoSeries.Get(Rec."No. Series") then begin
            if (lNoSeries."Posting Code" = '') or (lNoSeries."Posting Code" = Rec."Posting No. Series") then exit;
            Rec."Posting No. Series" := lNoSeries."Posting Code";
            Rec.Modify(false);
        end;
    end;
    //RSM0003 <<
    //<<RSM-G009 
    [EventSubscriber(ObjectType::Page, Page::"Sales Order Subform", 'OnInsertRecordEvent', '', true, true)]
    local procedure OnBeforeInsertEvent(var Rec: Record "Sales Line")
    begin
        if not (Rec.Type = Rec.Type::Item) then
            exit;
        ProcessPriceRules(Rec);
    end;

    [EventSubscriber(ObjectType::Page, Page::"Sales Order Subform", 'OnModifyRecordEvent', '', true, true)]
    local procedure OnModifyRecordEvent(var Rec: Record "Sales Line"; var xRec: Record "Sales Line"; var AllowModify: Boolean)
    begin
        if not (Rec.Type = Rec.Type::Item) then
            exit;
        if Rec."Unit Price" = xRec."Unit Price" then
            exit;
        ProcessPriceRules(Rec);
    end;

    local procedure ProcessPriceRules(SalesLine: Record "Sales Line")
    var
        SalesPriceCalculationRule: Record RSMUSSalesPriceCalculationRule;
        Item: record Item;
        UnitCost: Decimal;
    begin
        UnitCost := SalesLine."Unit Cost";
        if Item.Get(SalesLine."No.") then
            UnitCost := item."Unit Cost";

        if (FindSalesPriceRule(SalesLine, SalesPriceCalculationRule)) then begin
            if (SalesLine."Unit Price" < SalesPriceCalculationRule."Average Selling Price") and (SalesLine."Unit Price" < UnitCost) then
                message(StrSubstNo(SalesPriceAverageAndUnitCost,
                    StrSubstNo(SalesPriceRuleUnitPriceLessThanUnitCost, round(SalesLine."Unit Price", 0.01), round(UnitCost, 0.01), round(UnitCost - SalesLine."Unit Price", 0.00001)),
                    StrSubstNo(SalesPriceRuleUnitPriceLessThanAverage, round(SalesLine."Unit Price", 0.01), round(SalesPriceCalculationRule."Average Selling Price", 0.01), round(SalesLine."Unit Price" - SalesPriceCalculationRule."Average Selling Price", 0.00001))))
            else
                if (SalesLine."Unit Price" < SalesPriceCalculationRule."Average Selling Price") then
                    message(StrSubstNo(SalesPriceRuleUnitPriceLessThanAverage, round(SalesLine."Unit Price", 0.01), round(SalesPriceCalculationRule."Average Selling Price", 0.01), round(SalesLine."Unit Price" - SalesPriceCalculationRule."Average Selling Price", 0.00001)))
                else
                    if (SalesLine."Unit Price" < UnitCost) then
                        message(StrSubstNo(SalesPriceRuleUnitPriceLessThanUnitCost, round(SalesLine."Unit Price", 0.01), round(UnitCost, 0.01), round(UnitCost - SalesLine."Unit Price", 0.00001)));

        end else begin
            if SalesLine."Unit Price" < UnitCost then
                message(StrSubstNo(SalesPriceRuleUnitPriceLessThanUnitCost,
                   round(SalesLine."Unit Price", 0.01), round(UnitCost, 0.01), round(UnitCost - SalesLine."Unit Price", 0.00001)));
        end;

    end;

    local procedure FindSalesPriceRule(SalesLine: Record "Sales Line"; var SalesPriceCalculationRule: Record RSMUSSalesPriceCalculationRule): Boolean;
    var
    begin
        if not SalesPriceCalculationRule.get(SalesLine."No.", SalesLine."Variant Code") then begin
            SalesPriceCalculationRule.SetRange("Item No.", SalesLine."No.");
            SalesPriceCalculationRule.SetFilter("Variant Code", '=%1', '');
            exit(SalesPriceCalculationRule.FindFirst())
        end else
            exit(true);
    end;
    //>>RSM-G009

    var

        //RSM0002 >>
        SalesInvHeader: Record "Sales Invoice Header";
        //RSM0002 <<
        //<<RSM-G009
        SalesPriceRuleUnitPriceLessThanUnitCost: Label 'Selling Price %1 is less than Unit Cost %2. The difference is %3.';
        SalesPriceRuleUnitPriceLessThanAverage: Label 'Selling Price %1 is less than Average Selling Price %2. The difference is %3.';
        SalesPriceAverageAndUnitCost: Label '%1.\\%2';
        ConfirmManagement: Codeunit "Confirm Management";
    //>>RSM-G009

}