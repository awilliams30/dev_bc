codeunit 50119 "RSMUSOAVRecordTypeCode"
{
    [EventSubscriber(ObjectType::Table, 1241, 'OnAfterValidateEvent', 'Record Type Code', false, false)]
    local procedure PosPayDetail(var Rec: Record "Positive Pay Detail"; var xRec: Record "Positive Pay Detail"; CurrFieldNo: Integer)
    begin
        if Rec."Record Type Code" = 'V' then Rec."RSMUSRecord type code2" := '370'
        else
            if Rec."Record Type Code" = 'O' then Rec."RSMUSRecord type code2" := '320';
        Rec.Modify();
    end;
}