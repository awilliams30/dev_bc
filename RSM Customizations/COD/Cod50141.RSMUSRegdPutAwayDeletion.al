codeunit 50141 "RSMUSRegdPutAwayDeletion"
{
    Permissions = tabledata "Registered Whse. Activity Hdr." = rd;

    procedure DeleteRegdWhsePutAway(RegdWhsePutAway: Record "Registered Whse. Activity Hdr.")
    begin
        if RegdWhsePutAway.Type = RegdWhsePutAway.Type::"Put-away" then begin
            if lRegisteredWhseActivityHdr.Get(RegdWhsePutAway.Type, RegdWhsePutAway."No.") then
                lRegisteredWhseActivityHdr.Delete(true);
        end else
            Error('Nothing to delete');
    end;

    var
        lRegisteredWhseActivityHdr: Record "Registered Whse. Activity Hdr.";
}