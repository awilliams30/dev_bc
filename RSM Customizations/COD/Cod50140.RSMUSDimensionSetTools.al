codeunit 50140 "RSMUSDimension Set Tools"
{
    var
        mrTempDimSetEntry: Record "Dimension Set Entry" temporary;
        mcuDimMgt: Codeunit DimensionManagement;

    procedure AddDimensionValue(psDimensionCode: Text[20]; psDimensionValueCode: Text[20])
    var
        rDimensionValue: Record "Dimension Value";
    begin
        if not rDimensionValue.Get(psDimensionCode, psDimensionValueCode) then exit;

        mrTempDimSetEntry.Reset();
        mrTempDimSetEntry."Dimension Code" := rDimensionValue."Dimension Code";
        mrTempDimSetEntry."Dimension Value Code" := rDimensionValue.Code;
        mrTempDimSetEntry."Dimension Value ID" := rDimensionValue."Dimension Value ID";
        mrTempDimSetEntry.Insert();
    end;

    procedure GetComputedDimensionSetID(): Integer
    begin
        exit(mcuDimMgt.GetDimensionSetID(mrTempDimSetEntry));
    end;
}