codeunit 50142 "RSMUSOverrideApprovalEntries"
{
    Permissions = tabledata "Approval Entry" = rm;

    procedure ImportApprovalEntries()

    begin
        clear(Filename);
        Clear(Sheetname);

        ExcelBuffer.DeleteAll();
        Rows := 1;
        Columns := 1;
        FileUploaded := UploadIntoStream('Select File to Upload', '', '', Filename, Instr);

        if Filename <> '' then
            Sheetname := ExcelBuffer.SelectSheetsNameStream(Instr)
        else
            exit;

        ExcelBuffer.Reset;
        ExcelBuffer.OpenBookStream(Instr, Sheetname);
        ExcelBuffer.ReadSheet();

        Commit();

        ExcelBuffer.Reset();
        ExcelBuffer.SetRange("Column No.", 1);
        if ExcelBuffer.FindFirst() then
            repeat
                Rows := Rows + 1;
            until ExcelBuffer.Next() = 0;


        for RowNo := 1 to Rows do begin
            UpdateApprovalEntry();
        end;
    end;

    local procedure GetValueAtIndex(RowNo: Integer; ColNo: Integer): Text
    begin
        ExcelBuffer.Reset();
        IF ExcelBuffer.Get(RowNo, ColNo) then
            exit(ExcelBuffer."Cell Value as Text");
    end;

    local procedure UpdateApprovalEntry()
    var
        lApprovalEntries: Record "Approval Entry";
        CellValue: Text;
        EntryNo: Integer;
    begin
        CellValue := GetValueAtIndex(RowNo, 1);
        if CellValue <> '' then
            if Evaluate(EntryNo, CellValue) then
                if lApprovalEntries.get(CellValue) then begin
                    lApprovalEntries.Validate(Status, lApprovalEntries.Status::Approved);
                    lApprovalEntries.Modify(true);

                end;
    end;

    var
        ExcelBuffer: Record "Excel Buffer";
        Rows: Integer;
        Columns: Integer;
        Filename: Text;
        Instr: InStream;
        Sheetname: Text;
        FileUploaded: Boolean;
        RowNo: Integer;
}