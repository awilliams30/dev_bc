pageextension 50117 "RSMUSPositivePayExportDetail" extends "Positive Pay Export Detail"
{
    layout
    {
    }
    trigger OnAfterGetRecord()
    var
    begin
        SETFILTER("Bank Payment Type", '<>' + FORMAT("Bank Payment Type"::"Electronic Payment"));
    end;
}
