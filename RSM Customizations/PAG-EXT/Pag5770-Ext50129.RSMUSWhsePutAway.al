pageextension 50129 "RSMUSWhsePutAway" extends "Warehouse Put-away" //5770 
{
    actions
    {
        addafter("&Print")
        {
            action(RSMUSLaVerneWhsePutAway)
            {
                Caption = 'La Verne Put-Away';
                ApplicationArea = all;
                Image = Print;
                Promoted = true;
                PromotedCategory = Process;

                trigger OnAction()
                begin
                    PrintWhsePutAway(Rec);
                end;
            }
        }
    }

    procedure PrintWhsePutAway(WhseActivHeader: Record "Warehouse Activity Header") //Copy/Modified version of base BC 
    begin
        clear(PutAwayList);
        WhseActivHeader.Reset();
        WhseActivHeader.SETRANGE("No.", WhseActivHeader."No.");
        PutAwayList.SETTABLEVIEW(WhseActivHeader);
        PutAwayList.SetBreakbulkFilter(WhseActivHeader."Breakbulk Filter");
        PutAwayList.UseRequestPage(false);
        PutAwayList.RUNMODAL;
    end;

    var
        PutAwayList: Report RSMUSPutAwayList;
        WhseActivHeader: Record "Warehouse Activity Header";
}