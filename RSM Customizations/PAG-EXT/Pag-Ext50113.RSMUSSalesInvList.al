pageextension 50113 "RSMUSSalesInvList" extends "Sales Invoice List"
{
    layout
    {
        addlast(Control1)
        {
            field("RSMUSWITS Sales Order No."; "RSMUSWITS SO No.")
            {
                ApplicationArea = all;
            }
        }
    }
}