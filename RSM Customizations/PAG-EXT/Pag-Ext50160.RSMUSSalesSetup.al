/*
RSM- G009- JCC-2020/11/08 - Added RSMUSSales Price Rules Enabled to enable rule. 
*/
pageextension 50160 "RSMUSSalesSetup" extends "Sales & Receivables Setup"
{
    layout
    {
        addlast(General)
        {
            field("RSMUSSales Price Rules Enabled"; Rec."RSMUSSales Price Rules Enabled")
            {
                ApplicationArea = All;
            }
            field(RSMUSSalesPriceRulePeriod; Rec.RSMUSSalesPriceRulePeriod)
            {
                ApplicationArea = All;
            }
        }
    }

}