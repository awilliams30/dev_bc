pageextension 50128 "RSMUSRegisteredPutAway" extends "Registered Put-away"
{

    actions
    {
        addafter("&Put-away")
        {
            action(RSMUSDeletePutAway)
            {
                ApplicationArea = Basic, Suite;
                AccessByPermission = codeunit "RSMUSRegdPutAwayDeletion" = x;
                Image = Delete;
                Promoted = true;
                PromotedCategory = Process;
                PromotedOnly = true;
                Caption = 'Delete Registered Put-Away';

                trigger OnAction()
                begin
                    RegdPutAwayDeletion.DeleteRegdWhsePutAway(Rec);
                end;

            }
        }
    }

    var
        RegdPutAwayDeletion: Codeunit RSMUSRegdPutAwayDeletion;
}