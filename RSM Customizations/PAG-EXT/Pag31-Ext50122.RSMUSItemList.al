pageextension 50122 "RSMUSItemList" extends "Item List" //31
{
    actions
    {
        addafter(Availability)
        {
            action(RSMUSGeneralFilter)
            {
                Caption = 'Variant Availabilty by Filter';
                ApplicationArea = all;
                Image = Filter;
                //RunObject = Page RSMUSItemAvailbyVariant;
                trigger OnAction()
                var
                    //AvailabilityVarFilter: Page RSMUSItemAvailbyVariant;
                    Item: Record Item;
                begin
                    Item.copy(Rec);
                    Item.SetRecFilter();
                    Page.Run(Page::RSMUSItemAvailbyVariant, Item);
                end;
            }
            action(RSMUSQtyOnHandAllItems)
            {
                Caption = 'Item Qty. On Hand';
                ApplicationArea = all;
                Image = ListPage;
                RunObject = page RSMUSAllItemQtyOnHand;
            }

        }
    }
}