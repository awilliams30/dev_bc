/*
DCASE0022924 -JCC -2021/09/26 - Updates to remittance
*/

pageextension 50163 "RSMUSCompanyInfo" extends "Company Information"
{
    layout
    {
        addafter(General)
        {
            group("RSMUSRemmitance Info")
            {
                Caption = 'Remittance Information';
                field(RSMUSRemittanceName; Rec.RSMUSRemittanceName)
                {
                    ApplicationArea = All;

                }
                field("RSMUSRemittance Name 2"; Rec."RSMUSRemittance Name 2")
                { ApplicationArea = All; }
                field(RSMUSRemittanceAddress; Rec.RSMUSRemittanceAddress)
                {
                    ApplicationArea = All;
                }
                field(RSMUSRemittanceCity; Rec.RSMUSRemittanceCity)
                {
                    ApplicationArea = All;
                }
                field("RSMUSRemittance County"; Rec."RSMUSRemittance County")
                { ApplicationArea = All; }
                field("RSMUSRemittance Post Code"; Rec."RSMUSRemittance Post Code")
                { ApplicationArea = All; }
                field("RSMUSRemit Country/Region Code"; Rec."RSMUSRemit Country/Region Code")
                { ApplicationArea = All; }
            }
        }
    }

}