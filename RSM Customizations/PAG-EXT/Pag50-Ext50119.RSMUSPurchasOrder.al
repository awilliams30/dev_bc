/*
RSM0001 GHL 03/25/2020 General Revisions G004
  Added variable ShortcutDimCode.
  Added trigger OnAfterGetRecord.
  Added field ShortcutDimCode[3].
*/
pageextension 50119 "RSMUSPurchasOrder" extends "Purchase Order" // 50
{
    layout
    {
        addafter("Posting Date")
        {
            field("RSMUSArrival Date"; "RSMUSArrival Date")
            {
                ApplicationArea = all;
            }
        }
        addafter(Status)
        {
            field(ShortcutDimCode3; ShortcutDimCode[3])
            {
                ApplicationArea = All;
                CaptionClass = '1,2,3';
                Importance = Additional;
                TableRelation = "Dimension Value".Code WHERE("Global Dimension No." = CONST(3), "Dimension Value Type" = CONST(Standard), Blocked = const(false));
                trigger OnValidate()
                Begin
                    ValidateShortcutDimCode(3, ShortcutDimCode[3]);
                End;
            }
        }
        addafter("Shipment Method Code")
        {
            field(RSMUSShippingAgentCode; RSMUSShippingAgentCode)
            {
                ApplicationArea = all;
            }
            field(RSMUSShippingAgentServicecode; RSMUSShippingAgentServicecode)
            {
                ApplicationArea = all;
            }
        }
        addafter("Payment Terms Code")
        {
            field("RSMUSPayment Method Code"; "Payment Method Code")
            {
                ApplicationArea = all;
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        ShowShortcutDimCode(ShortcutDimCode);
    end;

    var
        ShortcutDimCode: array[8] of Code[20];
}