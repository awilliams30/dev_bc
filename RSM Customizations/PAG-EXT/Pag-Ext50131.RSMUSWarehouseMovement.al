pageextension 50131 "RSMUSWarehouseMovement" extends "Warehouse Movement"
{
    actions
    {
        addafter("&Print")
        {
            action(RSMUSLaVernMovement)
            {
                Caption = 'La Verne Warehouse Movement';
                Image = Print;
                ApplicationArea = all;
                trigger OnAction()
                var
                    lWhseActHeader: Record "Warehouse Activity Header";
                begin
                    lWhseActHeader.Copy(Rec);
                    lWhseActHeader.SetRecFilter();
                    Report.Run(Report::RSMUSMovementList, false, false, lWhseActHeader);
                end;
            }
        }
    }
}