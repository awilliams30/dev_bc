pageextension 50161 "RSMUSPaymentMethods" extends "Payment Methods" //427
{
    layout
    {
        addbefore("Direct Debit")
        {
            field(RSMUSWellsFargoPaymentMethod; Rec.RSMUSWellsFargoPaymentMethod)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the Wells Fargo payment type for export file.';
            }
        }
    }
}