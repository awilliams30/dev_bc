pageextension 50121 "RSMUSPaymentJournal" extends "Payment Journal"
{

    actions
    {
        modify(Post)
        {
            trigger OnBeforeAction()
            begin
                WellsFargoPaymentExport.HaveAllLinesBeenPrinted(Rec);
            end;
        }
        modify("Post and &Print")
        {
            trigger OnBeforeAction()
            begin
                WellsFargoPaymentExport.HaveAllLinesBeenPrinted(Rec);
            end;
        }

        addafter("P&osting")
        {
            action(RSMUSOverrideApprovalEntries)
            {
                Caption = 'Override Active Workflow';
                ApplicationArea = all;
                AccessByPermission = codeunit "RSMUSOverrideApprovalEntries" = x;
                trigger OnAction()
                var
                    lOverrideActiveWorkflows: Codeunit RSMUSOverrideApprovalEntries;
                begin
                    lOverrideActiveWorkflows.ImportApprovalEntries();
                end;
            }
        }
        addlast("&Payments")
        {
            action(RSMUSWellsFargoPaymentExport)
            {
                Caption = 'Wells Fargo Payment Export';
                ApplicationArea = all;
                trigger OnAction()
                begin
                    WellsFargoPaymentExport.WellsFargoPaymentCSV(Rec);
                end;
            }
        }
    }

    var
        WellsFargoPaymentExport: Codeunit RSMUSWellsFargoPaymentExport;
}