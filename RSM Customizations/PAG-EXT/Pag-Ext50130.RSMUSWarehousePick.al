pageextension 50130 "RSMUSWarehousePick" extends "Warehouse Pick"
{
    actions
    {
        addafter("&Print")
        {
            action(RSMUSLaVernPick)
            {
                Caption = 'La Verne Warehouse Pick';
                Image = Print;
                ApplicationArea = all;

                trigger OnAction()
                var
                    lWhseActHeader: Record "Warehouse Activity Header";
                begin
                    lWhseActHeader.Copy(Rec);
                    lWhseActHeader.SetRecFilter();
                    Report.Run(Report::RSMUSPickingList, false, false, lWhseActHeader);
                end;
            }
        }
        addlast("F&unctions")
        {
            action(RSMUSImportWhsePick)
            {
                Caption = 'Import Pick';
                Image = ImportExcel;
                ApplicationArea = all;
                Promoted = true;
                PromotedCategory = Process;
                PromotedOnly = true;
                trigger OnAction()
                begin
                    ImportExportWhsePick.ImportWhsePicksExcel();
                end;
            }
            action(RSMUSExportWhsePick)
            {
                Caption = 'Export Pick';
                Image = ExportToExcel;
                ApplicationArea = all;
                Promoted = true;
                PromotedCategory = Process;
                PromotedOnly = true;
                trigger OnAction()
                var
                    lWhseActivityLine: Record "Warehouse Activity Line";
                begin
                    //If wanting filtering capabilities
                    // lWhseActivityLine.Reset();
                    // lWhseActivityLine.SetRange("No.", Rec."No.");
                    // Report.Run(Report::RSMUSImportExportWhsePicks, true, false, lWhseActivityLine);
                    ImportExportWhsePick.ExportWhsePicksExcel(Rec);
                end;
            }
        }
    }
    var
        ImportExportWhsePick: Report RSMUSImportExportWhsePicks;
}