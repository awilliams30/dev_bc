/*
    RSM0001 MP 10/16/2020 AvaTaxReconciliationImportExport: Added process to allow for importing and exporting Ava Reconciliation Records
*/
pageextension 50158 "RSMUSAVAReconciliation" extends "AVA Reconciliation" //70004823
{
    actions
    {
        addlast(processing)
        {
            //RSM0001 >>
            action(RSMUSExportReconciliationLines)
            {
                Caption = 'Export Reconciliation';
                Image = ExportToExcel;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                PromotedOnly = true;
                ApplicationArea = All;

                trigger OnAction()
                begin
                    CurrPage.subSalesRecon.Page.ExportAvaReconciliationExcel();
                end;

            }
            //May no longer need
            // action(RSMUSImportReconciliationLines)
            // {
            //     Caption = 'Import Reconciliation';
            //     Image = ImportExcel;
            //     Promoted = true;
            //     PromotedCategory = Process;
            //     PromotedIsBig = true;
            //     PromotedOnly = true;
            //     ApplicationArea = All;

            //     trigger OnAction()
            //     begin
            //         CurrPage.subSalesRecon.Page.ImportAvaReconciliationExcel();
            //     end;
            // }
            //RSM0001 <<
        }
    }
}
