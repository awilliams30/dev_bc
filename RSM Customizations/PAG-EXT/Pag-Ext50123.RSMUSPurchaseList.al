pageextension 50123 "RSMUSPurchase List" extends "Purchase Order List"
{
    layout
    {
        // Add changes to page layout here
        addafter("Vendor Authorization No.")
        {
            field("RSMUSVendor Shipment No."; "Vendor Shipment No.")
            {
                ApplicationArea = all;
                Caption = 'Vendor Shipping No.';
            }
            field("RSMUSVendor Order No."; "Vendor Order No.")
            {
                ApplicationArea = all;
            }
            field("RSMUSArrival Date"; "RSMUSArrival Date")
            {
                ApplicationArea = all;
            }
            field(RSMUSApprovedbyUser; RSMUSApprovedbyUser)
            {
                ApplicationArea = all;

            }
            field(RSMUSStatus; RSMUSStatus)
            {
                ApplicationArea = all;
            }
            field(RSMUSDateInitiated; RSMUSDateInitiated)
            {
                ApplicationArea = all;
            }
            field(RSMUSAmountInvoiced; RSMUSAmountInvoiced)
            {
                ApplicationArea = all;
            }
            field(RSMUSAmtRemainig; RSMUSAmtRemainig)
            {
                ApplicationArea = all;
            }

        }
    }


    actions
    {
        // Add changes to page actions here
    }
    trigger OnAfterGetRecord()
    var
        ApprovalEntry: Record "Approval Entry";
        PurchaLines: Record "Purchase Line";
        Total: Decimal;
        Total2: Decimal;
    begin
        ApprovalEntry.SetFilter(ApprovalEntry."Document Type", '=%1', ApprovalEntry."Document Type"::Order);
        ApprovalEntry.SetFilter(ApprovalEntry."Document No.", '=%1', Rec."No.");
        if ApprovalEntry.FindLast() then begin
            RSMUSApprovedbyUser := ApprovalEntry."Approver ID";
            RSMUSStatus := format(ApprovalEntry.Status);
            RSMUSDateInitiated := ApprovalEntry."Date-Time Sent for Approval";
        end;
        PurchaLines.SetFilter(PurchaLines."Document Type", '=%1', Rec."Document Type"::Order);
        PurchaLines.SetFilter(PurchaLines."Document No.", '=%1', Rec."No.");
        IF PurchaLines.Find('-') then
            repeat
                Total := Total + (PurchaLines."Quantity Invoiced" * PurchaLines."Direct Unit Cost");
                Total2 := Total2 + ((PurchaLines.Quantity - PurchaLines."Quantity Invoiced") * PurchaLines."Direct Unit Cost");
            until PurchaLines.Next = 0;

        RSMUSAmtRemainig := Total2;
        RSMUSAmountInvoiced := Total;

    end;

    var
        myInt: Integer;
}