pageextension 50102 "RSMUSItemCard" extends "Item Card"
{
    layout
    {
        addafter("SAT Item Classification")
        {
            field("RSMUSQty. on Component Lines16918"; "Qty. on Component Lines")
            {
                ApplicationArea = All;
            }
        }
        addafter("Service Item Group")
        {
            field("RSMUSManufacturer Code"; "Manufacturer Code")
            {
                ApplicationArea = All;
            }
        }

    }
    actions
    {
        addafter(Availability)
        {
            action(RSMUSGeneralFilter)
            {
                Caption = 'Variant Availabilty by Filter';
                ApplicationArea = all;
                Image = Filter;
                //RunObject = Page RSMUSItemAvailbyVariant;
                trigger OnAction()
                var
                    //AvailabilityVarFilter: Page RSMUSItemAvailbyVariant;
                    Item: Record Item;
                begin
                    Item.copy(Rec);
                    Item.SetRecFilter();
                    Page.Run(Page::RSMUSItemAvailbyVariant, Item);
                end;
            }
            action(RSMUSQtyOnHandAllItems)
            {
                Caption = 'Item Qty. On Hand';
                ApplicationArea = all;
                Image = ListPage;
                RunObject = page RSMUSAllItemQtyOnHand;
            }

        }
    }
}
