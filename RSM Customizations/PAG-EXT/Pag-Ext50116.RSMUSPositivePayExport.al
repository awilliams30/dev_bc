pageextension 50116 "RSMUSPositive Pay Export" extends "Positive Pay Export"
{
    layout
    {
    }
    actions
    {
        /*
            modify(Export)
            {
                //Enabled = False;
                Visible = False;
            }
            */
        addafter(Export)
        {
            action("RSMUSExport2")
            {
                ApplicationArea = all;
                Caption = 'Export 2';
                Image = Export;
                Promoted = true;
                PromotedIsBig = true;
                PromotedCategory = Process;

                trigger OnAction()
                var
                    CheckLedgerEntry: Record "Check Ledger Entry";
                begin
                    CheckLedgerEntry.Reset();
                    CheckLedgerEntry.SETCURRENTKEY("Bank Account No.", "Check Date");
                    CheckLedgerEntry.SETRANGE("Bank Account No.", "No.");
                    CheckLedgerEntry.SETRANGE("Check Date", LastUploadDateEntered, CutoffUploadDate);
                    CheckLedgerEntry.SETFILTER("Bank Payment Type", '<>' + FORMAT(CheckLedgerEntry."Bank Payment Type"::"Electronic Payment"));
                    // Filter to same CheckLedgerEntry but for all other than employee
                    // CheckLedgerEntry.ExportCheckFile;

                    // Refilter to get only employee records
                    // Get line with highest line number
                    // Run own process to insert Positive Pay Detail entries
                    // Copy from CU 1704 PreparePosPayDetail



                    CheckLedgerEntry.ExportCheckFile;
                    UpdateSubForm2();
                end;
            }
        }
    }
    var
        PositivePayEntry: Record "Positive Pay Entry";
        LastUploadDateEntered: Date;
        LastUploadTime: Time;
        CutoffUploadDate: Date;

    trigger OnOpenPage()
    var
        PositivePayEntry: record "Positive Pay Entry";
    begin
        PositivePayEntry.SETRANGE("Bank Account No.", "No.");
        IF PositivePayEntry.FINDLAST THEN BEGIN
            LastUploadDateEntered := DT2DATE(PositivePayEntry."Upload Date-Time");
            LastUploadTime := DT2TIME(PositivePayEntry."Upload Date-Time");
        END;
        CutoffUploadDate := WORKDATE;
        UpdateSubForm2();
    end;

    procedure UpdateSubForm2()
    var
    begin
        CurrPage.PosPayExportDetail.PAGE.Set(LastUploadDateEntered, CutoffUploadDate, "No.");
    end;
}
