pageextension 50118 "RSMUS Posted Sales Inv Subform" extends "Posted Sales Invoice Subform"
{
    layout
    {
        addafter("No.")
        {
            /*
            field("RSMUS Facebook Line No."; "RSMUS Facebook Line No.")
            {
                ApplicationArea = All;
            }
            */
            field("RSMUS Customer Ref No."; "RSMUS Customer Ref No.")
            {
                ApplicationArea = All;
            }
        }
    }
}
