pageextension 50114 "RSMUSSalesInvoice" extends "Sales Invoice"
{
    layout
    {
        addlast(General)
        {
            field("RSMUSWITS Sales Order No."; "RSMUSWITS SO No.")
            {
                ApplicationArea = all;
            }
        }
    }
}