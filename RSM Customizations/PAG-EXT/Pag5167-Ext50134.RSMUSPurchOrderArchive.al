/*
RSM0001 GHL 05/29/2020 PO SO Archive Attachments BRD P002
  Added Attachments menu option.
*/

pageextension 50134 "RSMUSPurchOrderArchive" extends "Purchase Order Archive" // 5167
{
    actions
    {
        addlast(navigation)
        {
            action(RSMUSShowAttachments)
            {
                Caption = 'Attachments';
                ApplicationArea = All;
                trigger OnAction()
                var
                    DocAttachment: record "Document Attachment";
                    DocAttachmentList: page 1173;
                begin
                    with DocAttachment do begin
                        setfilter("Table ID", '%1', database::"Purchase Header Archive");
                        //setfilter("Table ID", '%1', 5107);
                        setfilter("Document Type", '%1', rec."Document Type");
                        SetFilter("No.", '%1', rec."No.");
                        SetFilter("Line No.", '%1', rec."Version No.");
                    end;
                    with DocAttachmentList do begin
                        Editable := false;
                        SetTableView(DocAttachment);
                        runmodal;
                    end;
                end;
            }
        }

    }
}