pageextension 50105 "RSMUSSalesOrderList" extends "Sales Order List"
{
    layout
    {
        modify("Document Date")
        {
            Caption = 'Sales Order Date';
        }
        modify("External Document No.")
        {
            Caption = 'Customer PO No.';
        }

        addafter("Location Code")
        {
            field("RSMUSPayment Method Code"; "Payment Method Code")
            {
                ApplicationArea = all;
            }
        }
    }
}