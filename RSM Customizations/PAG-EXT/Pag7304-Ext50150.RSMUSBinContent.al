/*
RSM0001 GHL 03/19/2020 Generic enhancements BRD G003
    Added field(s) RSMUSVariant Description 2, RSMUSItem Category Code.
*/

pageextension 50150 "RSMUSBinContent" extends "Bin Content" // 7304
{
    layout
    {
        addafter("Item No.")
        {
            field("RSMUSItem Category Code"; "RSMUSItem Category Code")
            {
                ApplicationArea = All;
            }
            field("RSMUSVariant Description 2"; "RSMUSVariant Description 2")
            {
                ApplicationArea = All;
            }
        }
    }
}