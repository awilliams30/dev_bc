table 50105 "RSMUSSalesPriceCalculationRule"
{
    DataClassification = ToBeClassified;
    Caption = 'Sales Price Calculation Rule';

    fields
    {
        field(1; "Item No."; Code[20])
        {
            Caption = 'Item No.';
            TableRelation = Item;
        }
        field(2; "Variant Code"; Code[10])
        {
            Caption = 'Variant Code';
            TableRelation = "Item Variant".Code WHERE("Item No." = FIELD("Item No."));

        }
        field(3; Quantity; Decimal)
        {
            Caption = 'Quantity';
            DecimalPlaces = 0 : 2;
        }
        field(4; Amount; Decimal)
        {
            DecimalPlaces = 0 : 2;
            Caption = 'Amount';
        }
        field(5; "Average Selling Price"; Decimal)
        {
            DecimalPlaces = 0 : 2;
            Caption = 'Average Selling Price';
        }
        field(6; "Last Date/Time Modified"; DateTime)
        {
            Caption = 'Last Date/Time Modified';
            Editable = false;
        }

    }
    keys
    {
        key(PK; "Item No.", "Variant Code")
        {
            Clustered = true;
        }
    }



    trigger OnInsert()
    begin

    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;

}