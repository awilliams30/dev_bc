table 50104 "RSMUSItemVariants"
{
    DataClassification = ToBeClassified;
    DrillDownPageId = RSMUSItemVariants;
    LookupPageId = RSMUSItemVariants;


    fields
    {

        field(50100; "Item No."; Code[20])
        {
        }
        field(50101; "Code"; Code[10])
        {
        }
        field(50102; Description; Text[100])
        {

        }
        field(50103; "Description 2"; text[50])
        {
        }
    }

    keys
    {
        key(PK; "Item No.", Code)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; Code, "Description 2", Description) { }
        fieldgroup(Brick; Code, "Description 2", Description) { }
    }
}