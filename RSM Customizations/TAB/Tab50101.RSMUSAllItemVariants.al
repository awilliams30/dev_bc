table 50101 "RSMUSAllItemVariants"
{
    DataClassification = ToBeClassified;
    DrillDownPageId = RSMUSAllItemVariants;
    LookupPageId = RSMUSAllItemVariants;


    fields
    {

        field(50100; "Item No."; Code[20])
        {
            ////Editable = false;
            // TableRelation = RSMUSAllItemVariants."Item No.";
            // ValidateTableRelation = false;
        }
        field(50101; "Code"; Code[10])
        {
            ////Editable = false;
            // TableRelation = RSMUSAllItemVariants.Code;
            // ValidateTableRelation = false;
        }
        field(50102; Description; Text[100])
        {
            // Caption = 'Description';
            // ////Editable = false;
            // TableRelation = RSMUSAllItemVariants.Description;
            // ValidateTableRelation = false;

        }
        field(50103; "Description 2"; text[50])
        {
            // Caption = 'Description 2';
            // ////Editable = false;
            // TableRelation = RSMUSAllItemVariants."Description 2";
            // ValidateTableRelation = false;
        }
    }

    keys
    {
        key(PK; "Item No.", Code, Description, "Description 2")
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; Code, "Description 2", Description) { }
        fieldgroup(Brick; Code, "Description 2", Description) { }
    }
}